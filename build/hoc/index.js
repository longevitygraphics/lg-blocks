/******/ (function() { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/classnames-npm-2.3.2-d2fdae468d-10.zip/node_modules/classnames/index.js":
/*!*****************************************************************************************************************************************!*\
  !*** ../../../../../../../../../AppData/Local/Yarn/Berry/cache/classnames-npm-2.3.2-d2fdae468d-10.zip/node_modules/classnames/index.js ***!
  \*****************************************************************************************************************************************/
/***/ (function(module, exports) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*!
	Copyright (c) 2018 Jed Watson.
	Licensed under the MIT License (MIT), see
	http://jedwatson.github.io/classnames
*/
/* global define */

(function () {
	'use strict';

	var hasOwn = {}.hasOwnProperty;
	var nativeCodeString = '[native code]';

	function classNames() {
		var classes = [];

		for (var i = 0; i < arguments.length; i++) {
			var arg = arguments[i];
			if (!arg) continue;

			var argType = typeof arg;

			if (argType === 'string' || argType === 'number') {
				classes.push(arg);
			} else if (Array.isArray(arg)) {
				if (arg.length) {
					var inner = classNames.apply(null, arg);
					if (inner) {
						classes.push(inner);
					}
				}
			} else if (argType === 'object') {
				if (arg.toString !== Object.prototype.toString && !arg.toString.toString().includes('[native code]')) {
					classes.push(arg.toString());
					continue;
				}

				for (var key in arg) {
					if (hasOwn.call(arg, key) && arg[key]) {
						classes.push(key);
					}
				}
			}
		}

		return classes.join(' ');
	}

	if ( true && module.exports) {
		classNames.default = classNames;
		module.exports = classNames;
	} else if (true) {
		// register as 'classnames', consistent with npm package name
		!(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_RESULT__ = (function () {
			return classNames;
		}).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
		__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	} else {}
}());


/***/ }),

/***/ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_Symbol.js":
/*!*************************************************************************************************************************************!*\
  !*** ../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_Symbol.js ***!
  \*************************************************************************************************************************************/
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

var root = __webpack_require__(/*! ./_root */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_root.js");

/** Built-in value references. */
var Symbol = root.Symbol;

module.exports = Symbol;


/***/ }),

/***/ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_apply.js":
/*!************************************************************************************************************************************!*\
  !*** ../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_apply.js ***!
  \************************************************************************************************************************************/
/***/ (function(module) {

/**
 * A faster alternative to `Function#apply`, this function invokes `func`
 * with the `this` binding of `thisArg` and the arguments of `args`.
 *
 * @private
 * @param {Function} func The function to invoke.
 * @param {*} thisArg The `this` binding of `func`.
 * @param {Array} args The arguments to invoke `func` with.
 * @returns {*} Returns the result of `func`.
 */
function apply(func, thisArg, args) {
  switch (args.length) {
    case 0: return func.call(thisArg);
    case 1: return func.call(thisArg, args[0]);
    case 2: return func.call(thisArg, args[0], args[1]);
    case 3: return func.call(thisArg, args[0], args[1], args[2]);
  }
  return func.apply(thisArg, args);
}

module.exports = apply;


/***/ }),

/***/ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_arrayLikeKeys.js":
/*!********************************************************************************************************************************************!*\
  !*** ../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_arrayLikeKeys.js ***!
  \********************************************************************************************************************************************/
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

var baseTimes = __webpack_require__(/*! ./_baseTimes */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_baseTimes.js"),
    isArguments = __webpack_require__(/*! ./isArguments */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/isArguments.js"),
    isArray = __webpack_require__(/*! ./isArray */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/isArray.js"),
    isBuffer = __webpack_require__(/*! ./isBuffer */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/isBuffer.js"),
    isIndex = __webpack_require__(/*! ./_isIndex */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_isIndex.js"),
    isTypedArray = __webpack_require__(/*! ./isTypedArray */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/isTypedArray.js");

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * Creates an array of the enumerable property names of the array-like `value`.
 *
 * @private
 * @param {*} value The value to query.
 * @param {boolean} inherited Specify returning inherited property names.
 * @returns {Array} Returns the array of property names.
 */
function arrayLikeKeys(value, inherited) {
  var isArr = isArray(value),
      isArg = !isArr && isArguments(value),
      isBuff = !isArr && !isArg && isBuffer(value),
      isType = !isArr && !isArg && !isBuff && isTypedArray(value),
      skipIndexes = isArr || isArg || isBuff || isType,
      result = skipIndexes ? baseTimes(value.length, String) : [],
      length = result.length;

  for (var key in value) {
    if ((inherited || hasOwnProperty.call(value, key)) &&
        !(skipIndexes && (
           // Safari 9 has enumerable `arguments.length` in strict mode.
           key == 'length' ||
           // Node.js 0.10 has enumerable non-index properties on buffers.
           (isBuff && (key == 'offset' || key == 'parent')) ||
           // PhantomJS 2 has enumerable non-index properties on typed arrays.
           (isType && (key == 'buffer' || key == 'byteLength' || key == 'byteOffset')) ||
           // Skip index properties.
           isIndex(key, length)
        ))) {
      result.push(key);
    }
  }
  return result;
}

module.exports = arrayLikeKeys;


/***/ }),

/***/ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_assignValue.js":
/*!******************************************************************************************************************************************!*\
  !*** ../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_assignValue.js ***!
  \******************************************************************************************************************************************/
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

var baseAssignValue = __webpack_require__(/*! ./_baseAssignValue */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_baseAssignValue.js"),
    eq = __webpack_require__(/*! ./eq */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/eq.js");

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * Assigns `value` to `key` of `object` if the existing value is not equivalent
 * using [`SameValueZero`](http://ecma-international.org/ecma-262/7.0/#sec-samevaluezero)
 * for equality comparisons.
 *
 * @private
 * @param {Object} object The object to modify.
 * @param {string} key The key of the property to assign.
 * @param {*} value The value to assign.
 */
function assignValue(object, key, value) {
  var objValue = object[key];
  if (!(hasOwnProperty.call(object, key) && eq(objValue, value)) ||
      (value === undefined && !(key in object))) {
    baseAssignValue(object, key, value);
  }
}

module.exports = assignValue;


/***/ }),

/***/ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_baseAssignValue.js":
/*!**********************************************************************************************************************************************!*\
  !*** ../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_baseAssignValue.js ***!
  \**********************************************************************************************************************************************/
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

var defineProperty = __webpack_require__(/*! ./_defineProperty */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_defineProperty.js");

/**
 * The base implementation of `assignValue` and `assignMergeValue` without
 * value checks.
 *
 * @private
 * @param {Object} object The object to modify.
 * @param {string} key The key of the property to assign.
 * @param {*} value The value to assign.
 */
function baseAssignValue(object, key, value) {
  if (key == '__proto__' && defineProperty) {
    defineProperty(object, key, {
      'configurable': true,
      'enumerable': true,
      'value': value,
      'writable': true
    });
  } else {
    object[key] = value;
  }
}

module.exports = baseAssignValue;


/***/ }),

/***/ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_baseGetTag.js":
/*!*****************************************************************************************************************************************!*\
  !*** ../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_baseGetTag.js ***!
  \*****************************************************************************************************************************************/
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

var Symbol = __webpack_require__(/*! ./_Symbol */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_Symbol.js"),
    getRawTag = __webpack_require__(/*! ./_getRawTag */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_getRawTag.js"),
    objectToString = __webpack_require__(/*! ./_objectToString */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_objectToString.js");

/** `Object#toString` result references. */
var nullTag = '[object Null]',
    undefinedTag = '[object Undefined]';

/** Built-in value references. */
var symToStringTag = Symbol ? Symbol.toStringTag : undefined;

/**
 * The base implementation of `getTag` without fallbacks for buggy environments.
 *
 * @private
 * @param {*} value The value to query.
 * @returns {string} Returns the `toStringTag`.
 */
function baseGetTag(value) {
  if (value == null) {
    return value === undefined ? undefinedTag : nullTag;
  }
  return (symToStringTag && symToStringTag in Object(value))
    ? getRawTag(value)
    : objectToString(value);
}

module.exports = baseGetTag;


/***/ }),

/***/ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_baseIsArguments.js":
/*!**********************************************************************************************************************************************!*\
  !*** ../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_baseIsArguments.js ***!
  \**********************************************************************************************************************************************/
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

var baseGetTag = __webpack_require__(/*! ./_baseGetTag */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_baseGetTag.js"),
    isObjectLike = __webpack_require__(/*! ./isObjectLike */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/isObjectLike.js");

/** `Object#toString` result references. */
var argsTag = '[object Arguments]';

/**
 * The base implementation of `_.isArguments`.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an `arguments` object,
 */
function baseIsArguments(value) {
  return isObjectLike(value) && baseGetTag(value) == argsTag;
}

module.exports = baseIsArguments;


/***/ }),

/***/ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_baseIsNative.js":
/*!*******************************************************************************************************************************************!*\
  !*** ../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_baseIsNative.js ***!
  \*******************************************************************************************************************************************/
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

var isFunction = __webpack_require__(/*! ./isFunction */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/isFunction.js"),
    isMasked = __webpack_require__(/*! ./_isMasked */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_isMasked.js"),
    isObject = __webpack_require__(/*! ./isObject */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/isObject.js"),
    toSource = __webpack_require__(/*! ./_toSource */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_toSource.js");

/**
 * Used to match `RegExp`
 * [syntax characters](http://ecma-international.org/ecma-262/7.0/#sec-patterns).
 */
var reRegExpChar = /[\\^$.*+?()[\]{}|]/g;

/** Used to detect host constructors (Safari). */
var reIsHostCtor = /^\[object .+?Constructor\]$/;

/** Used for built-in method references. */
var funcProto = Function.prototype,
    objectProto = Object.prototype;

/** Used to resolve the decompiled source of functions. */
var funcToString = funcProto.toString;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/** Used to detect if a method is native. */
var reIsNative = RegExp('^' +
  funcToString.call(hasOwnProperty).replace(reRegExpChar, '\\$&')
  .replace(/hasOwnProperty|(function).*?(?=\\\()| for .+?(?=\\\])/g, '$1.*?') + '$'
);

/**
 * The base implementation of `_.isNative` without bad shim checks.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a native function,
 *  else `false`.
 */
function baseIsNative(value) {
  if (!isObject(value) || isMasked(value)) {
    return false;
  }
  var pattern = isFunction(value) ? reIsNative : reIsHostCtor;
  return pattern.test(toSource(value));
}

module.exports = baseIsNative;


/***/ }),

/***/ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_baseIsTypedArray.js":
/*!***********************************************************************************************************************************************!*\
  !*** ../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_baseIsTypedArray.js ***!
  \***********************************************************************************************************************************************/
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

var baseGetTag = __webpack_require__(/*! ./_baseGetTag */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_baseGetTag.js"),
    isLength = __webpack_require__(/*! ./isLength */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/isLength.js"),
    isObjectLike = __webpack_require__(/*! ./isObjectLike */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/isObjectLike.js");

/** `Object#toString` result references. */
var argsTag = '[object Arguments]',
    arrayTag = '[object Array]',
    boolTag = '[object Boolean]',
    dateTag = '[object Date]',
    errorTag = '[object Error]',
    funcTag = '[object Function]',
    mapTag = '[object Map]',
    numberTag = '[object Number]',
    objectTag = '[object Object]',
    regexpTag = '[object RegExp]',
    setTag = '[object Set]',
    stringTag = '[object String]',
    weakMapTag = '[object WeakMap]';

var arrayBufferTag = '[object ArrayBuffer]',
    dataViewTag = '[object DataView]',
    float32Tag = '[object Float32Array]',
    float64Tag = '[object Float64Array]',
    int8Tag = '[object Int8Array]',
    int16Tag = '[object Int16Array]',
    int32Tag = '[object Int32Array]',
    uint8Tag = '[object Uint8Array]',
    uint8ClampedTag = '[object Uint8ClampedArray]',
    uint16Tag = '[object Uint16Array]',
    uint32Tag = '[object Uint32Array]';

/** Used to identify `toStringTag` values of typed arrays. */
var typedArrayTags = {};
typedArrayTags[float32Tag] = typedArrayTags[float64Tag] =
typedArrayTags[int8Tag] = typedArrayTags[int16Tag] =
typedArrayTags[int32Tag] = typedArrayTags[uint8Tag] =
typedArrayTags[uint8ClampedTag] = typedArrayTags[uint16Tag] =
typedArrayTags[uint32Tag] = true;
typedArrayTags[argsTag] = typedArrayTags[arrayTag] =
typedArrayTags[arrayBufferTag] = typedArrayTags[boolTag] =
typedArrayTags[dataViewTag] = typedArrayTags[dateTag] =
typedArrayTags[errorTag] = typedArrayTags[funcTag] =
typedArrayTags[mapTag] = typedArrayTags[numberTag] =
typedArrayTags[objectTag] = typedArrayTags[regexpTag] =
typedArrayTags[setTag] = typedArrayTags[stringTag] =
typedArrayTags[weakMapTag] = false;

/**
 * The base implementation of `_.isTypedArray` without Node.js optimizations.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a typed array, else `false`.
 */
function baseIsTypedArray(value) {
  return isObjectLike(value) &&
    isLength(value.length) && !!typedArrayTags[baseGetTag(value)];
}

module.exports = baseIsTypedArray;


/***/ }),

/***/ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_baseKeys.js":
/*!***************************************************************************************************************************************!*\
  !*** ../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_baseKeys.js ***!
  \***************************************************************************************************************************************/
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

var isPrototype = __webpack_require__(/*! ./_isPrototype */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_isPrototype.js"),
    nativeKeys = __webpack_require__(/*! ./_nativeKeys */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_nativeKeys.js");

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * The base implementation of `_.keys` which doesn't treat sparse arrays as dense.
 *
 * @private
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of property names.
 */
function baseKeys(object) {
  if (!isPrototype(object)) {
    return nativeKeys(object);
  }
  var result = [];
  for (var key in Object(object)) {
    if (hasOwnProperty.call(object, key) && key != 'constructor') {
      result.push(key);
    }
  }
  return result;
}

module.exports = baseKeys;


/***/ }),

/***/ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_baseRest.js":
/*!***************************************************************************************************************************************!*\
  !*** ../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_baseRest.js ***!
  \***************************************************************************************************************************************/
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

var identity = __webpack_require__(/*! ./identity */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/identity.js"),
    overRest = __webpack_require__(/*! ./_overRest */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_overRest.js"),
    setToString = __webpack_require__(/*! ./_setToString */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_setToString.js");

/**
 * The base implementation of `_.rest` which doesn't validate or coerce arguments.
 *
 * @private
 * @param {Function} func The function to apply a rest parameter to.
 * @param {number} [start=func.length-1] The start position of the rest parameter.
 * @returns {Function} Returns the new function.
 */
function baseRest(func, start) {
  return setToString(overRest(func, start, identity), func + '');
}

module.exports = baseRest;


/***/ }),

/***/ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_baseSetToString.js":
/*!**********************************************************************************************************************************************!*\
  !*** ../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_baseSetToString.js ***!
  \**********************************************************************************************************************************************/
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

var constant = __webpack_require__(/*! ./constant */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/constant.js"),
    defineProperty = __webpack_require__(/*! ./_defineProperty */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_defineProperty.js"),
    identity = __webpack_require__(/*! ./identity */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/identity.js");

/**
 * The base implementation of `setToString` without support for hot loop shorting.
 *
 * @private
 * @param {Function} func The function to modify.
 * @param {Function} string The `toString` result.
 * @returns {Function} Returns `func`.
 */
var baseSetToString = !defineProperty ? identity : function(func, string) {
  return defineProperty(func, 'toString', {
    'configurable': true,
    'enumerable': false,
    'value': constant(string),
    'writable': true
  });
};

module.exports = baseSetToString;


/***/ }),

/***/ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_baseTimes.js":
/*!****************************************************************************************************************************************!*\
  !*** ../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_baseTimes.js ***!
  \****************************************************************************************************************************************/
/***/ (function(module) {

/**
 * The base implementation of `_.times` without support for iteratee shorthands
 * or max array length checks.
 *
 * @private
 * @param {number} n The number of times to invoke `iteratee`.
 * @param {Function} iteratee The function invoked per iteration.
 * @returns {Array} Returns the array of results.
 */
function baseTimes(n, iteratee) {
  var index = -1,
      result = Array(n);

  while (++index < n) {
    result[index] = iteratee(index);
  }
  return result;
}

module.exports = baseTimes;


/***/ }),

/***/ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_baseUnary.js":
/*!****************************************************************************************************************************************!*\
  !*** ../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_baseUnary.js ***!
  \****************************************************************************************************************************************/
/***/ (function(module) {

/**
 * The base implementation of `_.unary` without support for storing metadata.
 *
 * @private
 * @param {Function} func The function to cap arguments for.
 * @returns {Function} Returns the new capped function.
 */
function baseUnary(func) {
  return function(value) {
    return func(value);
  };
}

module.exports = baseUnary;


/***/ }),

/***/ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_copyObject.js":
/*!*****************************************************************************************************************************************!*\
  !*** ../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_copyObject.js ***!
  \*****************************************************************************************************************************************/
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

var assignValue = __webpack_require__(/*! ./_assignValue */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_assignValue.js"),
    baseAssignValue = __webpack_require__(/*! ./_baseAssignValue */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_baseAssignValue.js");

/**
 * Copies properties of `source` to `object`.
 *
 * @private
 * @param {Object} source The object to copy properties from.
 * @param {Array} props The property identifiers to copy.
 * @param {Object} [object={}] The object to copy properties to.
 * @param {Function} [customizer] The function to customize copied values.
 * @returns {Object} Returns `object`.
 */
function copyObject(source, props, object, customizer) {
  var isNew = !object;
  object || (object = {});

  var index = -1,
      length = props.length;

  while (++index < length) {
    var key = props[index];

    var newValue = customizer
      ? customizer(object[key], source[key], key, object, source)
      : undefined;

    if (newValue === undefined) {
      newValue = source[key];
    }
    if (isNew) {
      baseAssignValue(object, key, newValue);
    } else {
      assignValue(object, key, newValue);
    }
  }
  return object;
}

module.exports = copyObject;


/***/ }),

/***/ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_coreJsData.js":
/*!*****************************************************************************************************************************************!*\
  !*** ../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_coreJsData.js ***!
  \*****************************************************************************************************************************************/
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

var root = __webpack_require__(/*! ./_root */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_root.js");

/** Used to detect overreaching core-js shims. */
var coreJsData = root['__core-js_shared__'];

module.exports = coreJsData;


/***/ }),

/***/ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_createAssigner.js":
/*!*********************************************************************************************************************************************!*\
  !*** ../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_createAssigner.js ***!
  \*********************************************************************************************************************************************/
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

var baseRest = __webpack_require__(/*! ./_baseRest */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_baseRest.js"),
    isIterateeCall = __webpack_require__(/*! ./_isIterateeCall */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_isIterateeCall.js");

/**
 * Creates a function like `_.assign`.
 *
 * @private
 * @param {Function} assigner The function to assign values.
 * @returns {Function} Returns the new assigner function.
 */
function createAssigner(assigner) {
  return baseRest(function(object, sources) {
    var index = -1,
        length = sources.length,
        customizer = length > 1 ? sources[length - 1] : undefined,
        guard = length > 2 ? sources[2] : undefined;

    customizer = (assigner.length > 3 && typeof customizer == 'function')
      ? (length--, customizer)
      : undefined;

    if (guard && isIterateeCall(sources[0], sources[1], guard)) {
      customizer = length < 3 ? undefined : customizer;
      length = 1;
    }
    object = Object(object);
    while (++index < length) {
      var source = sources[index];
      if (source) {
        assigner(object, source, index, customizer);
      }
    }
    return object;
  });
}

module.exports = createAssigner;


/***/ }),

/***/ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_defineProperty.js":
/*!*********************************************************************************************************************************************!*\
  !*** ../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_defineProperty.js ***!
  \*********************************************************************************************************************************************/
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

var getNative = __webpack_require__(/*! ./_getNative */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_getNative.js");

var defineProperty = (function() {
  try {
    var func = getNative(Object, 'defineProperty');
    func({}, '', {});
    return func;
  } catch (e) {}
}());

module.exports = defineProperty;


/***/ }),

/***/ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_freeGlobal.js":
/*!*****************************************************************************************************************************************!*\
  !*** ../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_freeGlobal.js ***!
  \*****************************************************************************************************************************************/
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

/** Detect free variable `global` from Node.js. */
var freeGlobal = typeof __webpack_require__.g == 'object' && __webpack_require__.g && __webpack_require__.g.Object === Object && __webpack_require__.g;

module.exports = freeGlobal;


/***/ }),

/***/ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_getNative.js":
/*!****************************************************************************************************************************************!*\
  !*** ../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_getNative.js ***!
  \****************************************************************************************************************************************/
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

var baseIsNative = __webpack_require__(/*! ./_baseIsNative */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_baseIsNative.js"),
    getValue = __webpack_require__(/*! ./_getValue */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_getValue.js");

/**
 * Gets the native function at `key` of `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @param {string} key The key of the method to get.
 * @returns {*} Returns the function if it's native, else `undefined`.
 */
function getNative(object, key) {
  var value = getValue(object, key);
  return baseIsNative(value) ? value : undefined;
}

module.exports = getNative;


/***/ }),

/***/ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_getRawTag.js":
/*!****************************************************************************************************************************************!*\
  !*** ../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_getRawTag.js ***!
  \****************************************************************************************************************************************/
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

var Symbol = __webpack_require__(/*! ./_Symbol */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_Symbol.js");

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * Used to resolve the
 * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
 * of values.
 */
var nativeObjectToString = objectProto.toString;

/** Built-in value references. */
var symToStringTag = Symbol ? Symbol.toStringTag : undefined;

/**
 * A specialized version of `baseGetTag` which ignores `Symbol.toStringTag` values.
 *
 * @private
 * @param {*} value The value to query.
 * @returns {string} Returns the raw `toStringTag`.
 */
function getRawTag(value) {
  var isOwn = hasOwnProperty.call(value, symToStringTag),
      tag = value[symToStringTag];

  try {
    value[symToStringTag] = undefined;
    var unmasked = true;
  } catch (e) {}

  var result = nativeObjectToString.call(value);
  if (unmasked) {
    if (isOwn) {
      value[symToStringTag] = tag;
    } else {
      delete value[symToStringTag];
    }
  }
  return result;
}

module.exports = getRawTag;


/***/ }),

/***/ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_getValue.js":
/*!***************************************************************************************************************************************!*\
  !*** ../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_getValue.js ***!
  \***************************************************************************************************************************************/
/***/ (function(module) {

/**
 * Gets the value at `key` of `object`.
 *
 * @private
 * @param {Object} [object] The object to query.
 * @param {string} key The key of the property to get.
 * @returns {*} Returns the property value.
 */
function getValue(object, key) {
  return object == null ? undefined : object[key];
}

module.exports = getValue;


/***/ }),

/***/ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_isIndex.js":
/*!**************************************************************************************************************************************!*\
  !*** ../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_isIndex.js ***!
  \**************************************************************************************************************************************/
/***/ (function(module) {

/** Used as references for various `Number` constants. */
var MAX_SAFE_INTEGER = 9007199254740991;

/** Used to detect unsigned integer values. */
var reIsUint = /^(?:0|[1-9]\d*)$/;

/**
 * Checks if `value` is a valid array-like index.
 *
 * @private
 * @param {*} value The value to check.
 * @param {number} [length=MAX_SAFE_INTEGER] The upper bounds of a valid index.
 * @returns {boolean} Returns `true` if `value` is a valid index, else `false`.
 */
function isIndex(value, length) {
  var type = typeof value;
  length = length == null ? MAX_SAFE_INTEGER : length;

  return !!length &&
    (type == 'number' ||
      (type != 'symbol' && reIsUint.test(value))) &&
        (value > -1 && value % 1 == 0 && value < length);
}

module.exports = isIndex;


/***/ }),

/***/ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_isIterateeCall.js":
/*!*********************************************************************************************************************************************!*\
  !*** ../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_isIterateeCall.js ***!
  \*********************************************************************************************************************************************/
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

var eq = __webpack_require__(/*! ./eq */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/eq.js"),
    isArrayLike = __webpack_require__(/*! ./isArrayLike */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/isArrayLike.js"),
    isIndex = __webpack_require__(/*! ./_isIndex */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_isIndex.js"),
    isObject = __webpack_require__(/*! ./isObject */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/isObject.js");

/**
 * Checks if the given arguments are from an iteratee call.
 *
 * @private
 * @param {*} value The potential iteratee value argument.
 * @param {*} index The potential iteratee index or key argument.
 * @param {*} object The potential iteratee object argument.
 * @returns {boolean} Returns `true` if the arguments are from an iteratee call,
 *  else `false`.
 */
function isIterateeCall(value, index, object) {
  if (!isObject(object)) {
    return false;
  }
  var type = typeof index;
  if (type == 'number'
        ? (isArrayLike(object) && isIndex(index, object.length))
        : (type == 'string' && index in object)
      ) {
    return eq(object[index], value);
  }
  return false;
}

module.exports = isIterateeCall;


/***/ }),

/***/ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_isMasked.js":
/*!***************************************************************************************************************************************!*\
  !*** ../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_isMasked.js ***!
  \***************************************************************************************************************************************/
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

var coreJsData = __webpack_require__(/*! ./_coreJsData */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_coreJsData.js");

/** Used to detect methods masquerading as native. */
var maskSrcKey = (function() {
  var uid = /[^.]+$/.exec(coreJsData && coreJsData.keys && coreJsData.keys.IE_PROTO || '');
  return uid ? ('Symbol(src)_1.' + uid) : '';
}());

/**
 * Checks if `func` has its source masked.
 *
 * @private
 * @param {Function} func The function to check.
 * @returns {boolean} Returns `true` if `func` is masked, else `false`.
 */
function isMasked(func) {
  return !!maskSrcKey && (maskSrcKey in func);
}

module.exports = isMasked;


/***/ }),

/***/ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_isPrototype.js":
/*!******************************************************************************************************************************************!*\
  !*** ../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_isPrototype.js ***!
  \******************************************************************************************************************************************/
/***/ (function(module) {

/** Used for built-in method references. */
var objectProto = Object.prototype;

/**
 * Checks if `value` is likely a prototype object.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a prototype, else `false`.
 */
function isPrototype(value) {
  var Ctor = value && value.constructor,
      proto = (typeof Ctor == 'function' && Ctor.prototype) || objectProto;

  return value === proto;
}

module.exports = isPrototype;


/***/ }),

/***/ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_nativeKeys.js":
/*!*****************************************************************************************************************************************!*\
  !*** ../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_nativeKeys.js ***!
  \*****************************************************************************************************************************************/
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

var overArg = __webpack_require__(/*! ./_overArg */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_overArg.js");

/* Built-in method references for those with the same name as other `lodash` methods. */
var nativeKeys = overArg(Object.keys, Object);

module.exports = nativeKeys;


/***/ }),

/***/ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_nodeUtil.js":
/*!***************************************************************************************************************************************!*\
  !*** ../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_nodeUtil.js ***!
  \***************************************************************************************************************************************/
/***/ (function(module, exports, __webpack_require__) {

/* module decorator */ module = __webpack_require__.nmd(module);
var freeGlobal = __webpack_require__(/*! ./_freeGlobal */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_freeGlobal.js");

/** Detect free variable `exports`. */
var freeExports =  true && exports && !exports.nodeType && exports;

/** Detect free variable `module`. */
var freeModule = freeExports && "object" == 'object' && module && !module.nodeType && module;

/** Detect the popular CommonJS extension `module.exports`. */
var moduleExports = freeModule && freeModule.exports === freeExports;

/** Detect free variable `process` from Node.js. */
var freeProcess = moduleExports && freeGlobal.process;

/** Used to access faster Node.js helpers. */
var nodeUtil = (function() {
  try {
    // Use `util.types` for Node.js 10+.
    var types = freeModule && freeModule.require && freeModule.require('util').types;

    if (types) {
      return types;
    }

    // Legacy `process.binding('util')` for Node.js < 10.
    return freeProcess && freeProcess.binding && freeProcess.binding('util');
  } catch (e) {}
}());

module.exports = nodeUtil;


/***/ }),

/***/ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_objectToString.js":
/*!*********************************************************************************************************************************************!*\
  !*** ../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_objectToString.js ***!
  \*********************************************************************************************************************************************/
/***/ (function(module) {

/** Used for built-in method references. */
var objectProto = Object.prototype;

/**
 * Used to resolve the
 * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
 * of values.
 */
var nativeObjectToString = objectProto.toString;

/**
 * Converts `value` to a string using `Object.prototype.toString`.
 *
 * @private
 * @param {*} value The value to convert.
 * @returns {string} Returns the converted string.
 */
function objectToString(value) {
  return nativeObjectToString.call(value);
}

module.exports = objectToString;


/***/ }),

/***/ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_overArg.js":
/*!**************************************************************************************************************************************!*\
  !*** ../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_overArg.js ***!
  \**************************************************************************************************************************************/
/***/ (function(module) {

/**
 * Creates a unary function that invokes `func` with its argument transformed.
 *
 * @private
 * @param {Function} func The function to wrap.
 * @param {Function} transform The argument transform.
 * @returns {Function} Returns the new function.
 */
function overArg(func, transform) {
  return function(arg) {
    return func(transform(arg));
  };
}

module.exports = overArg;


/***/ }),

/***/ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_overRest.js":
/*!***************************************************************************************************************************************!*\
  !*** ../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_overRest.js ***!
  \***************************************************************************************************************************************/
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

var apply = __webpack_require__(/*! ./_apply */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_apply.js");

/* Built-in method references for those with the same name as other `lodash` methods. */
var nativeMax = Math.max;

/**
 * A specialized version of `baseRest` which transforms the rest array.
 *
 * @private
 * @param {Function} func The function to apply a rest parameter to.
 * @param {number} [start=func.length-1] The start position of the rest parameter.
 * @param {Function} transform The rest array transform.
 * @returns {Function} Returns the new function.
 */
function overRest(func, start, transform) {
  start = nativeMax(start === undefined ? (func.length - 1) : start, 0);
  return function() {
    var args = arguments,
        index = -1,
        length = nativeMax(args.length - start, 0),
        array = Array(length);

    while (++index < length) {
      array[index] = args[start + index];
    }
    index = -1;
    var otherArgs = Array(start + 1);
    while (++index < start) {
      otherArgs[index] = args[index];
    }
    otherArgs[start] = transform(array);
    return apply(func, this, otherArgs);
  };
}

module.exports = overRest;


/***/ }),

/***/ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_root.js":
/*!***********************************************************************************************************************************!*\
  !*** ../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_root.js ***!
  \***********************************************************************************************************************************/
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

var freeGlobal = __webpack_require__(/*! ./_freeGlobal */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_freeGlobal.js");

/** Detect free variable `self`. */
var freeSelf = typeof self == 'object' && self && self.Object === Object && self;

/** Used as a reference to the global object. */
var root = freeGlobal || freeSelf || Function('return this')();

module.exports = root;


/***/ }),

/***/ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_setToString.js":
/*!******************************************************************************************************************************************!*\
  !*** ../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_setToString.js ***!
  \******************************************************************************************************************************************/
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

var baseSetToString = __webpack_require__(/*! ./_baseSetToString */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_baseSetToString.js"),
    shortOut = __webpack_require__(/*! ./_shortOut */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_shortOut.js");

/**
 * Sets the `toString` method of `func` to return `string`.
 *
 * @private
 * @param {Function} func The function to modify.
 * @param {Function} string The `toString` result.
 * @returns {Function} Returns `func`.
 */
var setToString = shortOut(baseSetToString);

module.exports = setToString;


/***/ }),

/***/ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_shortOut.js":
/*!***************************************************************************************************************************************!*\
  !*** ../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_shortOut.js ***!
  \***************************************************************************************************************************************/
/***/ (function(module) {

/** Used to detect hot functions by number of calls within a span of milliseconds. */
var HOT_COUNT = 800,
    HOT_SPAN = 16;

/* Built-in method references for those with the same name as other `lodash` methods. */
var nativeNow = Date.now;

/**
 * Creates a function that'll short out and invoke `identity` instead
 * of `func` when it's called `HOT_COUNT` or more times in `HOT_SPAN`
 * milliseconds.
 *
 * @private
 * @param {Function} func The function to restrict.
 * @returns {Function} Returns the new shortable function.
 */
function shortOut(func) {
  var count = 0,
      lastCalled = 0;

  return function() {
    var stamp = nativeNow(),
        remaining = HOT_SPAN - (stamp - lastCalled);

    lastCalled = stamp;
    if (remaining > 0) {
      if (++count >= HOT_COUNT) {
        return arguments[0];
      }
    } else {
      count = 0;
    }
    return func.apply(undefined, arguments);
  };
}

module.exports = shortOut;


/***/ }),

/***/ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_toSource.js":
/*!***************************************************************************************************************************************!*\
  !*** ../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_toSource.js ***!
  \***************************************************************************************************************************************/
/***/ (function(module) {

/** Used for built-in method references. */
var funcProto = Function.prototype;

/** Used to resolve the decompiled source of functions. */
var funcToString = funcProto.toString;

/**
 * Converts `func` to its source code.
 *
 * @private
 * @param {Function} func The function to convert.
 * @returns {string} Returns the source code.
 */
function toSource(func) {
  if (func != null) {
    try {
      return funcToString.call(func);
    } catch (e) {}
    try {
      return (func + '');
    } catch (e) {}
  }
  return '';
}

module.exports = toSource;


/***/ }),

/***/ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/assign.js":
/*!************************************************************************************************************************************!*\
  !*** ../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/assign.js ***!
  \************************************************************************************************************************************/
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

var assignValue = __webpack_require__(/*! ./_assignValue */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_assignValue.js"),
    copyObject = __webpack_require__(/*! ./_copyObject */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_copyObject.js"),
    createAssigner = __webpack_require__(/*! ./_createAssigner */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_createAssigner.js"),
    isArrayLike = __webpack_require__(/*! ./isArrayLike */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/isArrayLike.js"),
    isPrototype = __webpack_require__(/*! ./_isPrototype */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_isPrototype.js"),
    keys = __webpack_require__(/*! ./keys */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/keys.js");

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * Assigns own enumerable string keyed properties of source objects to the
 * destination object. Source objects are applied from left to right.
 * Subsequent sources overwrite property assignments of previous sources.
 *
 * **Note:** This method mutates `object` and is loosely based on
 * [`Object.assign`](https://mdn.io/Object/assign).
 *
 * @static
 * @memberOf _
 * @since 0.10.0
 * @category Object
 * @param {Object} object The destination object.
 * @param {...Object} [sources] The source objects.
 * @returns {Object} Returns `object`.
 * @see _.assignIn
 * @example
 *
 * function Foo() {
 *   this.a = 1;
 * }
 *
 * function Bar() {
 *   this.c = 3;
 * }
 *
 * Foo.prototype.b = 2;
 * Bar.prototype.d = 4;
 *
 * _.assign({ 'a': 0 }, new Foo, new Bar);
 * // => { 'a': 1, 'c': 3 }
 */
var assign = createAssigner(function(object, source) {
  if (isPrototype(source) || isArrayLike(source)) {
    copyObject(source, keys(source), object);
    return;
  }
  for (var key in source) {
    if (hasOwnProperty.call(source, key)) {
      assignValue(object, key, source[key]);
    }
  }
});

module.exports = assign;


/***/ }),

/***/ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/constant.js":
/*!**************************************************************************************************************************************!*\
  !*** ../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/constant.js ***!
  \**************************************************************************************************************************************/
/***/ (function(module) {

/**
 * Creates a function that returns `value`.
 *
 * @static
 * @memberOf _
 * @since 2.4.0
 * @category Util
 * @param {*} value The value to return from the new function.
 * @returns {Function} Returns the new constant function.
 * @example
 *
 * var objects = _.times(2, _.constant({ 'a': 1 }));
 *
 * console.log(objects);
 * // => [{ 'a': 1 }, { 'a': 1 }]
 *
 * console.log(objects[0] === objects[1]);
 * // => true
 */
function constant(value) {
  return function() {
    return value;
  };
}

module.exports = constant;


/***/ }),

/***/ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/eq.js":
/*!********************************************************************************************************************************!*\
  !*** ../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/eq.js ***!
  \********************************************************************************************************************************/
/***/ (function(module) {

/**
 * Performs a
 * [`SameValueZero`](http://ecma-international.org/ecma-262/7.0/#sec-samevaluezero)
 * comparison between two values to determine if they are equivalent.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to compare.
 * @param {*} other The other value to compare.
 * @returns {boolean} Returns `true` if the values are equivalent, else `false`.
 * @example
 *
 * var object = { 'a': 1 };
 * var other = { 'a': 1 };
 *
 * _.eq(object, object);
 * // => true
 *
 * _.eq(object, other);
 * // => false
 *
 * _.eq('a', 'a');
 * // => true
 *
 * _.eq('a', Object('a'));
 * // => false
 *
 * _.eq(NaN, NaN);
 * // => true
 */
function eq(value, other) {
  return value === other || (value !== value && other !== other);
}

module.exports = eq;


/***/ }),

/***/ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/identity.js":
/*!**************************************************************************************************************************************!*\
  !*** ../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/identity.js ***!
  \**************************************************************************************************************************************/
/***/ (function(module) {

/**
 * This method returns the first argument it receives.
 *
 * @static
 * @since 0.1.0
 * @memberOf _
 * @category Util
 * @param {*} value Any value.
 * @returns {*} Returns `value`.
 * @example
 *
 * var object = { 'a': 1 };
 *
 * console.log(_.identity(object) === object);
 * // => true
 */
function identity(value) {
  return value;
}

module.exports = identity;


/***/ }),

/***/ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/isArguments.js":
/*!*****************************************************************************************************************************************!*\
  !*** ../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/isArguments.js ***!
  \*****************************************************************************************************************************************/
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

var baseIsArguments = __webpack_require__(/*! ./_baseIsArguments */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_baseIsArguments.js"),
    isObjectLike = __webpack_require__(/*! ./isObjectLike */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/isObjectLike.js");

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/** Built-in value references. */
var propertyIsEnumerable = objectProto.propertyIsEnumerable;

/**
 * Checks if `value` is likely an `arguments` object.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an `arguments` object,
 *  else `false`.
 * @example
 *
 * _.isArguments(function() { return arguments; }());
 * // => true
 *
 * _.isArguments([1, 2, 3]);
 * // => false
 */
var isArguments = baseIsArguments(function() { return arguments; }()) ? baseIsArguments : function(value) {
  return isObjectLike(value) && hasOwnProperty.call(value, 'callee') &&
    !propertyIsEnumerable.call(value, 'callee');
};

module.exports = isArguments;


/***/ }),

/***/ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/isArray.js":
/*!*************************************************************************************************************************************!*\
  !*** ../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/isArray.js ***!
  \*************************************************************************************************************************************/
/***/ (function(module) {

/**
 * Checks if `value` is classified as an `Array` object.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an array, else `false`.
 * @example
 *
 * _.isArray([1, 2, 3]);
 * // => true
 *
 * _.isArray(document.body.children);
 * // => false
 *
 * _.isArray('abc');
 * // => false
 *
 * _.isArray(_.noop);
 * // => false
 */
var isArray = Array.isArray;

module.exports = isArray;


/***/ }),

/***/ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/isArrayLike.js":
/*!*****************************************************************************************************************************************!*\
  !*** ../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/isArrayLike.js ***!
  \*****************************************************************************************************************************************/
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

var isFunction = __webpack_require__(/*! ./isFunction */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/isFunction.js"),
    isLength = __webpack_require__(/*! ./isLength */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/isLength.js");

/**
 * Checks if `value` is array-like. A value is considered array-like if it's
 * not a function and has a `value.length` that's an integer greater than or
 * equal to `0` and less than or equal to `Number.MAX_SAFE_INTEGER`.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is array-like, else `false`.
 * @example
 *
 * _.isArrayLike([1, 2, 3]);
 * // => true
 *
 * _.isArrayLike(document.body.children);
 * // => true
 *
 * _.isArrayLike('abc');
 * // => true
 *
 * _.isArrayLike(_.noop);
 * // => false
 */
function isArrayLike(value) {
  return value != null && isLength(value.length) && !isFunction(value);
}

module.exports = isArrayLike;


/***/ }),

/***/ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/isBuffer.js":
/*!**************************************************************************************************************************************!*\
  !*** ../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/isBuffer.js ***!
  \**************************************************************************************************************************************/
/***/ (function(module, exports, __webpack_require__) {

/* module decorator */ module = __webpack_require__.nmd(module);
var root = __webpack_require__(/*! ./_root */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_root.js"),
    stubFalse = __webpack_require__(/*! ./stubFalse */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/stubFalse.js");

/** Detect free variable `exports`. */
var freeExports =  true && exports && !exports.nodeType && exports;

/** Detect free variable `module`. */
var freeModule = freeExports && "object" == 'object' && module && !module.nodeType && module;

/** Detect the popular CommonJS extension `module.exports`. */
var moduleExports = freeModule && freeModule.exports === freeExports;

/** Built-in value references. */
var Buffer = moduleExports ? root.Buffer : undefined;

/* Built-in method references for those with the same name as other `lodash` methods. */
var nativeIsBuffer = Buffer ? Buffer.isBuffer : undefined;

/**
 * Checks if `value` is a buffer.
 *
 * @static
 * @memberOf _
 * @since 4.3.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a buffer, else `false`.
 * @example
 *
 * _.isBuffer(new Buffer(2));
 * // => true
 *
 * _.isBuffer(new Uint8Array(2));
 * // => false
 */
var isBuffer = nativeIsBuffer || stubFalse;

module.exports = isBuffer;


/***/ }),

/***/ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/isFunction.js":
/*!****************************************************************************************************************************************!*\
  !*** ../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/isFunction.js ***!
  \****************************************************************************************************************************************/
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

var baseGetTag = __webpack_require__(/*! ./_baseGetTag */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_baseGetTag.js"),
    isObject = __webpack_require__(/*! ./isObject */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/isObject.js");

/** `Object#toString` result references. */
var asyncTag = '[object AsyncFunction]',
    funcTag = '[object Function]',
    genTag = '[object GeneratorFunction]',
    proxyTag = '[object Proxy]';

/**
 * Checks if `value` is classified as a `Function` object.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a function, else `false`.
 * @example
 *
 * _.isFunction(_);
 * // => true
 *
 * _.isFunction(/abc/);
 * // => false
 */
function isFunction(value) {
  if (!isObject(value)) {
    return false;
  }
  // The use of `Object#toString` avoids issues with the `typeof` operator
  // in Safari 9 which returns 'object' for typed arrays and other constructors.
  var tag = baseGetTag(value);
  return tag == funcTag || tag == genTag || tag == asyncTag || tag == proxyTag;
}

module.exports = isFunction;


/***/ }),

/***/ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/isLength.js":
/*!**************************************************************************************************************************************!*\
  !*** ../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/isLength.js ***!
  \**************************************************************************************************************************************/
/***/ (function(module) {

/** Used as references for various `Number` constants. */
var MAX_SAFE_INTEGER = 9007199254740991;

/**
 * Checks if `value` is a valid array-like length.
 *
 * **Note:** This method is loosely based on
 * [`ToLength`](http://ecma-international.org/ecma-262/7.0/#sec-tolength).
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a valid length, else `false`.
 * @example
 *
 * _.isLength(3);
 * // => true
 *
 * _.isLength(Number.MIN_VALUE);
 * // => false
 *
 * _.isLength(Infinity);
 * // => false
 *
 * _.isLength('3');
 * // => false
 */
function isLength(value) {
  return typeof value == 'number' &&
    value > -1 && value % 1 == 0 && value <= MAX_SAFE_INTEGER;
}

module.exports = isLength;


/***/ }),

/***/ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/isObject.js":
/*!**************************************************************************************************************************************!*\
  !*** ../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/isObject.js ***!
  \**************************************************************************************************************************************/
/***/ (function(module) {

/**
 * Checks if `value` is the
 * [language type](http://www.ecma-international.org/ecma-262/7.0/#sec-ecmascript-language-types)
 * of `Object`. (e.g. arrays, functions, objects, regexes, `new Number(0)`, and `new String('')`)
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an object, else `false`.
 * @example
 *
 * _.isObject({});
 * // => true
 *
 * _.isObject([1, 2, 3]);
 * // => true
 *
 * _.isObject(_.noop);
 * // => true
 *
 * _.isObject(null);
 * // => false
 */
function isObject(value) {
  var type = typeof value;
  return value != null && (type == 'object' || type == 'function');
}

module.exports = isObject;


/***/ }),

/***/ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/isObjectLike.js":
/*!******************************************************************************************************************************************!*\
  !*** ../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/isObjectLike.js ***!
  \******************************************************************************************************************************************/
/***/ (function(module) {

/**
 * Checks if `value` is object-like. A value is object-like if it's not `null`
 * and has a `typeof` result of "object".
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is object-like, else `false`.
 * @example
 *
 * _.isObjectLike({});
 * // => true
 *
 * _.isObjectLike([1, 2, 3]);
 * // => true
 *
 * _.isObjectLike(_.noop);
 * // => false
 *
 * _.isObjectLike(null);
 * // => false
 */
function isObjectLike(value) {
  return value != null && typeof value == 'object';
}

module.exports = isObjectLike;


/***/ }),

/***/ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/isTypedArray.js":
/*!******************************************************************************************************************************************!*\
  !*** ../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/isTypedArray.js ***!
  \******************************************************************************************************************************************/
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

var baseIsTypedArray = __webpack_require__(/*! ./_baseIsTypedArray */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_baseIsTypedArray.js"),
    baseUnary = __webpack_require__(/*! ./_baseUnary */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_baseUnary.js"),
    nodeUtil = __webpack_require__(/*! ./_nodeUtil */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_nodeUtil.js");

/* Node.js helper references. */
var nodeIsTypedArray = nodeUtil && nodeUtil.isTypedArray;

/**
 * Checks if `value` is classified as a typed array.
 *
 * @static
 * @memberOf _
 * @since 3.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a typed array, else `false`.
 * @example
 *
 * _.isTypedArray(new Uint8Array);
 * // => true
 *
 * _.isTypedArray([]);
 * // => false
 */
var isTypedArray = nodeIsTypedArray ? baseUnary(nodeIsTypedArray) : baseIsTypedArray;

module.exports = isTypedArray;


/***/ }),

/***/ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/keys.js":
/*!**********************************************************************************************************************************!*\
  !*** ../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/keys.js ***!
  \**********************************************************************************************************************************/
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

var arrayLikeKeys = __webpack_require__(/*! ./_arrayLikeKeys */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_arrayLikeKeys.js"),
    baseKeys = __webpack_require__(/*! ./_baseKeys */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/_baseKeys.js"),
    isArrayLike = __webpack_require__(/*! ./isArrayLike */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/isArrayLike.js");

/**
 * Creates an array of the own enumerable property names of `object`.
 *
 * **Note:** Non-object values are coerced to objects. See the
 * [ES spec](http://ecma-international.org/ecma-262/7.0/#sec-object.keys)
 * for more details.
 *
 * @static
 * @since 0.1.0
 * @memberOf _
 * @category Object
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of property names.
 * @example
 *
 * function Foo() {
 *   this.a = 1;
 *   this.b = 2;
 * }
 *
 * Foo.prototype.c = 3;
 *
 * _.keys(new Foo);
 * // => ['a', 'b'] (iteration order is not guaranteed)
 *
 * _.keys('hi');
 * // => ['0', '1']
 */
function keys(object) {
  return isArrayLike(object) ? arrayLikeKeys(object) : baseKeys(object);
}

module.exports = keys;


/***/ }),

/***/ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/stubFalse.js":
/*!***************************************************************************************************************************************!*\
  !*** ../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/stubFalse.js ***!
  \***************************************************************************************************************************************/
/***/ (function(module) {

/**
 * This method returns `false`.
 *
 * @static
 * @memberOf _
 * @since 4.13.0
 * @category Util
 * @returns {boolean} Returns `false`.
 * @example
 *
 * _.times(2, _.stubFalse);
 * // => [false, false]
 */
function stubFalse() {
  return false;
}

module.exports = stubFalse;


/***/ }),

/***/ "./src/hoc/animations/index.js":
/*!*************************************!*\
  !*** ./src/hoc/animations/index.js ***!
  \*************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/extends */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/@babel-runtime-npm-7.21.0-c4ef698c89-10.zip/node_modules/@babel/runtime/helpers/esm/extends.js");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _wordpress_hooks__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @wordpress/hooks */ "@wordpress/hooks");
/* harmony import */ var _wordpress_hooks__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_wordpress_hooks__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _wordpress_compose__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @wordpress/compose */ "@wordpress/compose");
/* harmony import */ var _wordpress_compose__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_wordpress_compose__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _wordpress_block_editor__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @wordpress/block-editor */ "@wordpress/block-editor");
/* harmony import */ var _wordpress_block_editor__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _wordpress_components__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @wordpress/components */ "@wordpress/components");
/* harmony import */ var _wordpress_components__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_wordpress_components__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @wordpress/i18n */ "@wordpress/i18n");
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_6__);








// Enable style controls on the following blocks
const disableStyleControlOnBlocks = ["lg-blocks/swiper-testimonials", "gravityforms/form"];
const withAnimations = (0,_wordpress_compose__WEBPACK_IMPORTED_MODULE_3__.createHigherOrderComponent)(BlockEdit => {
  return props => {
    if (disableStyleControlOnBlocks.includes(props.name)) {
      return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)(BlockEdit, props);
    }
    const {
      attributes,
      setAttributes,
      isSelected
    } = props;
    const {
      animation
    } = attributes;
    return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)(_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.Fragment, null, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)(BlockEdit, props), isSelected && (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_4__.InspectorControls, null, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)(_wordpress_components__WEBPACK_IMPORTED_MODULE_5__.PanelBody, {
      title: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_6__.__)("Animations", "lg-blocks")
    }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)(_wordpress_components__WEBPACK_IMPORTED_MODULE_5__.SelectControl, {
      label: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_6__.__)("Select Animation", "lg-blocks"),
      value: animation,
      onChange: value => setAttributes({
        animation: value
      }),
      options: [{
        value: "",
        label: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_6__.__)("Select Animation", "lg-blocks")
      }, {
        value: "fadeIn",
        label: "fadeIn"
      }, {
        value: "fadeInUp",
        label: "fadeInUp"
      }, {
        value: "fadeInLeft",
        label: "fadeInLeft"
      }, {
        value: "fadeInRight",
        label: "fadeInRight"
      }]
    }))));
  };
}, "withAnimations");
const addAnimationAttributes = (settings, name) => {
  if (disableStyleControlOnBlocks.includes(name)) {
    return settings;
  }
  if (!settings?.attributes?.animation) {
    settings.attributes = Object.assign(settings.attributes, {
      animation: {
        type: 'string',
        default: ""
      }
    });
  }
  return settings;
};

/**
 * Add custom lg-blocks editor animation attributes to all blocks
 *
 * @param {Function} BlockEdit Original component.
 * @return {string} Wrapped component.
 */
const withAnimationAttributes = (0,_wordpress_compose__WEBPACK_IMPORTED_MODULE_3__.createHigherOrderComponent)(BlockListBlock => {
  return props => {
    if (disableStyleControlOnBlocks.includes(props.name)) {
      return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)(BlockListBlock, props);
    }
    const {
      name,
      attributes
    } = props;
    const {
      animation
    } = attributes;
    let wrapperProps;
    if (animation) {
      wrapperProps = props?.wrapperProps;
      wrapperProps = {
        ...wrapperProps,
        'data-animation': animation
      };
    }
    return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)(BlockListBlock, (0,_babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__["default"])({}, props, {
      wrapperProps: wrapperProps
    }));
  };
}, 'withAnimationAttributes');

/**
 * Override props assigned to save component to inject animation attributes.
 *
 * @param {Object} extraProps Additional props applied to save element.
 * @param {Object} blockType  Block type.
 * @param {Object} attributes Current block attributes.
 *
 * @return {Object} Filtered props applied to save element.
 */
function applyAnimationAttributes(extraProps, blockType, attributes) {
  const {
    animation
  } = attributes;
  if (animation) {
    extraProps = {
      ...extraProps,
      'data-animation': animation
    };
  }
  return extraProps;
}
(0,_wordpress_hooks__WEBPACK_IMPORTED_MODULE_2__.addFilter)("blocks.registerBlockType", "lg-blocks/animations/add-attributes", addAnimationAttributes);
(0,_wordpress_hooks__WEBPACK_IMPORTED_MODULE_2__.addFilter)("editor.BlockEdit", "lg-blocks/animations/with-animations", withAnimations);
(0,_wordpress_hooks__WEBPACK_IMPORTED_MODULE_2__.addFilter)('editor.BlockListBlock', 'lg-blocks/animations/editor-animation-attributes', withAnimationAttributes);
(0,_wordpress_hooks__WEBPACK_IMPORTED_MODULE_2__.addFilter)('blocks.getSaveContent.extraProps', 'lg-blocks/animations/save-animation-attributes', applyAnimationAttributes);

/***/ }),

/***/ "./src/hoc/data-attributes/index.js":
/*!******************************************!*\
  !*** ./src/hoc/data-attributes/index.js ***!
  \******************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _wordpress_compose__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @wordpress/compose */ "@wordpress/compose");
/* harmony import */ var _wordpress_compose__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_wordpress_compose__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _wordpress_block_editor__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @wordpress/block-editor */ "@wordpress/block-editor");
/* harmony import */ var _wordpress_block_editor__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _wordpress_components__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @wordpress/components */ "@wordpress/components");
/* harmony import */ var _wordpress_components__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_wordpress_components__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _wordpress_hooks__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @wordpress/hooks */ "@wordpress/hooks");
/* harmony import */ var _wordpress_hooks__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_wordpress_hooks__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @wordpress/i18n */ "@wordpress/i18n");
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! lodash */ "lodash");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _style_scss__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./style.scss */ "./src/hoc/data-attributes/style.scss");










//restrict to specific block names
const allowedBlocks = ['core/button'];

/**
 * 
 *
 * @param {Object} settings Settings for the block.
 *
 * @return {Object} settings Modified settings.
 */
function addAttributes(settings) {
  //check if object exists for old Gutenberg version compatibility
  //add allowedBlocks restriction
  if (typeof settings.attributes !== 'undefined' && allowedBlocks.includes(settings.name)) {
    settings.attributes = Object.assign(settings.attributes, {
      dataAttributes: {
        type: 'array',
        default: [{
          "name": "",
          "value": ""
        }]
      }
    });
  }
  return settings;
}

/**
 * 
 *
 * @param {function} BlockEdit Block edit component.
 *
 * @return {function} BlockEdit Modified block edit component.
 */
const withAdvancedControls = (0,_wordpress_compose__WEBPACK_IMPORTED_MODULE_1__.createHigherOrderComponent)(BlockEdit => {
  return props => {
    const {
      name,
      attributes,
      setAttributes,
      isSelected
    } = props;
    const {
      dataAttributes
    } = attributes;
    console.log(dataAttributes);
    const onChangeAttributeCount = count => {
      console.log(count);
      console.log(typeof count);
      if (dataAttributes.length !== count) {
        if (dataAttributes.length > count) {
          let newData = [...dataAttributes];
          newData.length = count;
          setAttributes({
            dataAttributes: newData
          });
        }
      }
      if (dataAttributes.length !== count) {
        if (dataAttributes.length < count) {
          let newData = [...dataAttributes];
          newData.push({
            "name": "",
            "value": ""
          });
          setAttributes({
            dataAttributes: newData
          });
        }
      }
    };
    const onChangeAttribute = (value, type, index) => {
      let newData = [...dataAttributes];
      newData[index][type] = value;
      setAttributes({
        dataAttributes: newData
      });
    };
    const renderControls = () => {
      return dataAttributes.map((attribute, index) => {
        return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("div", {
          key: index
        }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_components__WEBPACK_IMPORTED_MODULE_3__.TextControl, {
          label: `Attribute ${index + 1} Name`,
          value: dataAttributes[index].name,
          onChange: v => {
            onChangeAttribute(v, "name", index);
          }
        }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_components__WEBPACK_IMPORTED_MODULE_3__.TextControl, {
          label: `Attribute ${index + 1} Value`,
          value: dataAttributes[index].value,
          onChange: v => {
            onChangeAttribute(v, "value", index);
          }
        }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("hr", null));
      });
    };
    return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(BlockEdit, props), isSelected && allowedBlocks.includes(name) && (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_2__.InspectorControls, null, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_components__WEBPACK_IMPORTED_MODULE_3__.PanelBody, {
      title: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_5__.__)("Data Attributes", "lg-blocks"),
      initialOpen: false
    }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_components__WEBPACK_IMPORTED_MODULE_3__.TextControl, {
      label: "# of Data Attributes",
      value: dataAttributes.length,
      onChange: v => onChangeAttributeCount(parseFloat(v)),
      type: "number"
    }), renderControls())));
  };
}, 'withAdvancedControls');

/**
 * Add custom element class in save element.
 *
 * @param {Object} extraProps     Block element.
 * @param {Object} blockType      Blocks object.
 * @param {Object} attributes     Blocks attributes.
 *
 * @return {Object} extraProps Modified block element.
 */
function applyExtraClass(extraProps, blockType, attributes) {
  const {
    dataAttributes
  } = attributes;
  if (!allowedBlocks.includes(blockType.name)) {
    return extraProps;
  }

  //check if attribute exists for old Gutenberg version compatibility
  if (typeof dataAttributes !== 'undefined') {
    // prevent core/block errors on defaults
    if (dataAttributes.length === 1 && dataAttributes[0].name !== "" && dataAttributes[0].value !== "") {
      dataAttributes.forEach(attribute => {
        (0,lodash__WEBPACK_IMPORTED_MODULE_6__.assign)(extraProps, {
          [attribute.name]: attribute.value
        });
      });
    }
  }
  return extraProps;
}

//add filters

(0,_wordpress_hooks__WEBPACK_IMPORTED_MODULE_4__.addFilter)('blocks.registerBlockType', 'editorskit/custom-attributes', addAttributes);
(0,_wordpress_hooks__WEBPACK_IMPORTED_MODULE_4__.addFilter)('editor.BlockEdit', 'editorskit/custom-advanced-control', withAdvancedControls);
(0,_wordpress_hooks__WEBPACK_IMPORTED_MODULE_4__.addFilter)('blocks.getSaveContent.extraProps', 'editorskit/applyExtraClass', applyExtraClass);

/***/ }),

/***/ "./src/hoc/fontawesome-lists/index.js":
/*!********************************************!*\
  !*** ./src/hoc/fontawesome-lists/index.js ***!
  \********************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _wordpress_compose__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @wordpress/compose */ "@wordpress/compose");
/* harmony import */ var _wordpress_compose__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_wordpress_compose__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _wordpress_block_editor__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @wordpress/block-editor */ "@wordpress/block-editor");
/* harmony import */ var _wordpress_block_editor__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _wordpress_components__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @wordpress/components */ "@wordpress/components");
/* harmony import */ var _wordpress_components__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_wordpress_components__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _wordpress_hooks__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @wordpress/hooks */ "@wordpress/hooks");
/* harmony import */ var _wordpress_hooks__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_wordpress_hooks__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @wordpress/i18n */ "@wordpress/i18n");
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! classnames */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/classnames-npm-2.3.2-d2fdae468d-10.zip/node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _style_scss__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./style.scss */ "./src/hoc/fontawesome-lists/style.scss");










//restrict to specific block names
const allowedBlocks = ['core/list'];

/**
 * Add custom attribute for mobile visibility.
 *
 * @param {Object} settings Settings for the block.
 *
 * @return {Object} settings Modified settings.
 */
function addAttributes(settings) {
  //check if object exists for old Gutenberg version compatibility
  //add allowedBlocks restriction
  if (typeof settings.attributes !== 'undefined' && allowedBlocks.includes(settings.name)) {
    settings.attributes = Object.assign(settings.attributes, {
      markerContent: {
        type: 'string',
        default: "f00c"
      },
      markerFamily: {
        type: 'string',
        default: 'Font Awesome 6 Pro'
      }
    });
  }
  return settings;
}

/**
 * Add mobile visibility controls on Advanced Block Panel.
 *
 * @param {function} BlockEdit Block edit component.
 *
 * @return {function} BlockEdit Modified block edit component.
 */
const withAdvancedControls = (0,_wordpress_compose__WEBPACK_IMPORTED_MODULE_1__.createHigherOrderComponent)(BlockEdit => {
  return props => {
    const {
      name,
      attributes,
      setAttributes,
      isSelected
    } = props;
    const {
      markerContent,
      markerFamily
    } = attributes;
    return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(BlockEdit, props), isSelected && allowedBlocks.includes(name) && (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_2__.InspectorControls, null, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_components__WEBPACK_IMPORTED_MODULE_3__.PanelBody, {
      title: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_5__.__)("FontAwesome", "lg-blocks"),
      initialOpen: false
    }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_components__WEBPACK_IMPORTED_MODULE_3__.TextControl, {
      label: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_5__.__)('Font Family'),
      value: markerFamily,
      onChange: v => setAttributes({
        markerFamily: v
      })
    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_components__WEBPACK_IMPORTED_MODULE_3__.TextControl, {
      label: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_5__.__)('Unicode Character'),
      value: markerContent,
      onChange: v => setAttributes({
        markerContent: v
      })
    }))));
  };
}, 'withAdvancedControls');

/**
 * Add custom element class in save element.
 *
 * @param {Object} extraProps     Block element.
 * @param {Object} blockType      Blocks object.
 * @param {Object} attributes     Blocks attributes.
 *
 * @return {Object} extraProps Modified block element.
 */
function applyExtraClass(extraProps, blockType, attributes) {
  const {
    markerContent,
    markerFamily
  } = attributes;

  //check if attribute exists for old Gutenberg version compatibility
  //add class only when visibleOnMobile = false
  //add allowedBlocks restriction
  if (typeof markerContent !== 'undefined' && markerContent && typeof markerFamily !== 'undefined' && markerFamily && allowedBlocks.includes(blockType.name)) {
    extraProps.style = `--markerContent: '\\${markerContent}'; --markerFamily: '${markerFamily}';`;
  }
  return extraProps;
}

//add filters

(0,_wordpress_hooks__WEBPACK_IMPORTED_MODULE_4__.addFilter)('blocks.registerBlockType', 'editorskit/custom-attributes', addAttributes);
(0,_wordpress_hooks__WEBPACK_IMPORTED_MODULE_4__.addFilter)('editor.BlockEdit', 'editorskit/custom-advanced-control', withAdvancedControls);
(0,_wordpress_hooks__WEBPACK_IMPORTED_MODULE_4__.addFilter)('blocks.getSaveContent.extraProps', 'editorskit/applyExtraClass', applyExtraClass);

/***/ }),

/***/ "./src/hoc/index.js":
/*!**************************!*\
  !*** ./src/hoc/index.js ***!
  \**************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _animations_index__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./animations/index */ "./src/hoc/animations/index.js");
/* harmony import */ var _lightbox_index__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./lightbox/index */ "./src/hoc/lightbox/index.js");
/* harmony import */ var _spacing_controls_index__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./spacing-controls/index */ "./src/hoc/spacing-controls/index.js");
/* harmony import */ var _fontawesome_lists__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./fontawesome-lists */ "./src/hoc/fontawesome-lists/index.js");
/* harmony import */ var _data_attributes__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./data-attributes */ "./src/hoc/data-attributes/index.js");






/***/ }),

/***/ "./src/hoc/lightbox/index.js":
/*!***********************************!*\
  !*** ./src/hoc/lightbox/index.js ***!
  \***********************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/extends */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/@babel-runtime-npm-7.21.0-c4ef698c89-10.zip/node_modules/@babel/runtime/helpers/esm/extends.js");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _wordpress_compose__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @wordpress/compose */ "@wordpress/compose");
/* harmony import */ var _wordpress_compose__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_wordpress_compose__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _wordpress_hooks__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @wordpress/hooks */ "@wordpress/hooks");
/* harmony import */ var _wordpress_hooks__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_wordpress_hooks__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _wordpress_components__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @wordpress/components */ "@wordpress/components");
/* harmony import */ var _wordpress_components__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_wordpress_components__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _wordpress_block_editor__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @wordpress/block-editor */ "@wordpress/block-editor");
/* harmony import */ var _wordpress_block_editor__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @wordpress/i18n */ "@wordpress/i18n");
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! classnames */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/classnames-npm-2.3.2-d2fdae468d-10.zip/node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_7__);


/**
 * WordPress Dependencies
 */






const blocksWithLightboxSupport = ['core/gallery'];
const withLightbox = (0,_wordpress_compose__WEBPACK_IMPORTED_MODULE_2__.createHigherOrderComponent)(BlockEdit => {
  return props => {
    const {
      name,
      attributes,
      setAttributes,
      isSelected
    } = props;
    const {
      lightbox,
      fancyLightbox
    } = attributes;
    const supportsLightbox = blocksWithLightboxSupport.includes(name);
    return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)(_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.Fragment, null, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)(BlockEdit, props), isSelected && supportsLightbox && (!!props.attributes?.images?.length || !!props.attributes.id) && (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_5__.InspectorControls, null, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)(_wordpress_components__WEBPACK_IMPORTED_MODULE_4__.PanelBody, {
      title: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_6__.__)("Lightbox Settings", "lg-blocks")
    }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)(_wordpress_components__WEBPACK_IMPORTED_MODULE_4__.ToggleControl, {
      label: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_6__.__)('Enable Lightbox', 'lg-blocks'),
      checked: !!lightbox,
      onChange: () => setAttributes({
        lightbox: !lightbox
      })
    }), lightbox && (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)(_wordpress_components__WEBPACK_IMPORTED_MODULE_4__.ToggleControl, {
      label: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_6__.__)('Enable fancy style', 'lg-blocks'),
      checked: !!fancyLightbox,
      onChange: () => setAttributes({
        fancyLightbox: !fancyLightbox
      })
    }))));
  };
}, "withLightbox");
const addAttributes = (settings, name) => {
  if (name === "core/gallery") {
    //console.log(settings, name);
    const supportsLightbox = blocksWithLightboxSupport.includes(name);
    // Add custom attribute
    if (supportsLightbox) {
      if (!settings?.attributes?.lightbox) {
        settings.attributes = Object.assign(settings.attributes, {
          lightbox: {
            type: 'boolean',
            default: false
          },
          fancyLightbox: {
            type: 'boolean',
            default: false
          }
        });
      }
    }
  }
  return settings;
};

/**
 * Add custom lg-blocks editor lightbox classes to selected blocks
 *
 * @param {Function} BlockEdit Original component.
 * @return {string} Wrapped component.
 */
const withLightboxClasses = (0,_wordpress_compose__WEBPACK_IMPORTED_MODULE_2__.createHigherOrderComponent)(BlockListBlock => {
  return props => {
    const {
      name,
      attributes
    } = props;
    const {
      lightbox,
      fancyLightbox
    } = attributes;
    const supportsLightbox = blocksWithLightboxSupport.includes(name);
    let wrapperProps;
    let newClassName = props.className;
    if (supportsLightbox) {
      wrapperProps = props?.wrapperProps;
      newClassName = classnames__WEBPACK_IMPORTED_MODULE_7___default()(props.className, {
        [`has-lightbox`]: lightbox,
        [`has-fancy-lightbox`]: fancyLightbox
      });
    }
    wrapperProps = {
      ...wrapperProps,
      'data-animation': 'fadeIn'
    };
    return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)(BlockListBlock, (0,_babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__["default"])({}, props, {
      wrapperProps: wrapperProps,
      className: newClassName
    }));
  };
}, 'withlightboxClasses');

/**
 * Override props assigned to save component to inject lightbox classes.
 *
 * @param {Object} extraProps Additional props applied to save element.
 * @param {Object} blockType  Block type.
 * @param {Object} attributes Current block attributes.
 *
 * @return {Object} Filtered props applied to save element.
 */
function applyLightboxClasses(extraProps, blockType, attributes) {
  const supportsLightbox = blocksWithLightboxSupport.includes(blockType?.name);
  if (supportsLightbox) {
    const {
      lightbox,
      fancyLightbox
    } = attributes;
    extraProps = {
      ...extraProps,
      'data-animation': 'fadeIn'
    };
    extraProps.className = classnames__WEBPACK_IMPORTED_MODULE_7___default()(extraProps.className, {
      [`has-lightbox`]: lightbox,
      [`has-fancy-lightbox`]: fancyLightbox
    });
  }
  return extraProps;
}
(0,_wordpress_hooks__WEBPACK_IMPORTED_MODULE_3__.addFilter)("blocks.registerBlockType", "lg-blocks/lightbox/add-attributes", addAttributes);
(0,_wordpress_hooks__WEBPACK_IMPORTED_MODULE_3__.addFilter)("editor.BlockEdit", "lg-blocks/lightbox/with-lightbox", withLightbox);
(0,_wordpress_hooks__WEBPACK_IMPORTED_MODULE_3__.addFilter)('editor.BlockListBlock', 'lg-blocks/lightbox/editor-lightbox-classes', withLightboxClasses);
(0,_wordpress_hooks__WEBPACK_IMPORTED_MODULE_3__.addFilter)('blocks.getSaveContent.extraProps', 'lg-blocks/lightbox/save-lightbox-classes', applyLightboxClasses);

/***/ }),

/***/ "./src/hoc/spacing-controls/index.js":
/*!*******************************************!*\
  !*** ./src/hoc/spacing-controls/index.js ***!
  \*******************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var lodash_assign__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! lodash/assign */ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/lodash-npm-4.17.21-6382451519-10.zip/node_modules/lodash/assign.js");
/* harmony import */ var lodash_assign__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(lodash_assign__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _wordpress_compose__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @wordpress/compose */ "@wordpress/compose");
/* harmony import */ var _wordpress_compose__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_wordpress_compose__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _wordpress_block_editor__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @wordpress/block-editor */ "@wordpress/block-editor");
/* harmony import */ var _wordpress_block_editor__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _wordpress_components__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @wordpress/components */ "@wordpress/components");
/* harmony import */ var _wordpress_components__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_wordpress_components__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _wordpress_hooks__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @wordpress/hooks */ "@wordpress/hooks");
/* harmony import */ var _wordpress_hooks__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_wordpress_hooks__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @wordpress/i18n */ "@wordpress/i18n");
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var bootstrap_dist_css_bootstrap_utilities_css__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! bootstrap/dist/css/bootstrap-utilities.css */ "./.yarn/__virtual__/bootstrap-virtual-1c32f8fa92/10/AppData/Local/Yarn/Berry/cache/bootstrap-npm-5.2.3-7458283a23-10.zip/node_modules/bootstrap/dist/css/bootstrap-utilities.css");
/* harmony import */ var bootstrap_dist_css_bootstrap_grid_css__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! bootstrap/dist/css/bootstrap-grid.css */ "./.yarn/__virtual__/bootstrap-virtual-1c32f8fa92/10/AppData/Local/Yarn/Berry/cache/bootstrap-npm-5.2.3-7458283a23-10.zip/node_modules/bootstrap/dist/css/bootstrap-grid.css");










// Enable style controls on the following blocks
const disableStyleControlOnBlocks = ["lg-blocks/breadcrumb", "lg-blocks/swiper-testimonials", "lg-blocks/post-filter", "gravityforms/form", "aioseo/breadcrumbs", "yoast-seo/breadcrumbs"];
const spacingClasses = {
  pt: ["pt-1", "pt-2", "pt-3", "pt-4", "pt-5"],
  pb: ["pb-1", "pb-2", "pb-3", "pb-4", "pb-5"],
  pl: ["pl-1", "pl-2", "pl-3", "pl-4", "pl-5"],
  pr: ["pr-1", "pr-2", "pr-3", "pr-4", "pr-5"],
  mt: ["mt-1", "mt-2", "mt-3", "mt-4", "mt-5"],
  mb: ["mb-1", "mb-2", "mb-3", "mb-4", "mb-5"],
  ml: ["ml-1", "ml-2", "ml-3", "ml-4", "ml-5"],
  mr: ["mr-1", "mr-2", "mr-3", "mr-4", "mr-5"]
};

// Available spacing control options
const spacingControlOptions = [{
  label: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_6__.__)("None", "lg-blocks"),
  value: 0
}, {
  label: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_6__.__)("Extra Small", "lg-blocks"),
  value: 1
}, {
  label: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_6__.__)("Small", "lg-blocks"),
  value: 2
}, {
  label: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_6__.__)("Medium", "lg-blocks"),
  value: 3
}, {
  label: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_6__.__)("Large", "lg-blocks"),
  value: 4
}, {
  label: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_6__.__)("Extra Large", "lg-blocks"),
  value: 5
}];

/**
 * Add spacing control attribute to block.
 *
 * @param {object} settings Current block settings.
 * @param {string} name Name of block.
 *
 * @returns {object} Modified block settings.
 */
const addSpacingControlAttribute = (settings, name) => {
  //console.log(settings, name);
  // Use Lodash's assign to gracefully handle if attributes are undefined

  if (disableStyleControlOnBlocks.includes(name)) {
    return settings;
  }
  settings.attributes = lodash_assign__WEBPACK_IMPORTED_MODULE_1___default()(settings.attributes, {
    paddingTop: {
      type: "number",
      default: spacingControlOptions[0].value
    },
    paddingBottom: {
      type: "number",
      default: spacingControlOptions[0].value
    },
    paddingLeft: {
      type: "number",
      default: spacingControlOptions[0].value
    },
    paddingRight: {
      type: "number",
      default: spacingControlOptions[0].value
    },
    marginTop: {
      type: "number",
      default: spacingControlOptions[0].value
    },
    marginBottom: {
      type: "number",
      default: spacingControlOptions[0].value
    },
    marginLeft: {
      type: "number",
      default: spacingControlOptions[0].value
    },
    marginRight: {
      type: "number",
      default: spacingControlOptions[0].value
    }
  });
  return settings;
};
(0,_wordpress_hooks__WEBPACK_IMPORTED_MODULE_5__.addFilter)("blocks.registerBlockType", "lg-blocks/attribute/spacing-controls", addSpacingControlAttribute);

/**
 * Create HOC to add spacing control to inspector controls of block.
 */
const withSpacingControl = (0,_wordpress_compose__WEBPACK_IMPORTED_MODULE_2__.createHigherOrderComponent)(BlockEdit => {
  return props => {
    //console.log('PROPS: ', props);
    if (disableStyleControlOnBlocks.includes(props.name)) {
      return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(BlockEdit, props);
    }
    const {
      paddingTop,
      paddingBottom,
      //paddingLeft,
      //paddingRight,
      marginTop,
      marginBottom
      //marginLeft,
      //marginRight
    } = props.attributes;
    const onSpacingChange = (selectedSpacingOption, spacingType) => {
      if (!spacingType) {
        return true;
      }
      const {
        className
      } = props.attributes;
      //console.log(selectedSpacingOption, spacingType, spacingClasses[spacingType]);
      // Filter out spacing css classes to preserve other additional classes
      const classNameWithoutSpacing = className ? className.split(" ").filter(classString => !spacingClasses[spacingType].includes(classString)) : [];
      let currentSpacingClasses = [];
      if (selectedSpacingOption > 0) {
        currentSpacingClasses.push(`${spacingType}-${selectedSpacingOption}`);
      }
      const allSpacingClasses = classNameWithoutSpacing.concat(currentSpacingClasses);
      //console.log(allSpacingClasses);
      const allSpacingClassesStr = allSpacingClasses.join(" ");
      let spacingTypeFullName = "";
      switch (spacingType) {
        case "pt":
          spacingTypeFullName = "paddingTop";
          break;
        case "pb":
          spacingTypeFullName = "paddingBottom";
          break;
        case "pl":
          spacingTypeFullName = "paddingLeft";
          break;
        case "pr":
          spacingTypeFullName = "paddingRight";
          break;
        case "mt":
          spacingTypeFullName = "marginTop";
          break;
        case "mb":
          spacingTypeFullName = "marginBottom";
          break;
        case "ml":
          spacingTypeFullName = "marginLeft";
          break;
        case "mr":
          spacingTypeFullName = "marginRight";
          break;
      }
      props.setAttributes({
        [spacingTypeFullName]: selectedSpacingOption
      });
      props.setAttributes({
        className: allSpacingClassesStr
      });
    };
    return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(BlockEdit, props), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_3__.InspectorControls, null, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_components__WEBPACK_IMPORTED_MODULE_4__.PanelBody, {
      title: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_6__.__)("Spacing Controls", "lg-blocks"),
      initialOpen: false
    }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_components__WEBPACK_IMPORTED_MODULE_4__.RangeControl, {
      label: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_6__.__)("Margin Top", "lg-blocks"),
      value: marginTop,
      onChange: selectedSpacingOption => onSpacingChange(selectedSpacingOption, "mt"),
      min: 0,
      max: 5
    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_components__WEBPACK_IMPORTED_MODULE_4__.RangeControl, {
      label: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_6__.__)("Margin Bottom", "lg-blocks"),
      value: marginBottom,
      onChange: selectedSpacingOption => onSpacingChange(selectedSpacingOption, "mb"),
      min: 0,
      max: 5
    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_components__WEBPACK_IMPORTED_MODULE_4__.RangeControl, {
      label: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_6__.__)("Padding Top", "lg-blocks"),
      value: paddingTop,
      onChange: selectedSpacingOption => onSpacingChange(selectedSpacingOption, "pt"),
      min: 0,
      max: 5
    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_components__WEBPACK_IMPORTED_MODULE_4__.RangeControl, {
      label: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_6__.__)("Padding Bottom", "lg-blocks"),
      value: paddingBottom,
      onChange: selectedSpacingOption => onSpacingChange(selectedSpacingOption, "pb"),
      min: 0,
      max: 5
    }))));
  };
}, "withSpacingControl");
(0,_wordpress_hooks__WEBPACK_IMPORTED_MODULE_5__.addFilter)("editor.BlockEdit", "lg-blocks/with-spacing-control", withSpacingControl);

/**
 * Add margin style attribute to save element of block.
 *
 * @param {object} saveElementProps Props of save element.
 * @param {Object} blockType Block type information.
 * @param {Object} attributes Attributes of block.
 *
 * @returns {object} Modified props of save element.
 */
const addSpacingExtraProps = (saveElementProps, blockType, attributes) => {
  //console.log(saveElementProps);

  if (attributes.paddingTop) {
    // Use Lodash's assign to gracefully handle if attributes are undefined
    /* assign(saveElementProps, {
    /* assign(saveElementProps, {
       paddingTop:attributes.paddingTop
     });*/
  }
  //console.log(blockType, attributes);
  return saveElementProps;
};
(0,_wordpress_hooks__WEBPACK_IMPORTED_MODULE_5__.addFilter)("blocks.getSaveContent.extraProps", "lg-blocks/get-save-content/extra-props", addSpacingExtraProps);

/***/ }),

/***/ "./.yarn/__virtual__/bootstrap-virtual-1c32f8fa92/10/AppData/Local/Yarn/Berry/cache/bootstrap-npm-5.2.3-7458283a23-10.zip/node_modules/bootstrap/dist/css/bootstrap-grid.css":
/*!***********************************************************************************************************************************************************************************!*\
  !*** ./.yarn/__virtual__/bootstrap-virtual-1c32f8fa92/10/AppData/Local/Yarn/Berry/cache/bootstrap-npm-5.2.3-7458283a23-10.zip/node_modules/bootstrap/dist/css/bootstrap-grid.css ***!
  \***********************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ }),

/***/ "./.yarn/__virtual__/bootstrap-virtual-1c32f8fa92/10/AppData/Local/Yarn/Berry/cache/bootstrap-npm-5.2.3-7458283a23-10.zip/node_modules/bootstrap/dist/css/bootstrap-utilities.css":
/*!****************************************************************************************************************************************************************************************!*\
  !*** ./.yarn/__virtual__/bootstrap-virtual-1c32f8fa92/10/AppData/Local/Yarn/Berry/cache/bootstrap-npm-5.2.3-7458283a23-10.zip/node_modules/bootstrap/dist/css/bootstrap-utilities.css ***!
  \****************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ }),

/***/ "./src/hoc/data-attributes/style.scss":
/*!********************************************!*\
  !*** ./src/hoc/data-attributes/style.scss ***!
  \********************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ }),

/***/ "./src/hoc/fontawesome-lists/style.scss":
/*!**********************************************!*\
  !*** ./src/hoc/fontawesome-lists/style.scss ***!
  \**********************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ }),

/***/ "lodash":
/*!*************************!*\
  !*** external "lodash" ***!
  \*************************/
/***/ (function(module) {

"use strict";
module.exports = window["lodash"];

/***/ }),

/***/ "@wordpress/block-editor":
/*!*************************************!*\
  !*** external ["wp","blockEditor"] ***!
  \*************************************/
/***/ (function(module) {

"use strict";
module.exports = window["wp"]["blockEditor"];

/***/ }),

/***/ "@wordpress/components":
/*!************************************!*\
  !*** external ["wp","components"] ***!
  \************************************/
/***/ (function(module) {

"use strict";
module.exports = window["wp"]["components"];

/***/ }),

/***/ "@wordpress/compose":
/*!*********************************!*\
  !*** external ["wp","compose"] ***!
  \*********************************/
/***/ (function(module) {

"use strict";
module.exports = window["wp"]["compose"];

/***/ }),

/***/ "@wordpress/element":
/*!*********************************!*\
  !*** external ["wp","element"] ***!
  \*********************************/
/***/ (function(module) {

"use strict";
module.exports = window["wp"]["element"];

/***/ }),

/***/ "@wordpress/hooks":
/*!*******************************!*\
  !*** external ["wp","hooks"] ***!
  \*******************************/
/***/ (function(module) {

"use strict";
module.exports = window["wp"]["hooks"];

/***/ }),

/***/ "@wordpress/i18n":
/*!******************************!*\
  !*** external ["wp","i18n"] ***!
  \******************************/
/***/ (function(module) {

"use strict";
module.exports = window["wp"]["i18n"];

/***/ }),

/***/ "../../../../../../../../../AppData/Local/Yarn/Berry/cache/@babel-runtime-npm-7.21.0-c4ef698c89-10.zip/node_modules/@babel/runtime/helpers/esm/extends.js":
/*!****************************************************************************************************************************************************************!*\
  !*** ../../../../../../../../../AppData/Local/Yarn/Berry/cache/@babel-runtime-npm-7.21.0-c4ef698c89-10.zip/node_modules/@babel/runtime/helpers/esm/extends.js ***!
  \****************************************************************************************************************************************************************/
/***/ (function(__unused_webpack___webpack_module__, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* binding */ _extends; }
/* harmony export */ });
function _extends() {
  _extends = Object.assign ? Object.assign.bind() : function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];
      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }
    return target;
  };
  return _extends.apply(this, arguments);
}

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			id: moduleId,
/******/ 			loaded: false,
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = __webpack_modules__;
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/chunk loaded */
/******/ 	!function() {
/******/ 		var deferred = [];
/******/ 		__webpack_require__.O = function(result, chunkIds, fn, priority) {
/******/ 			if(chunkIds) {
/******/ 				priority = priority || 0;
/******/ 				for(var i = deferred.length; i > 0 && deferred[i - 1][2] > priority; i--) deferred[i] = deferred[i - 1];
/******/ 				deferred[i] = [chunkIds, fn, priority];
/******/ 				return;
/******/ 			}
/******/ 			var notFulfilled = Infinity;
/******/ 			for (var i = 0; i < deferred.length; i++) {
/******/ 				var chunkIds = deferred[i][0];
/******/ 				var fn = deferred[i][1];
/******/ 				var priority = deferred[i][2];
/******/ 				var fulfilled = true;
/******/ 				for (var j = 0; j < chunkIds.length; j++) {
/******/ 					if ((priority & 1 === 0 || notFulfilled >= priority) && Object.keys(__webpack_require__.O).every(function(key) { return __webpack_require__.O[key](chunkIds[j]); })) {
/******/ 						chunkIds.splice(j--, 1);
/******/ 					} else {
/******/ 						fulfilled = false;
/******/ 						if(priority < notFulfilled) notFulfilled = priority;
/******/ 					}
/******/ 				}
/******/ 				if(fulfilled) {
/******/ 					deferred.splice(i--, 1)
/******/ 					var r = fn();
/******/ 					if (r !== undefined) result = r;
/******/ 				}
/******/ 			}
/******/ 			return result;
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	!function() {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = function(module) {
/******/ 			var getter = module && module.__esModule ?
/******/ 				function() { return module['default']; } :
/******/ 				function() { return module; };
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	!function() {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = function(exports, definition) {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/global */
/******/ 	!function() {
/******/ 		__webpack_require__.g = (function() {
/******/ 			if (typeof globalThis === 'object') return globalThis;
/******/ 			try {
/******/ 				return this || new Function('return this')();
/******/ 			} catch (e) {
/******/ 				if (typeof window === 'object') return window;
/******/ 			}
/******/ 		})();
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	!function() {
/******/ 		__webpack_require__.o = function(obj, prop) { return Object.prototype.hasOwnProperty.call(obj, prop); }
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	!function() {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = function(exports) {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/node module decorator */
/******/ 	!function() {
/******/ 		__webpack_require__.nmd = function(module) {
/******/ 			module.paths = [];
/******/ 			if (!module.children) module.children = [];
/******/ 			return module;
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/jsonp chunk loading */
/******/ 	!function() {
/******/ 		// no baseURI
/******/ 		
/******/ 		// object to store loaded and loading chunks
/******/ 		// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 		// [resolve, reject, Promise] = chunk loading, 0 = chunk loaded
/******/ 		var installedChunks = {
/******/ 			"index": 0,
/******/ 			"./style-index": 0
/******/ 		};
/******/ 		
/******/ 		// no chunk on demand loading
/******/ 		
/******/ 		// no prefetching
/******/ 		
/******/ 		// no preloaded
/******/ 		
/******/ 		// no HMR
/******/ 		
/******/ 		// no HMR manifest
/******/ 		
/******/ 		__webpack_require__.O.j = function(chunkId) { return installedChunks[chunkId] === 0; };
/******/ 		
/******/ 		// install a JSONP callback for chunk loading
/******/ 		var webpackJsonpCallback = function(parentChunkLoadingFunction, data) {
/******/ 			var chunkIds = data[0];
/******/ 			var moreModules = data[1];
/******/ 			var runtime = data[2];
/******/ 			// add "moreModules" to the modules object,
/******/ 			// then flag all "chunkIds" as loaded and fire callback
/******/ 			var moduleId, chunkId, i = 0;
/******/ 			if(chunkIds.some(function(id) { return installedChunks[id] !== 0; })) {
/******/ 				for(moduleId in moreModules) {
/******/ 					if(__webpack_require__.o(moreModules, moduleId)) {
/******/ 						__webpack_require__.m[moduleId] = moreModules[moduleId];
/******/ 					}
/******/ 				}
/******/ 				if(runtime) var result = runtime(__webpack_require__);
/******/ 			}
/******/ 			if(parentChunkLoadingFunction) parentChunkLoadingFunction(data);
/******/ 			for(;i < chunkIds.length; i++) {
/******/ 				chunkId = chunkIds[i];
/******/ 				if(__webpack_require__.o(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 					installedChunks[chunkId][0]();
/******/ 				}
/******/ 				installedChunks[chunkId] = 0;
/******/ 			}
/******/ 			return __webpack_require__.O(result);
/******/ 		}
/******/ 		
/******/ 		var chunkLoadingGlobal = self["webpackChunklg_blocks"] = self["webpackChunklg_blocks"] || [];
/******/ 		chunkLoadingGlobal.forEach(webpackJsonpCallback.bind(null, 0));
/******/ 		chunkLoadingGlobal.push = webpackJsonpCallback.bind(null, chunkLoadingGlobal.push.bind(chunkLoadingGlobal));
/******/ 	}();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module depends on other loaded chunks and execution need to be delayed
/******/ 	var __webpack_exports__ = __webpack_require__.O(undefined, ["./style-index"], function() { return __webpack_require__("./src/hoc/index.js"); })
/******/ 	__webpack_exports__ = __webpack_require__.O(__webpack_exports__);
/******/ 	
/******/ })()
;
//# sourceMappingURL=index.js.map