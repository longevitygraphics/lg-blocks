<?php

function extend_data_attributes($block_content, $block) {
    if ( $block['blockName'] === 'core/button' ){
		$attrs = $block['attrs'];
		if( isset($attrs['dataAttributes']) && $attrs['dataAttributes'] ) {
            $html  = str_get_html($block_content);
            $ret = $html->find('a', 0);
            foreach($attrs['dataAttributes'] as $attribute) {
                $property = "data-" . $attribute["name"];
                $ret->$property = $attribute["value"];
            }
            $block_content = $html;
        }
        
	}
	return $block_content;
}

add_filter( 'render_block', 'extend_data_attributes', 10, 2);