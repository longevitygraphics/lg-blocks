<?php

/**
 * Serverside Rendering file for Yoast Breadcrumbs Block
 *
 * @package lg_blocks
 */

?>
<?php

/**
 * Render Callback function for Yoast Breadcrumbs Blocks
 *
 * @param array $attributes Gutenberg Block attributes registered in register_block() in main plugin.php.
 *
 * @return string
 */

// $swiper_testimonials_script_loaded = false;

function lg_blocks_render_swiper_testimonials($attributes)
{
	wp_enqueue_script('lg-blocks-swiper-testimonials-view-script');

	$swiper_testimonials = '';


	//print_r($attributes);
	$classes = 'wp-block-lg-blocks-testimonials ';
	if (isset($attributes['className'])) {
		$classes .= $attributes['className'];
	}
	$args = array(
		'posts_per_page' => $attributes['sliderFetchCount'] ? $attributes['sliderFetchCount'] : 5,
		'post_type'      => $attributes['postType'] ? $attributes['postType'] : 'lg_testimonial',
	);

	$slides_query        = new WP_Query($args);

	if ($slides_query->have_posts()) {
		if ('list' === $attributes['layoutType']) {
			include plugin_dir_path(__FILE__) . 'layouts/list.php';
		} else {
			include plugin_dir_path(__FILE__) . 'layouts/slider.php';
		}
		wp_reset_postdata();
		return $swiper_testimonials;
	} else {
		return '<div>' . __('No Posts Found', 'lg-blocks') . '</div>';
	}
}
