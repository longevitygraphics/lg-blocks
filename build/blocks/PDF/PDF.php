<?php

function lg_blocks_render_pdf( $attributes, $content, $block ) {
    wp_enqueue_style( 'lg-pdf-frontend', plugin_dir_url( __FILE__ ) . 'frontend.css' );

    global $post;
    $current_post_id = isset($block->context['postId']) ? $block->context['postId'] : $post->post_id;


    $current_post_content = get_post_field('post_content', $current_post_id);

    $blocks = parse_blocks($current_post_content);

    foreach ($blocks as $parsed_block) {
        if ($parsed_block['blockName'] === 'lg-blocks/pdf-viewer') {
            $attributes = $parsed_block['attrs'];
        }
    } 

    // echo '<pre>';
    // var_dump($attributes);
    // var_dump($block->context);
    // var_dump($blocks);
    // echo '</pre>';

    ob_start();
	?>
		<div class="lg-pdf-viewer-block" data-file-url="<?php echo esc_attr( $attributes['pdfURL'] ); ?>"></div>
	<?php
    return ob_get_clean();

}
