<?php

function lg_blocks_render_page_excerpt( $attributes) {
    
    ob_start() ?>
    <div class="wp-block-lg-blocks-page-excerpt">
        <p class="has-<?php echo $attributes['textColor'] ?>-color  wp-block-lg-blocks-page-excerpt__content" ><?php echo get_the_excerpt(); ?></p>
    </div>

    <?php
    return ob_get_clean();

}