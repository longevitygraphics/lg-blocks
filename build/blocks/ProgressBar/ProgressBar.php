<?php
function lg_blocks_render_progress_bar( $attributes ) {
    global $lg_progress_bar_script_enqueued;

    if ( ! isset( $lg_progress_bar_script_enqueued ) ) {
        $lg_progress_bar_script_enqueued = true;

        wp_enqueue_script(
            'lg-frontend-progress',
            plugins_url( 'ProgressBar/view.js', __DIR__ ),
            array( 'react', 'react-dom', 'wp-element', 'wp-i18n', 'wp-polyfill' ),
            "c12b2f446afc5020f8ba",
            false
        );
    }

    ob_start();

    ?>
    <div class="lg-progress-bar-block" data-block-attributes="<?php echo esc_attr( json_encode( $attributes )); ?>"></div>
    <?php

    return ob_get_clean();
}