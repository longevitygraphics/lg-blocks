import $ from 'jquery';
import "./longevity-lightbox.scss";

(function ($) {
  'use strict';

  //swiper init config used for gallery lightbox.
  const swiperConfig = {
    effect: 'fade',
    fadeEffect: {
      crossFade: true
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    pagination: {
      el: '.swiper-pagination',
      type: 'bullets',
    },
    keyboard: {
      enabled: true
    }
  };

  //get all the wp-gallery blocks that has the lightbox enabled.
  const lightboxGalleries = document.getElementsByClassName('has-lightbox');

  //loop through all lightbox galleries, add indexed class and call the renderLightboxModal function for each of them.
  Array.from(lightboxGalleries).forEach(function (lightbox, index) {
    lightbox.className += ' lightbox-' + index + ' ';
    let hasFancyLightbox = lightbox.classList.contains("has-fancy-lightbox");
    renderLightboxModal(index, hasFancyLightbox);
  });

  //create and append modals to body tag for each gallery added to the given page
  function renderLightboxModal(lightboxIndex, hasFancyLightbox) {
    const images = document.querySelectorAll(`.has-lightbox.lightbox-${lightboxIndex} figure img`);
    if (images.length > 0) {
      //append lightbox modal to body
      $("body").append(getLightboxModalHtml(images, lightboxIndex, hasFancyLightbox));

      let lightboxSwiperInit;
      //add click event to images.
      Array.from(images).forEach((image, imageIndex) => {
        image.closest('figure').addEventListener("click", () => {
          $('#longevityLightboxModal-' + lightboxIndex).modal("show");
          //initialize the lightbox swiper slider
          lightboxSwiperInit = new Swiper("#lightboxSwiper-" + lightboxIndex, {
            ...swiperConfig,
            init: false,
            initialSlide: imageIndex
          });
          if (hasFancyLightbox) {
            lightboxSwiperInit.on('init slideChange', function () {
              if (images[this.activeIndex]) {
                const activeImageSrc = images[this.activeIndex].getAttribute('src');
                document.getElementById(`longevityLightboxModal-${lightboxIndex}`).style.setProperty("--fancy-background-image", `url("${activeImageSrc}")`);
              }
            });
          }

          lightboxSwiperInit.init();
        });
      });

      //Destroy the swiper instance when modal is closed.
      $('#longevityLightboxModal-' + lightboxIndex).on('hidden.bs.modal', function () {
        lightboxSwiperInit.destroy();
      });
    }
  }

  //return the lightbox modal html with swiper slider markup
  function getLightboxModalHtml(images, lightboxIndex, hasFancyLightbox) {
    let swiperSlides = "";
    Array.from(images).forEach((image) => {
      swiperSlides += `<div class="swiper-slide text-center">${image.outerHTML}</div>`;
    });

    return `
        <div id="longevityLightboxModal-${lightboxIndex}" class="modal longevity-lightbox-modal" tabindex="-1">
            <div class="modal-dialog modal-dialog-centered">

                <div class="modal-content">
                    <div class="modal-body">
                        <a href="javascript:;" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&#x2715;</span>
                        </a>
                        <div class="swiper-container" id="lightboxSwiper-${lightboxIndex}">
                            <div class="swiper-wrapper">
                                ${swiperSlides}
                            </div>
                            <div class="swiper-pagination"></div>
                            <div class="swiper-button-prev"></div>
                            <div class="swiper-button-next"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>`;
  }
}($));
