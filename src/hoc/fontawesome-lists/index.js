import { createHigherOrderComponent } from "@wordpress/compose";
import { InspectorControls } from "@wordpress/block-editor";
import { PanelBody, TextControl } from "@wordpress/components";
import { addFilter } from "@wordpress/hooks";
import { Fragment } from "@wordpress/element";
import { __ } from "@wordpress/i18n";
import classnames from 'classnames';

import "./style.scss"

//restrict to specific block names
const allowedBlocks = ['core/list'];

/**
 * Add custom attribute for mobile visibility.
 *
 * @param {Object} settings Settings for the block.
 *
 * @return {Object} settings Modified settings.
 */
function addAttributes(settings) {

    //check if object exists for old Gutenberg version compatibility
    //add allowedBlocks restriction
    if (typeof settings.attributes !== 'undefined' && allowedBlocks.includes(settings.name)) {

        settings.attributes = Object.assign(settings.attributes, {
            markerContent: {
                type: 'string',
                default: "f00c",
            },
            markerFamily: {
                type: 'string',
                default: 'Font Awesome 6 Pro'
            }
        });

    }

    return settings;
}

/**
 * Add mobile visibility controls on Advanced Block Panel.
 *
 * @param {function} BlockEdit Block edit component.
 *
 * @return {function} BlockEdit Modified block edit component.
 */
const withAdvancedControls = createHigherOrderComponent((BlockEdit) => {
    return (props) => {

        const {
            name,
            attributes,
            setAttributes,
            isSelected,
        } = props;

        const {
            markerContent, markerFamily
        } = attributes;


        return (
            <Fragment>
                <BlockEdit {...props} />
                {isSelected && allowedBlocks.includes(name) &&
                    <InspectorControls>
                        <PanelBody
                            title={__("FontAwesome", "lg-blocks")}
                            initialOpen={false}
                        >
                            <TextControl
                                label={__('Font Family')}
                                value={markerFamily}
                                onChange={(v) => setAttributes({ markerFamily: v })}
                            />
                            <TextControl
                                label={__('Unicode Character')}
                                value={markerContent}
                                onChange={(v) => setAttributes({ markerContent: v })}
                            />
                        </PanelBody>
                    </InspectorControls>
                }

            </Fragment>
        );
    };
}, 'withAdvancedControls');

/**
 * Add custom element class in save element.
 *
 * @param {Object} extraProps     Block element.
 * @param {Object} blockType      Blocks object.
 * @param {Object} attributes     Blocks attributes.
 *
 * @return {Object} extraProps Modified block element.
 */
function applyExtraClass(extraProps, blockType, attributes) {

    const { markerContent, markerFamily } = attributes;

    //check if attribute exists for old Gutenberg version compatibility
    //add class only when visibleOnMobile = false
    //add allowedBlocks restriction
    if (typeof markerContent !== 'undefined' && markerContent && typeof markerFamily !== 'undefined' && markerFamily && allowedBlocks.includes(blockType.name)) {
        extraProps.style = `--markerContent: '\\${markerContent}'; --markerFamily: '${markerFamily}';`;
    }

    return extraProps;
}

//add filters

addFilter(
    'blocks.registerBlockType',
    'editorskit/custom-attributes',
    addAttributes
);

addFilter(
    'editor.BlockEdit',
    'editorskit/custom-advanced-control',
    withAdvancedControls
);

addFilter(
    'blocks.getSaveContent.extraProps',
    'editorskit/applyExtraClass',
    applyExtraClass
);