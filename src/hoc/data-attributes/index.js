import { createHigherOrderComponent } from "@wordpress/compose";
import { InspectorControls } from "@wordpress/block-editor";
import { PanelBody, TextControl } from "@wordpress/components";
import { addFilter } from "@wordpress/hooks";
import { Fragment } from "@wordpress/element";
import { __ } from "@wordpress/i18n";
import { assign } from "lodash";

import "./style.scss"

//restrict to specific block names
const allowedBlocks = ['core/button'];

/**
 * 
 *
 * @param {Object} settings Settings for the block.
 *
 * @return {Object} settings Modified settings.
 */
function addAttributes(settings) {

    //check if object exists for old Gutenberg version compatibility
    //add allowedBlocks restriction
    if (typeof settings.attributes !== 'undefined' && allowedBlocks.includes(settings.name)) {

        settings.attributes = Object.assign(settings.attributes, {
            dataAttributes: {
                type: 'array',
                default: [
                    { "name": "", "value": "" }
                ],
            },

        });

    }

    return settings;
}

/**
 * 
 *
 * @param {function} BlockEdit Block edit component.
 *
 * @return {function} BlockEdit Modified block edit component.
 */
const withAdvancedControls = createHigherOrderComponent((BlockEdit) => {
    return (props) => {

       

        const {
            name,
            attributes,
            setAttributes,
            isSelected,
        } = props;

        const {
            dataAttributes
        } = attributes;

        console.log(dataAttributes);

        const onChangeAttributeCount = (count) => {
            console.log(count);
            console.log(typeof count);
            if (dataAttributes.length !== count) {
                if ( dataAttributes.length > count) {
                    let newData = [...dataAttributes];
                    newData.length = count;
                    setAttributes({ dataAttributes: newData});
                    
                }
                
            }
            if (dataAttributes.length !== count) {
                if ( dataAttributes.length < count) {
                    let newData = [...dataAttributes];
                    newData.push( { "name": "", "value": "" });
                    setAttributes({ dataAttributes: newData});
                }
                
            }
        }

        const onChangeAttribute = ( value, type, index ) => {
            let newData = [...dataAttributes];
            newData[index][type] = value;
            setAttributes({dataAttributes: newData});
        }

        const renderControls = () => {
            return dataAttributes.map((attribute, index) => {
                return (
                    <div key ={index}>
                        <TextControl
                            label={`Attribute ${index + 1} Name`}
                            value={dataAttributes[index].name}
                            onChange={ (v) => { onChangeAttribute(v, "name", index)}}
                        />
                        <TextControl
                            label={`Attribute ${index + 1} Value`}
                            value={dataAttributes[index].value}
                            onChange={ (v) => { onChangeAttribute(v, "value", index)}}
                        />
                        <hr />
                    </div>
                )
            })
        }


        return (
            <Fragment>
                <BlockEdit {...props} />
                {isSelected && allowedBlocks.includes(name) &&
                    <InspectorControls>
                        <PanelBody
                            title={__("Data Attributes", "lg-blocks")}
                            initialOpen={false}
                        >
                            <TextControl
                                label="# of Data Attributes"
                                value={dataAttributes.length}
                                onChange={(v) => onChangeAttributeCount(parseFloat(v))}
                                type="number"
                            />
                            {renderControls()}
                        </PanelBody>
                    </InspectorControls>
                }

            </Fragment>
        );
    };
}, 'withAdvancedControls');

/**
 * Add custom element class in save element.
 *
 * @param {Object} extraProps     Block element.
 * @param {Object} blockType      Blocks object.
 * @param {Object} attributes     Blocks attributes.
 *
 * @return {Object} extraProps Modified block element.
 */
function applyExtraClass(extraProps, blockType, attributes) {

    const { dataAttributes } = attributes;

    if (!allowedBlocks.includes(blockType.name)) {
        return extraProps;
    }

    //check if attribute exists for old Gutenberg version compatibility
    if (typeof dataAttributes !== 'undefined') {
        // prevent core/block errors on defaults
        if (dataAttributes.length === 1 && dataAttributes[0].name !== "" && dataAttributes[0].value !== "") {
            dataAttributes.forEach( (attribute) => {
               assign( extraProps, { [attribute.name] : attribute.value});
            })
        }
    }

    return extraProps;
}

//add filters

addFilter(
    'blocks.registerBlockType',
    'editorskit/custom-attributes',
    addAttributes
);

addFilter(
    'editor.BlockEdit',
    'editorskit/custom-advanced-control',
    withAdvancedControls
);

addFilter(
    'blocks.getSaveContent.extraProps',
    'editorskit/applyExtraClass',
    applyExtraClass
);