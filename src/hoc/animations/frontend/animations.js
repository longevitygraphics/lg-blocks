import Hunt from "huntjs";
import "./animations.scss";

const animatedElements = document.querySelectorAll("[data-animation]");
let observer = new Hunt(animatedElements, {
  enter: (animatedElement) => {
    //console.log(animatedElement);
    animatedElement.classList.add("animated", animatedElement.getAttribute("data-animation"));
  }
})
