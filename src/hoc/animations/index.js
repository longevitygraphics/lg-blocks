import {addFilter} from "@wordpress/hooks";
import {createHigherOrderComponent} from "@wordpress/compose";
import {InspectorControls} from "@wordpress/block-editor";
import {PanelBody, SelectControl} from "@wordpress/components";
import {__} from "@wordpress/i18n";

// Enable style controls on the following blocks
const disableStyleControlOnBlocks = [
  "lg-blocks/swiper-testimonials",
  "gravityforms/form"
];
const withAnimations = createHigherOrderComponent((BlockEdit) => {
  return (props) => {
    if (disableStyleControlOnBlocks.includes(props.name)) {
      return <BlockEdit {...props} />;
    }
    const {attributes, setAttributes, isSelected} = props;
    const {animation} = attributes;

    return (
      <>
        <BlockEdit {...props} />
        {isSelected && (
          <InspectorControls>
            <PanelBody title={__("Animations", "lg-blocks")}>
              <SelectControl
                label={__("Select Animation", "lg-blocks")}
                value={animation}
                onChange={(value) => setAttributes({animation: value})}
                options={[
                  {value: "", label: __("Select Animation", "lg-blocks")},
                  {value: "fadeIn", label: "fadeIn"},
                  {value: "fadeInUp", label: "fadeInUp"},
                  {value: "fadeInLeft", label: "fadeInLeft"},
                  {value: "fadeInRight", label: "fadeInRight"},
                ]}
              />
            </PanelBody>
          </InspectorControls>
        )}
      </>
    );
  };
}, "withAnimations");

const addAnimationAttributes = (settings, name) => {
  if (disableStyleControlOnBlocks.includes(name)) {
    return settings;
  }
  if (!settings?.attributes?.animation) {
    settings.attributes = Object.assign(settings.attributes, {
      animation: {
        type: 'string',
        default: "",
      },
    });
  }
  return settings;
}

/**
 * Add custom lg-blocks editor animation attributes to all blocks
 *
 * @param {Function} BlockEdit Original component.
 * @return {string} Wrapped component.
 */
const withAnimationAttributes = createHigherOrderComponent((BlockListBlock) => {
  return (props) => {
    if (disableStyleControlOnBlocks.includes(props.name)) {
      return <BlockListBlock {...props} />;
    }
    const {name, attributes} = props;
    const {animation} = attributes;

    let wrapperProps;

    if (animation) {
      wrapperProps = props?.wrapperProps;
      wrapperProps = {...wrapperProps, 'data-animation': animation}
    }
    return <BlockListBlock {...props} wrapperProps={wrapperProps}/>;
  };
}, 'withAnimationAttributes');

/**
 * Override props assigned to save component to inject animation attributes.
 *
 * @param {Object} extraProps Additional props applied to save element.
 * @param {Object} blockType  Block type.
 * @param {Object} attributes Current block attributes.
 *
 * @return {Object} Filtered props applied to save element.
 */
function applyAnimationAttributes(extraProps, blockType, attributes) {
  const {animation} = attributes;
  if (animation) {
    extraProps = {...extraProps, 'data-animation': animation}
  }
  return extraProps;
}


addFilter("blocks.registerBlockType", "lg-blocks/animations/add-attributes", addAnimationAttributes);
addFilter("editor.BlockEdit", "lg-blocks/animations/with-animations", withAnimations);
addFilter('editor.BlockListBlock', 'lg-blocks/animations/editor-animation-attributes', withAnimationAttributes);
addFilter('blocks.getSaveContent.extraProps', 'lg-blocks/animations/save-animation-attributes', applyAnimationAttributes);
