import { createHigherOrderComponent } from "@wordpress/compose";
import { addFilter } from "@wordpress/hooks";
import slugify from "slugify";

addFilter(
  "editor.BlockEdit",
  "lg-blocks/add-wrapper-elements",
  addWrapperElements
);

const addWrapperElements = createHigherOrderComponent(BlockEdit => {
  return props => {
    //console.log('PROPS: ', props);

    return (
      <div className={slugify(props.name)}>
        <BlockEdit {...props} />
      </div>
    );
  };
}, "addWrapperElements");
