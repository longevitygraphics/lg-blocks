/**
 * WordPress dependencies
 */
import {SVG, Path} from '@wordpress/components';

/**
 * Block user interface icons
 */
const icons = {};

icons.desktop = (
  <SVG className="dashicon" height="14" viewBox="0 0 16 16" width="14" xmlns="http://www.w3.org/2000/svg">
    <Path
      d="m12.5 0c.8325 0 1.5.61363636 1.5 1.36363636v11.27272724c0 .75-.675 1.3636364-1.5 1.3636364l-.5-2v-9h-10v9h10l.5 2h-11c-.8325 0-1.5-.6136364-1.5-1.3636364v-11.27272724c0-.75.6675-1.36363636 1.5-1.36363636zm-10 1c-.27614237 0-.5.22385763-.5.5s.22385763.5.5.5.5-.22385763.5-.5-.22385763-.5-.5-.5z"
      transform="translate(1 1.001872)"/>
  </SVG>
);

icons.tablet = (
  <SVG className="dashicon" height="14" viewBox="0 0 16 16" width="13" xmlns="http://www.w3.org/2000/svg">
    <Path
      d="m11.25 0h-9c-.825 0-1.5.61363636-1.5 1.36363636v13.27272724c0 .75.675 1.3636364 1.5 1.3636364h9c.825 0 1.5-.6136364 1.5-1.3636364v-13.27272724c0-.75-.675-1.36363636-1.5-1.36363636zm-.5 14h-8v-12h8z"
      transform="translate(1.25)"/>
  </SVG>
);

icons.mobile = (
  <SVG className="dashicon" height="14" viewBox="0 0 16 16" width="14" xmlns="http://www.w3.org/2000/svg">
    <Path
      d="m6.5 0h-5c-.825 0-1.5.61363636-1.5 1.36363636v9.27272724c0 .75.675 1.3636364 1.5 1.3636364h5c.825 0 1.5-.6136364 1.5-1.3636364v-9.27272724c0-.75-.675-1.36363636-1.5-1.36363636zm-.5 10h-4v-8h4z"
      transform="translate(4 2)"/>
  </SVG>
);
export default icons;
