import "./style.editor.scss";

/* Extensions */
import "./extensions/list-styles";

/* hoc */
import "./hoc/spacing-controls";
import "./hoc/lightbox";
//import "./hoc/animations";
//import "./hoc/wrapper-elements;"

/*All custom blocks should be imported below*/
//import "./blocks/base";
import "./blocks/TeamMember";
import "./blocks/CTA";
import "./blocks/LatestPosts";

import "./blocks/ImgText";
import "./blocks/Accordion";
import "./blocks/Accordion/AccordionItem";
import "./blocks/MapText";
import "./blocks/VideoModal";
import "./blocks/MultiCTA";
import "./blocks/Cards";
import "./blocks/Tabs";
// import "./blocks/swiper";
import "./blocks/SwiperSlider";
import "./blocks/SwiperSlider/SwiperSingleSlide";
import "./blocks/Breadcrumb";
import "./blocks/PricingTable";
import "./blocks/PricingTable/PricingTableItem";
import "./blocks/PostFilter";
import "./blocks/ResponsiveImage";
import "./blocks/PageHeader";
import "./blocks/Mailchimp";
import "./blocks/FancyHeading";
import "./blocks/SwiperTestimonials";
import "./blocks/VideoBanner";
import "./blocks/AdvancedMap";
import "./blocks/Copyright";
import "./blocks/AdvancedSlider";
import "./blocks/AdvancedSlider/AdvancedSlide";
import "./blocks/AdvancedSlider/VideoSlide";
import "./blocks/PostVideo";
//import "./blocks/product-info";
