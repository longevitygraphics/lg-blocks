import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";

import classnames from "classnames";
import { withSelect } from "@wordpress/data";
import { InnerBlocks, useBlockProps } from "@wordpress/block-editor";
import TabsInspectorControl from "./inspector";
import { Fragment, RawHTML, useEffect, useState } from "@wordpress/element";

import "./style.editor.scss";

const Edit = props => {
  const {
    attributes,
    tabCount,
    tabBlocks,
    isChildSelected,
    isSelected,
    setAttributes
  } = props;
  const { forcePreviewMode, tabs } = attributes;

  const blockProps = useBlockProps({
    className: "wp-block-lg-blocks-tabs"
  });

  const selfOrChildSelected = isSelected || isChildSelected;

  const [key, setKey] = useState(tabBlocks[0]?.attributes.key);

  useEffect(() => {
    let newTabs = [];

    console.log(tabBlocks);
    tabBlocks.forEach((tab, index) => {
      newTabs.push({ title: tab.attributes.title, key: tab.attributes.key, headerMode: tab.attributes.headerMode, mediaId: tab.attributes.mediaId, mediaUrl: tab.attributes.mediaUrl, faIcon: tab.attributes.faIcon, faStyle: tab.attributes.faStyle, faColor: tab.attributes.faColor });
    });

    setAttributes({ tabs: newTabs });
  }, [JSON.stringify(tabBlocks)]);

  console.log(tabs);

  const createInnerBlocks = tab => {
    return tab.innerBlocks.map((innerBlock, index) => {
      if (innerBlock.innerBlocks.length === 0) {
        return <RawHTML key={index}> {innerBlock.originalContent} </RawHTML>;
      }
      if (innerBlock.name === "core/group") {
        const GroupTag = innerBlock.attributes.tagName;
        return (
          <GroupTag className={innerBlock.attributes.className} key={index}>
            {createInnerBlocks(innerBlock)}
          </GroupTag>
        );
      }
      if (innerBlock.name === "core/columns") {
        return (
          <div className="wp-block-columns" key={index}>
            {createInnerBlocks(innerBlock)}
          </div>
        );
      }
      if (innerBlock.name === "core/column") {
        return (
          <div className="wp-block-column" key={index}>
            {createInnerBlocks(innerBlock)}
          </div>
        );
      }
      if (innerBlock.name === "core/buttons") {
        return (
          <div className="wp-block-buttons" key={index}>
            {createInnerBlocks(innerBlock)}
          </div>
        );
      }
      return;
    });
  };

  const renderTabs = () => {
    return tabBlocks.map((tab, index) => {
      return (
        <Tab
          eventKey={tab.attributes.key}
          key={tab.attributes.key}
          title={tab.attributes.title}
        >
          {createInnerBlocks(tab)}
        </Tab>
      );
    });
  };

  if (selfOrChildSelected && !forcePreviewMode) {
    return (
      <>
        <TabsInspectorControl data={props} />
        <div {...blockProps}>
          <InnerBlocks
            allowedBlocks={["lg-blocks/tab"]}
            template={[["lg-blocks/tab"], ["lg-blocks/tab"]]}
            renderAppender={InnerBlocks.ButtonBlockAppender}
          />
        </div>
      </>
    );
  } else {
    return (
      <>
        <TabsInspectorControl data={props} />
        <div>
          <Tabs
            activeKey={key}
            onSelect={k => {
              setKey(k);
            }}
          >
            {renderTabs(tabBlocks)}
          </Tabs>
          <div className="d-none">
            <InnerBlocks
              allowedBlocks={["lg-blocks/tab"]}
              template={[["lg-blocks/tab"], ["lg-blocks/tab"]]}
              renderAppender={InnerBlocks.ButtonBlockAppender}
            />
          </div>
        </div>
      </>
    );
  }
};

export default withSelect((select, props) => {
  const { clientId } = props;
  const coreEditor = select("core/block-editor");
  return {
    tabCount: coreEditor.getBlocks(clientId).length,
    isChildSelected: coreEditor.hasSelectedInnerBlock(clientId, true),
    tabBlocks: coreEditor.getBlocks(clientId)
  };
})(Edit);
