import jQuery from "jquery";

jQuery(document).ready(function () {

  const tabBlocks = document.querySelectorAll(".wp-block-lg-blocks-tabs");
  console.log("Tabs FE Script Loaded.");
  tabBlocks.forEach((tab, index) => {
    console.log(tab);

    console.log($(tab).find(".tab-pane").first());

    $(tab).find(".tab-pane").first().addClass("active show").siblings().removeClass("active show");
    $(tab).find(".nav-item").first().addClass("active");

    //add click event that adds class "active" to clicked button, removes "active" from all siblings,
    // same event also adds classes "active show" to matching tab-pane, removes from all "siblings"

    let navItems = tab.querySelectorAll(".nav-item");
    navItems.forEach((navItem, index) => {
      navItem.addEventListener("click", event => {
        jQuery(event.target).addClass("active").find(".nav-link").addClass("active").siblings().removeClass("active");
        jQuery(event.target).siblings().removeClass("active");
        
        let link = jQuery(event.target).find(".nav-link")[0];
        if (link === undefined) {
          link = event.target;
        }
        console.log(link);
        console.log(link.dataset.bsTarget);
        let pane = jQuery(link.dataset.bsTarget);
        jQuery(pane).addClass("active show").siblings().removeClass("active show");

      });
    });

    let activeSelector = $(tab).find(".active-selector");

    let buttons = tab.querySelectorAll(".nav-link");
    buttons.forEach((button, index) => {
      button.addEventListener("click", event => {
        console.log("Button Clicked directly.");
        jQuery(event.target).addClass("active").closest("li").addClass("active").siblings().removeClass('active').find(".nav-link").removeClass("active");

        let pane = jQuery(button.dataset.bsTarget);
        jQuery(pane).addClass("active show").siblings().removeClass("active show");
        $(activeSelector).html($(button).html()); 
        $(activeSelector).removeClass("open");
      });
    });

    activeSelector.on("click", (e) => {
      console.log("selector clicked");
      $(activeSelector).toggleClass("open");
    })




  });

})
