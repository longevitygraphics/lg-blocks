/**
 *
 * DEPENDENCIES
 *
 *
 */

import {
  InspectorControls,
  InspectorAdvancedControls
} from "@wordpress/block-editor";
import {
  PanelBody,
  RangeControl,
  ToggleControl,
  ButtonGroup,
  Button,
  Icon,
  ColorPalette
} from "@wordpress/components";

/**
 * https://developer.wordpress.org/block-editor/reference-guides/packages/packages-i18n/#x
 * Retrieve the translation of text.
 */
import { __ } from "@wordpress/i18n";

const TabsInspectorControl = props => {
  const { attributes, setAttributes } = props.data;
  const { forcePreviewMode, mobileDropdownSelector } = attributes;

  return (
    <>
      <InspectorControls>
        <PanelBody title={__("Tabs Options", "lg-blocks")}>
          <ToggleControl
            label="Mobile Dropdown Selector?"
            checked={mobileDropdownSelector}
            onChange={() => {
              setAttributes({ mobileDropdownSelector: !mobileDropdownSelector });
            }}
          />
        </PanelBody>
      </InspectorControls>
      <InspectorAdvancedControls>
        <ToggleControl
          label="Force Preview Mode?"
          checked={forcePreviewMode}
          onChange={value => {
            setAttributes({ forcePreviewMode: value });
          }}
        />
      </InspectorAdvancedControls>
    </>
  );
};

export default TabsInspectorControl;
