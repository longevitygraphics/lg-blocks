import { InnerBlocks, useBlockProps } from "@wordpress/block-editor";

const Save = props => {
  const { attributes } = props;
  const { tabs, mobileDropdownSelector } = attributes;

  const renderUl = () => {
    
    return tabs.map((tab, index) => {
      return (
        <li key={index} className="nav-item" role={"presentation"}>
          {(tab.headerMode === "image" && tab.mediaUrl && tab.mediaId) && (
            <img className={`wp-image-${tab.mediaId}`} src={tab.mediaUrl} />
          )}
          {(tab.headerMode === "fa" && tab.faIcon && tab.faStyle && tab.faColor) && (
            <i class={`${tab.faStyle} fa-${tab.faIcon}`} style={{ color: tab.faColor }} />
          )}
          <button
            href="#"
            className={`nav-link ${index === 0 ? "active" : ""}`}
            id={`${tab.key}-tab`}
            data-bs-toggle="tab"
            data-bs-target={`#${tab.key}`}
            type="button"
            role="tab"
            aria-controls={`${tab.key}`}
          >
            {tab.title}
          </button>
        </li>
      );
    });
    
  };

  const blockProps = useBlockProps.save();

  return (
    <div {...blockProps}>
      <>
      {mobileDropdownSelector && (
        <div className="active-selector" role={"presentation"}>
            {(tabs[0].headerMode === "image" && tabs[0].mediaUrl && tabs[0].mediaId) && (
              <img className={`wp-image-${tabs[0].mediaId}`} src={tabs[0].mediaUrl} />
            )}
            {(tabs[0].headerMode === "fa" && tabs[0].faIcon && tabs[0].faStyle && tabs[0].faColor) && (
              <i class={`${tabs[0].faStyle} fa-${tabs[0].faIcon}`} style={{ color: tabs[0].faColor }} />
            )}
              {tabs[0].title}
          </div>
      )}
      <ul className={`nav nav-tabs ${mobileDropdownSelector ? "hide" : ""}`} role="tablist">
        {renderUl()}
      </ul>
      </>
      <div className="tab-content">
        <InnerBlocks.Content />
      </div>
    </div>
  );
};

export default Save;
