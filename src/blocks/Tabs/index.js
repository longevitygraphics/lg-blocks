import { registerBlockType } from "@wordpress/blocks";
import { __ } from "@wordpress/i18n";
import "./Tab/index";
import Edit from "./edit.js";
import Save from "./save.js";
import metadata from './block.json';

import './style.scss';

registerBlockType(metadata.name, {
  edit: Edit,
  save: Save
});
