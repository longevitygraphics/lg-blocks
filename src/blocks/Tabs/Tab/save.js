import { InnerBlocks } from "@wordpress/block-editor";

const Save = props => {
  const { attributes, setAttributes } = props;
  const { title, key, tabNumber } = attributes;
  return (
    <div
      className={`tab-pane fade ${tabNumber === 0 ? "show active" : ""}`}
      role="tabpanel"
      aria-labelledby={`${key}-tab`}
      id={`${key}`}
    >
      <InnerBlocks.Content />
    </div>
  );
};

export default Save;
