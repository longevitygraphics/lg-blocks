import { InnerBlocks, RichText, useBlockProps, InspectorControls, MediaUpload, MediaUploadCheck, PanelColorSettings } from "@wordpress/block-editor";
import { withSelect } from "@wordpress/data";
import { useEffect } from "@wordpress/element";
import slugify from "slugify";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import {
  PanelBody,
  SelectControl,
  TextControl,
  Button,
  ResponsiveWrapper
} from "@wordpress/components";
import { __ } from "@wordpress/i18n";

const Edit = props => {
  const { attributes, setAttributes, tabNumber, context, media } = props;
  const { title, headerMode, mediaId, mediaUrl, faIcon, faStyle, faColor } = attributes;
  const blockProps = useBlockProps({
    className: `lg-blocks-tab tab-${tabNumber}`
  });

  useEffect(() => {
    setAttributes({ tabNumber: tabNumber });
  }, [tabNumber]);

  console.log(attributes);

  return (
    <>
      <InspectorControls>
        <PanelBody title={__("Tab Header Options", "lg-blocks")}>
          <SelectControl
            label="Header Mode"
            value={headerMode}
            options={[
              { label: "Text", value: "text" },
              { label: "Image", value: "image" },
              { label: "FontAwesome Icon", value: "fa" },
            ]}
            onChange={(v) => {
              console.log(v);
              setAttributes({ headerMode: v })
            }}
          />
          {headerMode === "image" && (
            <>
              <MediaUploadCheck>
                <MediaUpload
                  onSelect={(media) => {
                    setAttributes({
                      mediaId: media.id,
                      mediaUrl: media.url
                    })
                  }}
                  value={mediaId}
                  allowedTypes={['image']}
                  render={({ open }) => (
                    <Button
                      className={mediaId === 0 ? 'editor-post-featured-image__toggle' : 'editor-post-featured-image__preview'}
                      onClick={open}
                    >
                      {mediaId == 0 && __('Choose an image', 'awp')}
                      {props.media != undefined &&
                        <ResponsiveWrapper
                          naturalWidth={media.media_details.width}
                          naturalHeight={media.media_details.height}
                        >
                          <img src={media.source_url} />
                        </ResponsiveWrapper>
                      }
                    </Button>
                  )}
                />
              </MediaUploadCheck>
              {mediaId !== 0 && (
                <MediaUploadCheck>
                  <Button
                    onClick={() => {
                      setAttributes({
                        mediaId: 0,
                        mediaUrl: ''
                      })
                    }}
                    variant="link"
                    isDestructive>
                    {__('Remove image', 'lg-blocks')}
                  </Button>
                </MediaUploadCheck>
              )}
            </>
          )}
          {headerMode === "fa" && (
            <>
              <TextControl
                label={__(`Icon`)}
                value={faIcon}
                onChange={value => {
                  setAttributes({ faIcon: value });
                }}
              />
              <SelectControl
                label={__(`Icon Style`)}
                value={faStyle}
                options={[
                  { label: 'Regular', value: 'far' },
                  { label: 'Solid', value: 'fas' },
                  { label: 'Light', value: 'fal' },
                  { label: 'Thin', value: 'fat' },
                ]}
                onChange={(value) => setAttributes({ faStyle: value })}
              />
            </>
          )}
        </PanelBody>
        {headerMode === "fa" && (
          <PanelColorSettings
            title={__('Color Settings')}
            colorSettings={[
              {
                value: faColor,
                onChange: (colorValue) => setAttributes({ faColor: colorValue }),
                label: __('Icon Color'),
              },
            ]}
          >
          </PanelColorSettings>
        )}

      </InspectorControls>
      <div {...blockProps}>
        <div className="lg-blocks-tab__label">
          {(headerMode === "image" && mediaUrl) && (
            <img src={mediaUrl} />
          )}
          {(headerMode === "fa" && faIcon) && (
            <FontAwesomeIcon icon={[faStyle, faIcon]} color={faColor} style={ faColor === "#ffffff" ? { stroke: "#000"} : { }} />
          )}
          <RichText
            value={title}
            onChange={v => {
              setAttributes({ title: v, key: slugify(`tab-${v}`, { strict: true }) });
            }}

          />

        </div>
        <div className="lg-blocks-tab__content">
          <InnerBlocks template={[["core/heading"], ["core/paragraph"]]} />
        </div>
      </div>
    </>
  );
};

export default withSelect((select, props) => {
  const { clientId } = props;
  const coreEditor = select("core/block-editor");

  let parentID = coreEditor.getBlockParents(clientId);
  let parentBlock = coreEditor.getBlock(parentID);
  if (parentBlock !== null) {
    let childrenIDs = parentBlock.innerBlocks.map(child => {
      return child.clientId;
    });
    let tabNumber = childrenIDs.indexOf(clientId);
    return {
      tabNumber: tabNumber,
      media: props.attributes.mediaId ? select('core').getMedia(props.attributes.mediaId) : undefined
    };
  }
  return {
    tabNumber: undefined,
    media: props.attributes.mediaId ? select('core').getMedia(props.attributes.mediaId) : undefined
  }



})(Edit);
