import { Component } from "@wordpress/element";
import classnames from "classnames";
import { InnerBlocks, useBlockProps } from "@wordpress/block-editor";

const ALLOWED_BLOCKS = ['lg-blocks/accordion-item'];

import "./style.editor.scss";


const Edit = (props) => {

  const { className } = props;

  const blockProps = useBlockProps();

  return (
    <div {...blockProps}>
      <InnerBlocks
        allowedBlocks={ALLOWED_BLOCKS}
        template={[['lg-blocks/accordion-item', {}]]}
      />
    </div>
  )
}



// class Edit extends Component {

//   render() {
//     const {
//       className,
//     } = this.props;
//     const classes = classnames({
//       accordions: true,
//       [className]: !!className
//     });

//     return (
//       <div className={classes}>
//         <InnerBlocks
//           allowedBlocks={ALLOWED_BLOCKS}
//           template={[['lg-blocks/accordion-item', {}]]}
//         />
//       </div>
//     );
//   }
// }

export default Edit;
