/**
 * Wordpress dependencies
 */
import { Component } from "@wordpress/element";
import { InspectorControls } from "@wordpress/block-editor";
import { PanelBody, ToggleControl, SelectControl } from "@wordpress/components";
import { __ } from "@wordpress/i18n";

class Inspector extends Component {
  render() {
    const { attributes } = this.props;
    const { expanded, tagType } = attributes;

    return (
      <InspectorControls>
        <PanelBody title={__("Accordion Item settings", "lg-blocks")}>
          <ToggleControl
            label={__("Expanded?", "lg-blocks")}
            checked={expanded}
            onChange={value => this.props.setAttributes({ expanded: value })}
          />
          <SelectControl
            label={__("Question Tag", "lg-blocks")}
            value={tagType}
            options={[
              {
                label: "H1",
                value: "h1"
              },
              {
                label: "H2",
                value: "h2"
              },
              {
                label: "H3",
                value: "h3"
              },
              {
                label: "H4",
                value: "h4"
              },
              {
                label: "H5",
                value: "h5"
              },
              {
                label: "H6",
                value: "h6"
              },
              {
                label: "P",
                value: "p"
              }
            ]}
            onChange={value => {
              this.props.setAttributes({ tagType: value });
            }}
          />
        </PanelBody>
      </InspectorControls>
    );
  }
}

export default Inspector;
