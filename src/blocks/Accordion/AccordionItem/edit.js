/**
 * WordPress dependencies
 */
import { __ } from "@wordpress/i18n";
import { Component } from "@wordpress/element";
import { InnerBlocks, RichText, useBlockProps } from "@wordpress/block-editor";
import slugify from "slugify";

/**
 * External dependencies
 */
import classnames from "classnames";

/**
 * Local Dependencies
 */
import Inspector from "./inspector";

/**
 * Constants
 */
const TEMPLATE = [
  ["core/paragraph", { placeholder: __("Add content…", "lg-blocks") }]
];

const Edit = (props) => {


  const { attributes, setAttributes, isSelected } = props;
  
  const { title, expanded, tagType } = attributes;
  const accordionSlug = slugify(title, { strict: true });
  const accordionHeaderClasses = classnames({
    "accordion-header": true,
    collapsed: !expanded
  });
  const accordionContentClasses = classnames({
    "accordion-collapse": true,
    collapse: true,
    show: expanded
  });


  const blockProps = useBlockProps(
    { className: accordionHeaderClasses }
  )

  return (
    <div className="accordion-container">
      {isSelected && <Inspector {...props} />}
      <div

        {...blockProps}
        data-toggle="collapse"
        data-target={`#collapse-${accordionSlug}`}
        aria-expanded={expanded}
        aria-controls={`collapse-${accordionSlug}`}
      >
        <RichText
          tagName={tagType}
          value={title}
          onChange={title => {
            setAttributes({ title });
          }}
        />
      </div>

      <div
        id={`collapse-${accordionSlug}`}
        className={accordionContentClasses}
      >
        <div className="accordion-body">
          <InnerBlocks
            template={TEMPLATE}
          />
        </div>
      </div>
    </div>
  );


}

export default Edit;
