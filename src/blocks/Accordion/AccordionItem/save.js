import { InnerBlocks, RichText, useBlockProps } from "@wordpress/block-editor";
import slugify from "slugify";
import classnames from "classnames";

const save = (props) => {
  const { attributes } = props;
  const { title, expanded, tagType } = attributes;
  const accordionSlug = slugify(title, { strict: true });
  const accordionHeaderClasses = classnames({
    "accordion-header": true,
    collapsed: !expanded
  });
  const accordionContentClasses = classnames({
    "accordion-collapse": true,
    collapse: true,
    show: expanded
  });
  const blockProps = useBlockProps.save(
    { className: accordionHeaderClasses }
  );
  return (
    <div className="accordion-container">
      <div
        {...blockProps}
        data-bs-toggle="collapse"
        data-bs-target={`#collapse-${accordionSlug}`}
        aria-expanded="true"
        aria-controls={`collapse-${accordionSlug}`}
      >
        <RichText.Content tagName={tagType} value={title} />
      </div>
      <div id={`collapse-${accordionSlug}`} className={accordionContentClasses}>
        <div className="accordion-body">
          <InnerBlocks.Content />
        </div>
      </div>
    </div>
  );
}

export default save;
