/**
 * Internal dependencies
 */

import Edit from "./edit";
import save from "./save";
import metadata from './block.json';

/**
 * Wordpress dependencies
 */
import { registerBlockType } from "@wordpress/blocks";
import { __ } from "@wordpress/i18n";

registerBlockType(metadata.name, {
  edit: Edit,
  save: save
});
