/**
 * External dependencies
 */
import classnames from "classnames";

/**
 * WordPress dependencies
 */
import { Fragment, RawHTML } from "@wordpress/element";

import { __ } from "@wordpress/i18n";

const Save = props => {
  {
    const { attributes, isSelected } = props;
    const { className, title, content, hasAllRights } = attributes;

    const classes = classnames({
      copyright: true,
      [className]: !!className
    });

    const currentYear = new Date().getFullYear();

    return (
      <Fragment>
        <div className={classes}>
          <div>
            © <span className="currentYear" id="lg-copyright-current-year">{currentYear}</span> {title}
          </div>
          <div>
           {hasAllRights && ("All Rights Reserved")}{" "}
          </div>
          <div>
            {content && (
              <span className="lg-copyright-custom">
                <RawHTML>{content}</RawHTML>
              </span>
            )}
          </div>
        </div>
      </Fragment>
    );
  }
};

export default Save;
