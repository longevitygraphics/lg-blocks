/**
 * External dependencies
 */
import classnames from "classnames";

/**
 * WordPress dependencies
 */
import { Fragment, RawHTML } from "@wordpress/element";
import { withSelect } from "@wordpress/data";

import { __ } from "@wordpress/i18n";
import { InspectorControls } from "@wordpress/block-editor";
import { PanelBody, TextareaControl, ToggleControl } from "@wordpress/components";

const Edit = props => {
  {
    const { attributes, setAttributes, title } = props;
    const { className, content, hasAllRights } = attributes;

    const classes = classnames({
      copyright: true,
      [className]: !!className
    });

    const currentYear = new Date().getFullYear();

    setAttributes({ title: title });

    return (
      <Fragment>
        <InspectorControls>
          <PanelBody title={__("Custom Content", "lg-blocks")}>
            <TextareaControl
              label={__("Additional Content", "lg-blocks")}
              onChange={e => {
                setAttributes({ content: e });
              }}
              value={content}
            />
            <ToggleControl 
              label={__("All Rights Reserved?", "lg-blocks")}
              onChange={() => {
                setAttributes({ hasAllRights: !hasAllRights})
              }}
              checked={hasAllRights}
            />
          </PanelBody>
        </InspectorControls>
        <Fragment>
          <div className={classes}>
            <div>
              © {currentYear} {title} {hasAllRights && ("All Rights Reserved")}{" "}
              {content && (
                <span className="lg-copyright-custom">
                  {<RawHTML>{content}</RawHTML>}
                </span>
              )}
            </div>
          </div>
        </Fragment>
      </Fragment>
    );
  }
};

export default withSelect((select, props) => {
  const core = select("core");
  return {
    title: core.getSite().title
  };
})(Edit);
