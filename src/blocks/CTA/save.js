import { getColorClassName, RichText } from "@wordpress/block-editor";
import classnames from "classnames";

function save({ attributes }) {
  const {
    content,
    textColor,
    customTextColor,
    backgroundColor,
    customBackgroundColor,
    buttonTitle,
    buttonUrl,
    isExternal,
    buttonClasses
  } = attributes;

  //console.log(attributes);

  const backgroundClass = getColorClassName(
    "background-color",
    backgroundColor
  );
  const textClass = getColorClassName("color", textColor);

  const classes = classnames({
    cta: true,
    [backgroundClass]: backgroundClass,
    [textClass]: textClass
  });
  return (
    <div
      className={classes}
      style={{
        backgroundColor: backgroundClass ? undefined : customBackgroundColor,
        color: textClass ? undefined : customTextColor
      }}
    >
      <div className="container">
        <div className="cta__content">
          <RichText.Content
            tagName="h2"
            value={content}
            className={textClass}
          />
          <a
            className={buttonClasses}
            href={buttonUrl}
            rel={isExternal && "noopener noreferrer"}
            target={isExternal && "_blank"}
          >
            {buttonTitle}
          </a>
        </div>
      </div>
    </div>
  );
}

export default save;
