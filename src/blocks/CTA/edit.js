import { Component } from "@wordpress/element";
import {
  ContrastChecker,
  PanelColorSettings,
  RichText,
  URLInput,
  InspectorControls,
  withColors
} from "@wordpress/block-editor";
import { __ } from "@wordpress/i18n";
import { ColorPalette, PanelBody, ToggleControl } from "@wordpress/components";
import classnames from "classnames";

import "./style.editor.scss";

class Edit extends Component {
  editorColors = wp.data.select("core/editor").getEditorSettings().colors;
  state = {
    currentButtonColor: null
  };

  onContentChange = content => {
    this.props.setAttributes({ content });
  };

  onButtonToggle = value => {
    this.props.setAttributes({ isExternal: value });
  };
  onButtonColorSchemeChanged = value => {
    //console.log(value);
    //console.log(this.editorColors);
    const currentColor = this.editorColors.find(x => x.color === value);
    //console.log(currentColor);
    this.props.setAttributes({
      buttonClasses: `cta__link btn btn-${currentColor.slug}`
    });
  };

  //onButtonStyleToggle = value => {};

  render() {
    //console.log(this.props);
    const {
      attributes,
      backgroundColor,
      textColor,
      setBackgroundColor,
      setTextColor,
      setAttributes
    } = this.props;
    const {
      content,
      buttonTitle,
      buttonUrl,
      isExternal,
      buttonClasses,
      className
    } = attributes;

    const classes = classnames({
      cta: true,
      [className]: !!className
    });

    return (
      <>
        <InspectorControls>
          <PanelBody title="Button Settings" initialOpen={false}>
            <ToggleControl
              checked={isExternal}
              onChange={this.onButtonToggle}
              label={__("Open link in new tab", "lg-blocks")}
            />
            <p>{__("Button color scheme", "lg-blocks")}</p>
            <ColorPalette
              disableCustomColors={true}
              onChange={this.onButtonColorSchemeChanged}
              colors={this.editorColors}
            />
            <p>{__("Button style", "lg-blocks")}</p>
            <ToggleControl
              onChange={this.onButtonStyleToggle}
              label={__("Outline style", "lg-blocks")}
            />
          </PanelBody>
          <PanelColorSettings
            title={__("Color Settings", "lg-blocks")}
            initialOpen={false}
            colorSettings={[
              {
                value: backgroundColor.color,
                onChange: setBackgroundColor,
                label: __("Background Color", "lg-blocks")
              },
              {
                value: textColor.color,
                onChange: setTextColor,
                label: __("Text Color", "lg-blocks")
              }
            ]}
          >
            <ContrastChecker
              textColor={textColor.color}
              backgroundColor={backgroundColor.color}
            />
          </PanelColorSettings>
        </InspectorControls>
        <div
          className={classes}
          style={{
            backgroundColor: backgroundColor.color,
            color: textColor.color
          }}
        >
          <div className="container">
            <div className="cta__content">
              <RichText
                tagName="h2"
                value={content}
                style={{ color: textColor.color }}
                onChange={this.onContentChange}
                placeholder={__("Add some text here...", "lg-blocks")}
              />
              <div className="">
                <RichText
                  tagName="a"
                  className={buttonClasses}
                  value={buttonTitle}
                  onChange={value => setAttributes({ buttonTitle: value })}
                  placeholder={__("Add button title...", "lg-blocks")}
                />
                {this.props.isSelected && (
                  <URLInput
                    value={buttonUrl}
                    autoFocus={false}
                    className="cta__input"
                    isFullWidth
                    hasBorder
                    onChange={value => setAttributes({ buttonUrl: value })}
                  />
                )}
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default withColors("backgroundColor", { textColor: "color" })(Edit);
