import classnames from "classnames";
import {RawHTML} from "@wordpress/element";
import {
  getColorClassName,
  InnerBlocks,
  RichText
} from "@wordpress/block-editor";

const save = ({attributes}) => {
  const {
    title,
    subTitle,
    sameHeight,
    mapHorizontalAlignment,
    mapVerticalAlignment,
    textColor,
    customTextColor,
    backgroundColor,
    customBackgroundColor,
    backgroundImageUrl
  } = attributes;
  const backgroundClass = getColorClassName(
    "background-color",
    backgroundColor
  );
  const textClass = getColorClassName("color", textColor);

  const classes = classnames({
    "map-text": true,
    "map-text--sameHeight": sameHeight,
    [backgroundClass]: backgroundClass,
    [textClass]: textClass
  });
  const innerClasses = classnames({
    "map-text__inner": true,
    "align-items-start": !sameHeight && mapVerticalAlignment === "top",
    "align-items-center": !sameHeight && mapVerticalAlignment === "center",
    "align-items-end": !sameHeight && mapVerticalAlignment === "bottom"
  });
  const mapClasses = classnames({
    "map-text__map": true
  });
  const contentClasses = classnames({
    "map-text__content": true,
    "order-md-12": mapHorizontalAlignment === "right",
    "justify-content-start": sameHeight && mapVerticalAlignment === "top",
    "justify-content-center": sameHeight && mapVerticalAlignment === "center",
    "justify-content-end": sameHeight && mapVerticalAlignment === "bottom"
  });
  const contentStyles = {};
  const styles = {
    color: textClass ? undefined : customTextColor
  };
  if (sameHeight) {
    contentStyles.backgroundColor = backgroundClass
      ? undefined
      : customBackgroundColor;
  } else {
    styles.backgroundColor = backgroundClass
      ? undefined
      : customBackgroundColor;
  }
  if (backgroundImageUrl) {
    styles.backgroundImage = `url(${backgroundImageUrl})`;
  }
  return (
    <div className={classes} style={styles}>
      {(title || subTitle) && (
        <div className="map-text__titles">
          <RichText.Content tagName="h2" value={title}/>
          <RichText.Content tagName="h4" value={subTitle}/>
        </div>
      )}
      <div className={innerClasses}>
        <div className={contentClasses} style={contentStyles}>
          <InnerBlocks.Content/>
        </div>
        <div className={mapClasses}>
          <RawHTML>{attributes.iframe}</RawHTML>
        </div>
      </div>
    </div>
  );
};

export default save;
