/**
 * Internal dependencies
 */
import "./style.editor.scss";
import Edit from "./edit";
import save from "./save";

/**
 * Wordpress dependencies
 */
import { registerBlockType } from "@wordpress/blocks";
import { __ } from "@wordpress/i18n";
import metadata from './block.json';

import './style.scss';

registerBlockType(metadata.name, {
  edit: Edit,
  save: save
});
