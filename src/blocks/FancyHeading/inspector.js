/**
 * Wordpress dependencies
 */
import { Component } from "@wordpress/element";
import { InspectorControls } from "@wordpress/block-editor";
import { PanelBody, ToggleControl } from "@wordpress/components";
import { __ } from "@wordpress/i18n";

class Inspector extends Component {
  render() {
    const { attributes } = this.props;
    const { hasButton } = attributes;

    return (
      <InspectorControls>
        <PanelBody title={__("Fancy Heading settings", "lg-blocks")}>
          <ToggleControl
            label={__("Has button?", "lg-blocks")}
            checked={hasButton}
            onChange={value => this.props.setAttributes({ hasButton: value })}
          />
        </PanelBody>
      </InspectorControls>
    );
  }
}

export default Inspector;
