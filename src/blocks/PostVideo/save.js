function Save(props) {
  const { attributes } = props;
  const { videoId, videoUrl, videoThumbnail } = attributes;

  return (
    <div id={videoId} className="lg-vimeo-mount-container video-link">
      <a href={videoUrl}>
        <img src={videoThumbnail.large} />
      </a>
    </div>
  );
}

export default Save;
