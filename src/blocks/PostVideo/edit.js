/**
 * External Dependencies.
 */
import classnames from "classnames";
import Player from "@vimeo/player";

/**
 * Wordpress Dependencies.
 */

import { __ } from "@wordpress/i18n";
import { Button } from "@wordpress/components";
import { withSelect, withDispatch } from "@wordpress/data";
import { Icon, video } from "@wordpress/icons";
import { useEffect } from "@wordpress/element";
import { compose } from "@wordpress/compose";
import VSBlockControls from "./block-controls";
import { useEntityProp } from "@wordpress/core-data";
import { isEmpty } from "lodash";

import "./style.editor.scss";

const Edit = props => {
  const { attributes, setAttributes, className, isSelected, context } = props;

  const { videoUrl, videoProvider, videoId, videoThumbnail } = attributes;

  const { postId, postType, queryId } = context;

  const [meta, setMeta] = useEntityProp("postType", postType, "meta", postId);

  const [title, setTitle] = useEntityProp(
    "postType",
    postType,
    "title",
    postId
  );

  const classes = classnames({
    [className]: !!className,
    active: isSelected
  });

  const getVideoURLfromMeta = video => {
    console.log(video);
    console.log(title);
    if (video !== "" && video !== undefined && !isEmpty(video)) {
      setAttributes({ videoUrl: video });
      const videoDetails = determineVideoProvider(video);
      console.log(videoThumbnail.large);
      if (
        videoThumbnail.large ===
        "https://via.placeholder.com/800x400.png?text=Thumbnail%20Not%20Loaded"
      ) {
        getVideoThumbnail(videoId);
      }

      setAttributes({ videoProvider: videoDetails.provider });
      setAttributes({ videoId: videoDetails.id });
    } else {
      setAttributes({ videoUrl: undefined });
    }
  };

  const determineVideoProvider = url => {
    let data;

    try {
      data = new URL(url);
    } catch (error) {
      console.log("Invalid URL");
      data = {};
      data.provider = "unknown";
      return data;
    }

    switch (data.hostname) {
      case "vimeo.com":
      case "player.vimeo.com":
        data.provider = "vimeo";
        data.id = data.pathname.split("/").pop();
        break;
      case "www.youtube.com":
        data.provider = "youtube";
        break;
      default:
        data.provider = "unknown";
        break;
    }

    return data;
  };

  const getVideoThumbnail = async id => {
    if (videoProvider === "vimeo" && videoId) {
      const result = await fetch(
        `https://vimeo.com/api/v2/video/${videoId}.json`,
        {
          cache: "no-cache",
          method: "GET",
          redirect: "follow",
          referrer: "no-referrer"
        }
      )
        .then(response => response.json())
        .then(data => {
          let thumbnails = {
            small: data[0].thumbnail_small,
            medium: data[0].thumbnail_medium,
            large: data[0].thumbnail_large
          };
          console.log(thumbnails);
          setAttributes({ videoThumbnail: thumbnails });
        });
    }
  };

  useEffect(() => {
    if (videoProvider === "vimeo" && videoId) {
      const player = new Player(videoId, {
        id: videoId,
        responsive: "true",
        title: false
      });
    }
  });

  useEffect(() => {
    if (videoProvider === "vimeo" && videoId) {
      getVideoThumbnail(videoId);
    }
  }, []);

  useEffect(() => {
    if (videoProvider === "vimeo" && videoId) {
      getVideoThumbnail(videoId);
    }
  }, [videoThumbnail.large]);

  const isDescendentOfQueryLoop = Number.isFinite(queryId);

  getVideoURLfromMeta(meta.longevity_video_video_url);

  if (!videoUrl || videoProvider === "unknown" || videoUrl === undefined) {
    return (
      <div className={classes}>
        <div className="video-input">
          <div className="components-placeholder is-large">
            <div className="components-placeholder__label">
              <span className="block-editor-block-icon">
                <Icon icon={video} size={24} />
              </span>
              Post Video
            </div>
            <div className="components-placeholder__instructions">
              {__("Enter the video source URL", "lg-blocks")}
            </div>
            <div className="components-placeholder__fieldset">
              <form
                onSubmit={event => {
                  event.preventDefault();
                  let video = event.target[0].value;
                  setMeta({ longevity_video_video_url: video });
                  getVideoURLfromMeta(video);
                }}
              >
                <input
                  type="url"
                  className="components-placeholder__input"
                  aria-label={__("Video Source URL", "lg-blocks")}
                  placeholder={__(`Update Post ${title}'s Video Postmeta`)}
                />
                <Button variant="primary" type="submit">
                  {__("Submit", "lg-blocks")}
                </Button>
              </form>
              {videoUrl}
            </div>
          </div>
        </div>
      </div>
    );
  } else {
    return (
      <>
        <VSBlockControls setAttributes={setAttributes} />
        <div className={classes}>
          <div className="wp-block-lg-blocks-advanced-slide__video">
            <div id={videoId}></div>
          </div>
        </div>
      </>
    );
  }
};

export default compose([
  withSelect(select => {
    return {
      postMeta: select("core/editor").getEditedPostAttribute("meta")
    };
  }),
  withDispatch(dispatch => {
    return {
      setPostMeta(newMeta) {
        dispatch("core/editor").editPost({ meta: newMeta });
      }
    };
  })
])(Edit);
