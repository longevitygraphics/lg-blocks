import { Component } from "@wordpress/element";
import {
  BlockControls,
  BlockVerticalAlignmentToolbar,
  MediaUpload,
  MediaUploadCheck
} from "@wordpress/block-editor";
import { Button, Toolbar, ToolbarButton } from "@wordpress/components";
import { __ } from "@wordpress/i18n";

const VSBlockControls = props => {
  const { setAttributes } = props;

  return (
    <BlockControls>
      <Toolbar>
        <Button
          className="components-icon-button components-toolbar__control"
          label={__("Remove Video URL", "lg-blocks")}
          onClick={e => {
            setAttributes({ videoUrl: "", videoProvider: "unknown" });
          }}
          icon="trash"
        />
      </Toolbar>
    </BlockControls>
  );
};

export default VSBlockControls;
