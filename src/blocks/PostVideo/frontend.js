import Player from "@vimeo/player";

const vimeoPlayers = document.querySelectorAll(".lg-vimeo-mount-container");

vimeoPlayers.forEach((vimeoPlayer, index) => {
  let options = {
    id: vimeoPlayer.id,
    responsive: false,
    title: false,
    controls: false,
    loop: true
  };

  if (screen.width < 768) {
    options.height = screen.availHeight;
  } else {
    options.width = screen.availWidth;
  }

  console.log(options);

  let player = new Player(vimeoPlayer.id, options);
  player.play();
});
