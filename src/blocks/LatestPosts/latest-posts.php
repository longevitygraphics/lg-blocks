<?php

function lg_blocks_render_latest_posts_block($attributes)
{
  $args = array(
    'posts_per_page' => $attributes['numberOfPosts']
  );

  $classes = 'wp-block-lg-blocks-latest-posts ';
  $classes .= 'has-' . $attributes['numberOfPosts'] . '-columns ';
  if (isset($attributes['className'])) {
    $classes .= $attributes['className'];
  }
  $query = new WP_Query($args);
  $posts = '';
  if ($query->have_posts()) {
    $posts .= '<div class=" ' . $classes . ' "> ';
    while ($query->have_posts()) {
      $query->the_post();
      $posts .=
        '<div class="lg-post card">
                    <div class="lg-post__image card-img-top"> ' .
        get_the_post_thumbnail(null, null) .
        '</div>
                    <div class="lg-post__body card-body">
                        <div class="lg-post__title py-2"> ' .
        get_the_title() .
        '</div>
                        <div class="lg-post__exerpt"> ' .
        get_the_excerpt() .
        '</div>
                        <div class="lg-post__link"> ' .
        '<a href="' .
        esc_url(get_the_permalink()) .
        '"> Learn More </a>' .
        '</div>
                    </div>
                </div>';
    }

    $posts .= '</div>';
    wp_reset_postdata();
    return $posts;
  } else {
    return '<div>' . __("No Posts Found", "lg-blocks") . '</div>';
  }
}

?>
