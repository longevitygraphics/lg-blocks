import edit from "./edit";
import { registerBlockType } from "@wordpress/blocks";
import { __ } from "@wordpress/i18n";

import metadata from './block.json';

import './style.scss';

registerBlockType(metadata.name, {
  edit: edit,
  save() {
    return null;
  }
});
