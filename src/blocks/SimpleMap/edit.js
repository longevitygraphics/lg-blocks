import { useBlockProps, PlainText } from "@wordpress/block-editor";
import { Placeholder } from "@wordpress/components";

import "./style.editor.scss";
import { useState } from "react";
import { RawHTML } from "@wordpress/element";


const Edit = ({ attributes, setAttributes }) => {

  const blockProps = useBlockProps();
  const [input, setInput] = useState("");
  const [invalid, setInvalid] = useState(false);

  return (
    <div {...blockProps}>
      {!attributes.url && (
        <Placeholder icon={"google"} label="Embed Google Map" instructions="Paste a link to a google maps embed you want to display on your site." inputMode="url" >
          <input type="url" class="components-placeholder__input" aria-label="Embed URL" placeholder="Enter URL to embed here…" value={input} onChange={(e) => { setInput(e.target.value) }} />
          <button type="submit" class="components-button is-primary" onClick={() => {
            console.log(input);
            const srcRegex = /<iframe.*?src="(.*?)".*?>/i;
            let match = input.match(srcRegex);
            if (match && match[1]) {
              console.log(match[1]);
              setAttributes({ url: match[1] });
            } else {
              const googleRegex = /https:\/\/www\.google\.com\/maps\/embed/;
              let googleMatch = input.match(googleRegex);
              if (googleMatch) {
                setAttributes({ url: input });
              } else {
                setInvalid(true);
                setAttributes({ url: undefined });
              }
            }
          }}
          >Embed</button>
          {invalid && (
            <p style={{ flexBasis: "100%", margin: 0 }}>Invalid URL Provided.</p>
          )}
        </Placeholder>
      )}
      {attributes.url && (
       <iframe src={attributes.url} style={{ width: "100%", minHeight: "400px"}}></iframe>
      )}

    </div>
  );
}


export default Edit;
