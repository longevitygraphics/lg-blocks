import { useBlockProps } from "@wordpress/block-editor";

import "./style.scss";

const Save = ({ attributes }) => {

  const blockProps = useBlockProps.save()

  return (
    <div {...blockProps}>
      <iframe src={attributes.url} style={{ width: "100%", minHeight: "400px", border: "0"}} loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
    </div>
  );
}

export default Save;
