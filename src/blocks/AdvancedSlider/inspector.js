/**
 *
 * DEPENDENCIES
 *
 *
 */

import {
  InspectorControls,
  InspectorAdvancedControls
} from "@wordpress/block-editor";
import {
  PanelBody,
  RangeControl,
  ToggleControl,
  ButtonGroup,
  Button,
  Icon,
  ColorPalette,
  TextControl
} from "@wordpress/components";

/**
 * https://developer.wordpress.org/block-editor/reference-guides/packages/packages-i18n/#x
 * Retrieve the translation of text.
 */
import { __ } from "@wordpress/i18n";

const AdvancedSliderInspectorControls = props => {
  const { attributes, setAttributes, onResponsiveSettingUpdate } = props.data;
  const {
    hasSlideContent,
    hasThumbs,
    hasArrows,
    hasDots,
    autoHeight,
    hasDesktopThumbGallery,
    breakpoint,
    forcePreviewMode,
    sliderConfig,
    thumbsConfig,
  } = attributes;

  const onSliderConfigUpdate = (attribute, value) => {
    let newSliderConfig = { ...sliderConfig };
    newSliderConfig[attribute] = value;
    setAttributes({ sliderConfig: newSliderConfig });
  };

  const onThumbConfigUpdate = (attribute, value) => {
    let newSliderConfig = { ...thumbsConfig };
    newSliderConfig[attribute] = value;
    setAttributes({ thumbsConfig: newSliderConfig });
  };

  return (
    <>
      <InspectorControls>
        <PanelBody title={__("Slider Configuration", "lg-blocks")}>
          <ToggleControl
            label="Slide Content?"
            checked={hasSlideContent}
            onChange={value => {
              setAttributes({ hasSlideContent: value });
            }}
          />
          <ToggleControl
            label="Arrows"
            checked={hasArrows}
            onChange={value => {
              setAttributes({ hasArrows: value });
            }}
          />
          <ToggleControl
            label="Dots"
            checked={hasDots}
            onChange={value => {
              setAttributes({ hasDots: value });
            }}
          />
          <ToggleControl
            label="Fade Transition"
            checked={sliderConfig.effect}
            onChange={value => {
              if (sliderConfig.effect) {
                onSliderConfigUpdate("effect", false);
                // onSliderConfigUpdate("crossFade", undefined);
              }
              else { 
                onSliderConfigUpdate("effect", "fade");
                // onSliderConfigUpdate("crossFade", true);
              }
              
            }}
          />
          <ToggleControl
            label="Auto Height"
            checked={sliderConfig.autoHeight}
            onChange={value => {
              onSliderConfigUpdate("autoHeight", value);
            }}
          />
          <TextControl 
            label="Space Between"
            type="number"
            value={sliderConfig.spaceBetween}
            onChange={ value => {
              onSliderConfigUpdate("spaceBetween", value);
            }}
          />
          <ToggleControl
            label="Have Thumbnail Slider?"
            checked={hasThumbs}
            onChange={value => {
              setAttributes({ hasThumbs: value });
            }}
          />
          <ToggleControl
            label="Autoplay?"
            checked={sliderConfig.autoplay}
            onChange={value => {
              if (sliderConfig.autoplay) {
                onSliderConfigUpdate("autoplay", false)
              }
              else { 
                onSliderConfigUpdate("autoplay", {
                  delay: 5000,
                  disableOnInteraction: true,
                })
              }
              
            }}
          />
        </PanelBody>
        {hasThumbs && (
          <PanelBody title={__("Thumb Configuration", "lg-blocks")}>
            <ToggleControl
              label="Desktop Thumb Gallery Mode?"
              checked={hasDesktopThumbGallery}
              onChange={value => {
                setAttributes({ hasDesktopThumbGallery: value });
              }}
            />
            <RangeControl
            label="Thumb Visible Slides"
            value={ thumbsConfig.slidesPerView }
            onChange={value => {
                onThumbConfigUpdate("slidesPerView", value)
            }}
            min={1}
            max={8}
          />
          <TextControl 
            label="Space Between"
            type="number"
            value={thumbsConfig.spaceBetween}
            onChange={ value => {
              onThumbConfigUpdate("spaceBetween", value);
            }}
          />
          </PanelBody>
        )}

        <PanelBody title={__("Responsive Settings")}>
          <ButtonGroup>
            <Button
              isSmall
              title="Mobile(Default)"
              isPrimary={breakpoint === "mobile"}
              onClick={() => setAttributes({ breakpoint: "mobile" })}
            >
              <Icon icon={"smartphone"} />
            </Button>
            <Button
              isSmall
              title="Tablet"
              isPrimary={breakpoint === "tablet"}
              onClick={() => setAttributes({ breakpoint: "tablet" })}
            >
              <Icon icon={"tablet"} />
            </Button>
            <Button
              isSmall
              title="Desktop"
              isPrimary={breakpoint === "desktop"}
              onClick={() => setAttributes({ breakpoint: "desktop" })}
            >
              <Icon icon={"desktop"} />
            </Button>
          </ButtonGroup>
          <RangeControl
            label="Columns"
            value={
              breakpoint === "mobile"
                ? sliderConfig.slidesPerView // mobile
                : breakpoint === "tablet"
                ? sliderConfig.breakpoints["768"].slidesPerView //tablet
                : sliderConfig.breakpoints["1024"].slidesPerView // desktop
            }
            onChange={value =>
              onResponsiveSettingUpdate(breakpoint, "slidesPerView", value)
            }
            min={1}
            max={10}
            step={0.1}
            withInputField={true}
            isShiftStepEnabled={true}
            shiftStep={0.1}
          />
        </PanelBody>
      </InspectorControls>
      <InspectorAdvancedControls>
        <ToggleControl
          label="Force Preview Mode?"
          checked={forcePreviewMode}
          onChange={value => {
            setAttributes({ forcePreviewMode: value });
          }}
        />
      </InspectorAdvancedControls>
    </>
  );
};

export default AdvancedSliderInspectorControls;
