import {Swiper  } from "swiper/core";
import { Navigation, Pagination, Autoplay, EffectFade, Thumbs } from "swiper/modules";
import jQuery from "jquery";

Swiper.use([Navigation, Pagination, Autoplay, EffectFade, Thumbs]);


jQuery(document).ready(function(){

  const swiperSliders = document.querySelectorAll(
    ".lg-advanced-slider"
  );
  
  
  console.log("View Script for Slider Loaded!");
  console.log(swiperSliders);
  
  window.test = swiperSliders;
  
  swiperSliders.forEach((swiperSlider, index) => {
    let children = swiperSlider.childNodes;
  
    if (children.length > 1) {
      window.advancedslider = [];
      window.advancedslider[index] = {};
  
      let thumbConfig = JSON.parse(swiperSlider.children[1].dataset.swiper);
      let mainConfig = JSON.parse(swiperSlider.children[0].dataset.swiper);

      mainConfig.fadeEffect = {
        crossFade: true
      };

      thumbConfig.centerInsufficientSlides = true;

      mainConfig.thumbs = {
        autoScrollOffset: 1,
        swiper: {
          el: ".thumb",
          ...thumbConfig
        }
      };

      
      let slideCount = swiperSlider.children[1].children[0].children.length;
  
      thumbConfig.slidesPerView = slideCount < thumbConfig.slidesPerView ? slideCount : thumbConfig.slidesPerView;
  
      const thumb = new Swiper(swiperSlider.children[1], thumbConfig);
  
      console.log("main", mainConfig);
      console.log("thumb", thumbConfig);
  
      const main = new Swiper(swiperSlider.children[0], mainConfig);
      main.allowSlideNext = true;
    
      main.on("slideChange", function(e) {
        thumb.activeIndex = main.activeIndex;
        thumb.updateSlidesClasses();
      });
  
      thumb.allowTouchMove = true;
  
      window.advancedslider[index].thumb = thumb;
      window.advancedslider[index].main = main;

    } else {
      console.log("Not a thumb slider!");
      let swiperConfig = JSON.parse(swiperSlider.children[0].dataset.swiper);
      swiperConfig.cssMode = undefined;
      swiperConfig.grabCursor = true;
      swiperConfig.thumbs = undefined;
      swiperConfig.controller = undefined;
      // swiperConfig.effect = "fade";
      swiperConfig.crossFade = true,
      swiperConfig.fadeEffect = {
        crossFade: true
      };

      console.log(swiperConfig);

      if (window.advancedslider === undefined) {
        window.advancedslider = [];
      }
  
      window.advancedslider[index] = new Swiper(
        swiperSlider.children[0],
        swiperConfig
      );
    }
  });


});


