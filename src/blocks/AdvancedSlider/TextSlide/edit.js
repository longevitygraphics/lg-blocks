/**
 * External Dependencies.
 */
import classnames from "classnames";

/**
 * Wordpress Dependencies.
 */
import { InnerBlocks, MediaPlaceholder, useBlockProps } from "@wordpress/block-editor";
import { __ } from "@wordpress/i18n";
import { withSelect } from "@wordpress/data";



/**
 *
 */
 import "./style.editor.scss";

const ALLOWED_BLOCKS = [
  "core/paragraph",
  "core/heading",
  "core/buttons",
  "core/image",
  "core/list",
  "core/group",
  "lg-blocks/breadcrumb"
];

const TEMPLATE = [["core/paragraph"]];

const Edit = props => {
  const {
    attributes,
    setAttributes,
    className,
    isSelected,
    isChildSelected
  } = props;


  setAttributes({ hasSlideContent: props.hasSlideContent });

  const classes = classnames({
    [className]: !!className,
    active: isSelected || isChildSelected
  });

  const blockProps = useBlockProps(
    { className: classes }
  )

  return (
    <div { ...blockProps } >  
        <div className="wp-block-lg-blocks-text-slide__content">
          <InnerBlocks allowedBlocks={ALLOWED_BLOCKS} template={TEMPLATE} />
        </div>
    </div>
  );
};

export default withSelect((select, props) => {
  const { clientId } = props;
  const coreEditor = select("core/block-editor");
  const parentBlock = coreEditor.getBlockParentsByBlockName(
    clientId,
    "lg-blocks/advanced-slider"
  );
  const isChildSelected = coreEditor.hasSelectedInnerBlock(clientId, true);

  let hasSlideContent = false;
  if (parentBlock.length > 0 && parentBlock[0]) {
    const parentBlockId = parentBlock[0];
    const parentBlockAttributes = coreEditor.getBlockAttributes(parentBlockId);
    hasSlideContent = parentBlockAttributes.hasSlideContent;
  }

  return { hasSlideContent: hasSlideContent, isChildSelected: isChildSelected };
})(Edit);
