import { Component } from "@wordpress/element";
import {
  BlockControls,
  BlockVerticalAlignmentToolbar,
  MediaUpload,
  MediaUploadCheck
} from "@wordpress/block-editor";
import { Button, Toolbar, ToolbarButton } from "@wordpress/components";
import { __ } from "@wordpress/i18n";

const ASBlockControls = props => {
  const { onSelectImage, mediaId } = props;

  return (
    <BlockControls>
      <Toolbar>
        <MediaUploadCheck>
          <MediaUpload
            onSelect={onSelectImage}
            allowedTypes={["image"]}
            value={mediaId}
            render={({ open }) => {
              return (
                <Button
                  className="components-icon-button components-toolbar__control"
                  label={__("Change Slide Image", "lg-blocks")}
                  onClick={open}
                  icon="format-image"
                />
              );
            }}
          />
        </MediaUploadCheck>
      </Toolbar>
    </BlockControls>
  );
};

export default ASBlockControls;
