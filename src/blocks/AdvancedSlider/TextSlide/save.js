/**
 * External Dependencies
 */
import classnames from "classnames";

/**
 * Wordpress Dependencies
 */
import { InnerBlocks } from "@wordpress/block-editor";


function Save(props) {
  return (
    <div className="swiper-slide">
      <div className="swiper-slide__content">
       <InnerBlocks.Content />
      </div>
    </div>
  );
}

export default Save;
