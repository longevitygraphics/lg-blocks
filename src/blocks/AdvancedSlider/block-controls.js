import {
  BlockControls,
  AlignmentToolbar,
  MediaUploadCheck,
  MediaUpload
} from "@wordpress/block-editor";
import { Toolbar, Button } from "@wordpress/components";
import { __ } from "@wordpress/i18n";

const AdvancedSliderBlockControls = props => {
  const { attributes, setAttribute, onImageSelect } = props.data;
  const { alignment } = attributes;
  return (
    <BlockControls>
      <Toolbar>
        <MediaUploadCheck>
          <MediaUpload
            onSelect={e => {
              onImageSelect(e);
            }}
            multiple="true"
            allowedTypes={["image"]}
            value={null}
            render={({ open }) => {
              return (
                <Button
                  className="components-icon-button components-toolbar__control"
                  label={__("Add Images", "lg-blocks")}
                  onClick={open}
                  icon="format-image"
                />
              );
            }}
          />
        </MediaUploadCheck>
      </Toolbar>
    </BlockControls>
  );
};

export default AdvancedSliderBlockControls;
