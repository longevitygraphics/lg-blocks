/**
 * classnames - https://www.npmjs.com/package/classnames
 * A simple JavaScript utility for conditionally joining classNames together.
 */
import classnames from "classnames";

/**
 * https://developer.wordpress.org/block-editor/reference-guides/packages/packages-i18n/#x
 * Retrieve the translation of text.
 */
import { __ } from "@wordpress/i18n";

/**
 * https://developer.wordpress.org/block-editor/reference-guides/packages/packages-element/#fragment
 * A component which renders its children without any wrapping element.
 */
import { Fragment, RawHTML } from "@wordpress/element";

/**
 * https://github.com/WordPress/gutenberg/blob/HEAD/packages/block-editor/src/components/media-placeholder/README.md
 * MediaPlaceholder is a React component used to render either the media associated with a block, or an editing interface to replace the media for a block.
 */
import { InnerBlocks, MediaPlaceholder, useBlockProps } from "@wordpress/block-editor";

/**
 * withDispatch -   https://developer.wordpress.org/block-editor/reference-guides/packages/packages-data/#withdispatch
 * Higher-order component used to add dispatch props using registered action creators.
 * withSelect   -     https://developer.wordpress.org/block-editor/reference-guides/packages/packages-data/#withselect
 * Higher-order component used to inject state-derived props using registered selectors.
 */
import { withSelect, withDispatch } from "@wordpress/data";

/**
 * https://developer.wordpress.org/block-editor/reference-guides/packages/packages-compose/#compose
 * Composes multiple higher-order components into a single higher-order component. Performs right-to-left function composition, where each successive invocation is supplied the return value of the previous.
 */
import { compose } from "@wordpress/compose";

/**
 * https://developer.wordpress.org/block-editor/reference-guides/packages/packages-blocks/#createblock
 * Returns a block object given its type and attributes.
 */
import { createBlock } from "@wordpress/blocks";

/**
 * Custom Inspector Controls
 */
import AdvancedSliderInspectorControls from "./inspector";
import AdvancedSliderBlockControls from "./block-controls";

/**
 * Handles the preview mode in the editor
 */
import PreviewSlides from "./preview";

import {
  BlockControls,
  MediaUpload,
  MediaUploadCheck
} from "@wordpress/block-editor";

import { Placeholder, Toolbar, Button } from "@wordpress/components";


import "./style.editor.scss";

const Edit = props => {
  /**
   * @attributes - https://developer.wordpress.org/block-editor/reference-guides/block-api/block-edit-save/#attributes
   * @setAttributes - https://developer.wordpress.org/block-editor/reference-guides/block-api/block-edit-save/#setattributes
   * @hasInnerBlocks - Custom Prop added to props via withSelect, value is boolean based on whether the block has sub-blocks
   * @isSelected - A prop that reflects whether the block is currently selected in the editor
   * @isChildSelected - A prop sent from withSelect that reflects whether any child blocks of the current block are selected.
   *
   */
  const {
    attributes,
    setAttributes,
    hasInnerBlocks,
    isSelected,
    isChildSelected,
    slideBlocks
  } = props;



  const { className, sliderConfig, forcePreviewMode } = attributes;

  const isSelectedOrChildSelected = isSelected || isChildSelected;

  const classes = classnames({
    [className]: !!className,
    "lg-advanced-slider": true,
   // "wp-block-lg-blocks-advanced-slider": true
  });

  const blockProps = useBlockProps(
    { className: classes }
  )



  /**
   * OnImageSelect - invoked from the MediaPlaceholder item in the component's return
   *  @param images - An array of images provided by the MediaPlacehodler component during the OnSelect hook.
   */
  const onImageSelect = images => {
    console.log(images);
    const { createSwiperSlides } = props;
    createSwiperSlides(images);
  };

  const onResponsiveSettingUpdate = (breakpoint, setting, value) => {
    console.log(breakpoint, setting, value);
    switch (breakpoint) {
      case "mobile": {
        let newSliderConfig = { ...sliderConfig };
        newSliderConfig[setting] = value;
        setAttributes({ sliderConfig: newSliderConfig });
        return;
      }
      case "tablet": {
        let newSliderConfig = { ...sliderConfig };
        newSliderConfig.breakpoints["768"][setting] = value;
        setAttributes({ sliderConfig: newSliderConfig });
        return;
      }

      case "desktop": {
        let newSliderConfig = { ...sliderConfig };
        newSliderConfig.breakpoints["1024"][setting] = value;
        setAttributes({ sliderConfig: newSliderConfig });
        return;
      }
      default: {
        console.error("Invalid Setting onResponsiveSettingUpdate");
        throw "Invalid Setting onResponsiveSettingUpdate";
      }
    }
  };

  if (!hasInnerBlocks) {
    return (
      <Fragment>
        <AdvancedSliderInspectorControls
          data={{ attributes, setAttributes, onResponsiveSettingUpdate }}
        />
        <AdvancedSliderBlockControls
          data={{ attributes, setAttributes, onImageSelect }}
        />
        <div className={classes}>
          <Placeholder
            icon={"slides"}
            label={__("Advanced Slider", "lg-blocks")}
            instructions={"Add a block from the inserter to get started!"}
          />
          <InnerBlocks
            allowedBlocks={[
              "lg-blocks/advanced-slide",
              "lg-blocks/video-slide"
            ]}
            template={[]}
            renderAppender={ InnerBlocks.ButtonBlockAppender }
          />
        </div>
      </Fragment>
    );
  } else if (isSelectedOrChildSelected && !forcePreviewMode) {
    return (
      <Fragment>
        <AdvancedSliderInspectorControls
          data={{ attributes, setAttributes, onResponsiveSettingUpdate }}
        />
        <AdvancedSliderBlockControls
          data={{ attributes, setAttributes, onImageSelect }}
        />
        <div { ...blockProps } >
          <InnerBlocks
            allowedBlocks={[
              "lg-blocks/advanced-slide",
              "lg-blocks/video-slide",
              "lg-blocks/text-slide"
            ]}
            template={[]}
          />
        </div>
      </Fragment>
    );
  } else {
    return (
      <Fragment>
        <AdvancedSliderInspectorControls
          data={{ attributes, setAttributes, onResponsiveSettingUpdate }}
        />
        <PreviewSlides data={{ ...props, classes, ...attributes }} />
        <div className="d-none">
          <InnerBlocks
            allowedBlocks={[
              "lg-blocks/advanced-slide",
              "lg-blocks/video-slide"
            ]}
            template={[]}
          />
        </div>
      </Fragment>
    );
  }
};

export default compose([
  withSelect((select, props) => {
    const { clientId } = props;
    const coreEditor = select("core/block-editor");
    return {
      hasInnerBlocks: coreEditor.getBlocks(clientId).length > 0,
      isChildSelected: coreEditor.hasSelectedInnerBlock(clientId, true),
      slideBlocks: coreEditor.getBlocks(clientId)
    };
  }),
  withDispatch((dispatch, ownProps) => ({
    createSwiperSlides(images) {
      if (images) {
        console.log(images);
        let swiperSlides = [];
        const { clientId, attributes } = ownProps;
        const { hasSlideContent } = attributes;
        images.map(image => {
          let sizeData = Object.values(image.sizes);

          var srcSetArray = [];
          var sizesArray = [];
          sizeData.forEach((element, index) => {
            srcSetArray.push(`${element.url} ${element.width}w`);
            if (index !== sizeData.length) {
              sizesArray.push(
                `(max-width: ${element.width}px) ${element.width}px`
              );
            } else {
              sizesArray.push(`${element.width}px`);
            }
          });

          let srcSet = srcSetArray.join(",");
          let sizes = sizesArray.join(",");

          let swiperSlide = createBlock("lg-blocks/advanced-slide", {
            mediaId: image.id,
            mediaUrl: image.url,
            mediaAlt: image.alt,
            srcSet: srcSet,
            sizes: sizes,
            hasSlideContent: hasSlideContent
          });
          swiperSlides = [swiperSlide, ...swiperSlides];
        });
        dispatch("core/block-editor").replaceInnerBlocks(
          clientId,
          swiperSlides
        );
      }
    }
  }))
])(Edit);
