/**
 * https://www.npmjs.com/package/swiper
 * https://swiperjs.com/react
 * Swiper - is the free and most modern mobile touch slider with hardware accelerated transitions and amazing native behavior
 */
import { Swiper } from "swiper/react";
import { SwiperSlide } from "swiper/react";
// import { Navigation } from "swiper";
import { Pagination, Navigation } from "swiper/modules";
import Player from "@vimeo/player";
import YouTubePlayer from "youtube-player";


/**
 * https://developer.wordpress.org/block-editor/reference-guides/packages/packages-element/#fragment
 * A component which renders its children without any wrapping element.
 */
import { Fragment, RawHTML, useState, useEffect } from "@wordpress/element";
import { useBlockProps } from "@wordpress/block-editor";

const PreviewSlides = props => {
  const [thumbsSwiper, setThumbsSwiper] = useState(null);
  const blockProps = useBlockProps();

  const { slideBlocks, classes, hasThumbs, attributes } = props.data;
  const {
    breakpoint,
    sliderConfig,
    hasArrows,
    hasDots,
    forcePreviewMode
  } = attributes;

  blockProps.className = blockProps.className
    .split(" ")
    .filter(el => el.includes("has"))
    .join(" ");

  blockProps.className = `${blockProps.className} ${classes}`;

  const createInnerBlocks = slide => {
    return slide.innerBlocks.map((innerBlock, index) => {
      if (innerBlock.innerBlocks.length === 0) {
        return <RawHTML key={index}> {innerBlock.originalContent} </RawHTML>;
      }
      if (innerBlock.name === "core/group") {
        const GroupTag = innerBlock.attributes.tagName;
        return (
          <GroupTag className={innerBlock.attributes.className} key={index}>
            {createInnerBlocks(innerBlock)}
          </GroupTag>
        );
      }
      if (innerBlock.name === "core/buttons") {
        return (
          <div className="wp-block-buttons" key={index}>
            {createInnerBlocks(innerBlock)}
          </div>
        );
      }
      return;
    });
  };

  const createSlides = () => {
    return slideBlocks.map((slide, index) => {

      if (slide.name === "lg-blocks/video-slide") {
        return (
          <SwiperSlide key={index}>
            <div className="wp-block-lg-blocks-advanced-slide video-slide">
              <div className="preview-swiper-slide__video" data-provider={slide.attributes.videoProvider} data-videoId={slide.attributes.videoId} id={`preview-${slide.attributes.videoId}`}>
              </div>
              <div className="swiper-slide__content">
                {createInnerBlocks(slide)}
              </div>
            </div>
          </SwiperSlide>
        )
      }

      if (slide.name === 'lg-blocks/text-slide') {
        return(
          <SwiperSlide key={index}>
            <div className="wp-block-lg-blocks-advanced-slide text-slide">
            <div className="swiper-slide__content">
                {createInnerBlocks(slide)}
              </div>
            </div>
          </SwiperSlide>
        )
      }
 
      return (
        <SwiperSlide key={index}>
          <div className="wp-block-lg-blocks-advanced-slide swiper-slide">
            <div className="swiper-slide__image">
              <img
                src={slide.attributes.mediaUrl}
                alt=""
                className=""
                srcSet={slide.attributes.srcSet}
                sizes={slide.attributes.sizes}
              />
            </div>
            <div className="swiper-slide__content">
              {createInnerBlocks(slide)}
            </div>
          </div>
        </SwiperSlide>
      );
    });
  };

  const createThumbsGallery = () => {
    return slideBlocks.map((slide, index) => {
      return <RawHTML key={index}>{slide.originalContent}</RawHTML>;
    });
  };

  useEffect(() => {

    const asyncVideoHandling = async () => {

      const videoPlayers = document.querySelectorAll(".preview-swiper-slide__video");

      // await Promise.all(
      //   videoPlayers.forEach(async (playerElement, index) => {


      //     if (!playerElement.classList.contains("mounted")) {
      //       switch (playerElement.dataset.provider) {
      //         case "vimeo": {
      //           let player = new Player(playerElement.id, {
      //             id: playerElement.id,
      //             //responsive: true,
      //             title: false,
      //             controls: true,
      //             loop: true,
      //             width: screen.width
      //             //height: screen.availHeight
      //           });
      //           player.play();
      //           playerElement.classList = `${playerElement.classList} mounted`
      //           break;
      //         }
      //         case "youtube": {
      //           let player;
      //           player = YouTubePlayer(playerElement.id, {
      //             videoId: playerElement.dataset.videoId,
      //             origin: window.location.origin
      //           });

      //           player.loadVideoById(playerElement.dataset.videoid);
      //           player.playVideo();
      //           window.player = player;
      //           console.log(player);
      //           playerElement.classList = `${playerElement.classList} mounted`


      //           player.on('stateChange', (event) => {
      //             console.log(event);
      //           });

      //           break;



      //         }

      //       }
      //     }
      //   }));

      videoPlayers.forEach(async (playerElement, index) => {


        if (!playerElement.classList.contains("mounted")) {
          switch (playerElement.dataset.provider) {
            case "vimeo": {
              let player = new Player(playerElement.id, {
                id: playerElement.id,
                //responsive: true,
                title: false,
                controls: true,
                loop: true,
                width: screen.width
                //height: screen.availHeight
              });
              player.play();
              playerElement.classList = `${playerElement.classList} mounted`
              break;
            }
            case "youtube": {
              let player;
              player = YouTubePlayer(playerElement.id, {
                videoId: playerElement.dataset.videoId,
                playerVars: {
                  origin: window.location.origin,
                  controls: 0,
                  disablekb: 1,
                  modestbranding: 1,
                  loop: 1
                }
                
              });

              player.loadVideoById(playerElement.dataset.videoid);
              player.mute();
              player.playVideo();
              player.setLoop(true);

              window.player = player;
              console.log(player);
              playerElement.classList = `${playerElement.classList} mounted`


              player.on("stateChange", (ev) => {
                if (ev.data === 0) {
                  player.seekTo(0, true);
                  player.playVideo();
                }
              })

              break;

            }

          }
        }
      })

    }

    asyncVideoHandling();

  });




  return (
    <Fragment>
      <div className={blockProps.className}>
        <div className="clickme">
          Select the block in the list view to edit contents.{" "}
          {forcePreviewMode && (
            <span> - Force Preview Mode Enabled in Advanced Settings </span>
          )}
        </div>
        <Swiper
          spaceBetween={0}
          slidesPerView={
            breakpoint === "mobile"
              ? sliderConfig.slidesPerView // mobile
              : breakpoint === "tablet"
                ? sliderConfig.breakpoints["768"].slidesPerView //tablet
                : sliderConfig.breakpoints["1024"].slidesPerView // desktop
          }
          onSwiper={swiper => console.log(swiper)}
          className="lg-advanced-slider__preview"
          modules={[Navigation, Pagination]}
          navigation={hasArrows}
          pagination={hasDots}
        >
          {createSlides()}
        </Swiper>
        {hasThumbs && (
          <div
            className="d-none d-md-none d-lg-flex thumb-gallery"
            onClick={e => e.preventDefault()}
          >
            {createThumbsGallery()}
          </div>
        )}
      </div>
    </Fragment>
  );
};

export default PreviewSlides;
