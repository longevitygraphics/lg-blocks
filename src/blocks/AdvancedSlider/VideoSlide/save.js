/**
 * External Dependencies
 */
import classnames from "classnames";

/**
 * Wordpress Dependencies
 */
import { InnerBlocks } from "@wordpress/block-editor";

const classes = classnames({
  "lg-base": true
});

function Save(props) {
  const { attributes } = props;
  const { videoId, hasSlideContent, mobileVideoId, videoProvider } = attributes;

  switch (videoProvider) {
    case "vimeo": {
      return (
        <div className="swiper-slide" style={{ height: "100vh" }}>
          <div className="swiper-slide__video" style={{ height: "100vh" }}>
            <div
              id={videoId}
              data-mobileId={mobileVideoId}
              className="lg-vimeo-mount-container"
              style={{ height: "100vh" }}
            ></div>
          </div>

          <div className="swiper-slide__content">
            {hasSlideContent && <InnerBlocks.Content />}
          </div>
        </div>
      );
    }
    case "youtube": {
      return (
        <div className="swiper-slide">
          <div className="swiper-slide__video">
            <div
              id={videoId}
              data-mobileId={mobileVideoId}
              className="lg-youtube-mount-container"
            >
            </div>
          </div>
          <div className="swiper-slide__content">
            {hasSlideContent && <InnerBlocks.Content />}
          </div>

        </div>
      )
    }
  }




}

export default Save;
