import { InspectorControls } from "@wordpress/block-editor";
import {
  Button,
  ButtonGroup,
  PanelBody,
  TextControl,
  ToggleControl
} from "@wordpress/components";
import { __ } from "@wordpress/i18n";
import { useEffect } from "@wordpress/element";

const VSInspectorControls = props => {
  const { attributes, setAttributes } = props;

  const {
    hasVideoBreakpoints,
    videoUrl,
    mobileVideoUrl,
    mobileVideoProvider,
    mobileVideoId
  } = attributes;

  const determineVideoProvider = url => {
    let data = new URL(url);
    switch (data.hostname) {
      case "vimeo.com":
      case "player.vimeo.com":
        data.provider = "vimeo";
        data.id = data.pathname.split("/").pop();
        console.log(data.id);
        break;
        case "www.youtube.com":
        case "youtu.be":
            data.provider = "youtube";
            data.id = data.pathname.split("/").pop();
            break;
      default:
        data.provider = "unknown";
        break;
    }
    return data;
  };

  const onChangeMobileVideo = url => {
    setAttributes({ mobileVideoUrl: url });
    const videoDetails = determineVideoProvider(url);
    console.log(videoDetails);
    setAttributes({ mobileVideoProvider: videoDetails.provider });
    setAttributes({ mobileVideoId: videoDetails.id });
  };

  useEffect(() => {
    if (mobileVideoUrl && !mobileVideoId) {
      const videoDetails = determineVideoProvider(mobileVideoUrl);
      setAttributes({ mobileVideoProvider: videoDetails.provider });
      setAttributes({ mobileVideoId: videoDetails.id });
    }
  });

  return (
    <InspectorControls>
      <PanelBody title={__("Slide Configuration", "lg-blocks")}>
        <ToggleControl
          label="Has Video Sources per Breakpoint?"
          checked={hasVideoBreakpoints}
          onChange={value => {
            setAttributes({ hasVideoBreakpoints: value });
          }}
        />
        <TextControl
          label="Desktop Video Source"
          value={videoUrl}
          onChange={value => {
            setAttributes({ videoUrl: value });
          }}
        />
        <TextControl
          label="Mobile Video Source"
          value={mobileVideoUrl}
          onChange={value => {
            onChangeMobileVideo(value);
          }}
        />
        <span>
          {mobileVideoId},{mobileVideoProvider}
        </span>
      </PanelBody>
    </InspectorControls>
  );
};

export default VSInspectorControls;
