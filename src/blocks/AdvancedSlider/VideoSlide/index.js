/**
 * Wordpress Dependencies
 */
import { registerBlockType } from "@wordpress/blocks";
import { __ } from "@wordpress/i18n";

/**
 * Internal Dependencies
 */


import Edit from "./edit";
import Save from "./save";
import metadata from './block.json';

registerBlockType(metadata.name, {
  edit: Edit,
  save: Save
});
