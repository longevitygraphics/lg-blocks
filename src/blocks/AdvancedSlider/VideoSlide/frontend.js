import Player from "@vimeo/player";
import YouTubePlayer from "youtube-player";
import jQuery from "jquery";
import { debounce } from "lodash";



jQuery(document).ready(function () {

  console.log("Video Slide Front Script Loaded");

  const vimeoPlayers = document.querySelectorAll(".lg-vimeo-mount-container");
  vimeoPlayers.forEach((vimeoPlayer, index) => {
    let options = {
      //id: vimeoPlayer.id,
      responsive: false,
      title: false,
      controls: false,
      loop: true,
      background: true
    };

    if (screen.width < 768) {
      options.height = screen.availHeight;
      options.id = vimeoPlayer.dataset.mobileid;
    } else {
      options.width = screen.availWidth;
      options.id = vimeoPlayer.id;
    }

    console.log(options);

    window.VimeoPlayerLibrary = Player;

    let player = new Player(vimeoPlayer.id, options);
    //player.api("setVolume", 0);
    player.getVolume().then(volume => {
      console.log(volume);
    });
    player.play();

    player.ref = vimeoPlayer;

    window.lgvimeoplayers = [];
    window.lgvimeoplayers.push(player);

    window.addEventListener(
      "touchend",
      () => {
        player.play();
        console.log("playback start");
      },
      { once: true }
    );

    window.addEventListener(
      "click",
      () => {
        player.play();
        console.log("playback start");
      },
      { once: true }
    );

    window.addEventListener(
      "mousemove",
      () => {
        player.play();
        console.log("playback start");
      },
      { once: true }
    );

    window.addEventListener(
      "touchmove",
      () => {
        player.play();
        console.log("playback start");
      },
      { once: true }
    );
  });

  const youtubePlayers = document.querySelectorAll(".lg-youtube-mount-container");
  console.log(youtubePlayers);
  youtubePlayers.forEach((youtubePlayerEl, index) => {
    let player;
    // player = YouTubePlayer(videoId);

    let videoId = screen.availWidth >= 768 ? youtubePlayerEl.id : youtubePlayerEl.dataset.mobileid;

    if (window.lgyoutube === undefined) {
      window.lgyoutube = {};
    }
    if (window.lgyoutube[index] === undefined ) {
      window.lgyoutube[index] = {};
    } 
    window.lgyoutube[index].id = videoId;

    let options = {
      videoId: videoId,
      playerVars: {
        origin: window.location.origin,
        controls: 0,
        disablekb: 1,
        modestbranding: 1,
        loop: 1
      }
    }

    player = YouTubePlayer(youtubePlayerEl, options);
    window.lgyoutube[index].player = player;

    player.loadVideoById(videoId);
    player.mute();
    player.playVideo();
    player.setLoop(true);
    player.on("stateChange", (ev) => {
      if (ev.data === 0) {
        player.seekTo(0, true);
        player.playVideo();
      }
    });
  })
});

jQuery(window).on("resize", () => {
  debounce(handleResize, 500, { trailing: true })();
});

const handleResize = () => {
  console.log("Resized!");
  const youtubePlayers = document.querySelectorAll(".lg-youtube-mount-container");
  youtubePlayers.forEach((youtubePlayerEl, index) => {
    let videoId = screen.availWidth >= 768 ? youtubePlayerEl.id : youtubePlayerEl.dataset.mobileid;
    if (videoId !== window.lgyoutube[index].id) {
      console.log("Should switch video source!")
      window.lgyoutube[index].id = videoId;
      window.lgyoutube[index].player.loadVideoById(videoId);
      window.lgyoutube[index].player.playVideo();
    }
  })

}

