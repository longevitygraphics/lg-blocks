/**
 * External Dependencies.
 */
import classnames from "classnames";
import Player from "@vimeo/player";
import YouTubePlayer from "youtube-player";

/**
 * Wordpress Dependencies.
 */
import { InnerBlocks, MediaPlaceholder } from "@wordpress/block-editor";
import { __ } from "@wordpress/i18n";
import { Spinner, Button } from "@wordpress/components";
import { use, withSelect } from "@wordpress/data";
import { Icon, video, edit } from "@wordpress/icons";
import { useEffect } from "@wordpress/element";
import VSBlockControls from "./block-controls";
import VSInspectorControls from "./inspector-controls";

import "./style.editor.scss";
/**
 *
 */

const ALLOWED_BLOCKS = [
  "core/paragraph",
  "core/heading",
  "core/buttons",
  "core/image",
  "core/list",
  "core/group",
  "lg-blocks/breadcrumb"
];

const TEMPLATE = [["core/heading"]];

const Edit = props => {
  const {
    attributes,
    setAttributes,
    className,
    hasSlideContent,
    isSelected,
    isChildSelected
  } = props;

  const { videoUrl, videoProvider, videoId } = attributes;

  console.log(videoId);

  setAttributes({ hasSlideContent: props.hasSlideContent });

  const classes = classnames({
    [className]: !!className,
    active: isSelected || isChildSelected
  });

  const onVideoUrlSubmit = event => {
    event.preventDefault();
    let video = event.target[0].value;
    console.log(video);
    setAttributes({ videoUrl: video });
    console.log(videoUrl);
    const videoDetails = determineVideoProvider(video);
    console.log(videoDetails);
    setAttributes({ videoProvider: videoDetails.provider });
    setAttributes({ videoId: videoDetails.id });
  };

  const determineVideoProvider = url => {
    let data = new URL(url);
    console.log(data.hostname);
    switch (data.hostname) {
      case "player.vimeo.com":
      case "vimeo.com":
        data.provider = "vimeo";
        data.id = data.pathname.split("/").pop();
        break;
      case "www.youtube.com":
      case "youtu.be":
        data.provider = "youtube";
        data.id = data.pathname.split("/").pop();
        break;
      default:
        data.provider = "unknown";
        break;
    }
    return data;
  };

  useEffect(() => {
    if (videoProvider === "vimeo" && videoId) {
      const player = new Player(videoId, {
        id: videoId,
        responsive: "true",
        title: false
      });
      player.play();
    }
    if (videoProvider === "youtube" && videoId) {
      let player;
      // player = YouTubePlayer(videoId);
      player = YouTubePlayer(videoId, {
        videoId: videoId,
        playerVars: {
          origin: window.location.origin,
          controls: 0,
          disablekb: 1,
          modestbranding: 1,
          loop: 1
        }
      });

      player.loadVideoById(videoId);
      player.mute();
      player.playVideo();
      player.setLoop(true);
      player.on("stateChange", (ev) => {
        console.log(ev);
        if (ev.data === 0) {
          player.seekTo(0, true);
          player.playVideo();
        }
      })
    }
  });

  console.log(props.attributes);

  if (!videoUrl || videoProvider === "unknown") {
    return (
      <>
        <VSInspectorControls
          attributes={attributes}
          setAttributes={setAttributes}
        />
        <div className={classes}>
          <div className="video-input">
            <div className="components-placeholder is-large">
              <div className="components-placeholder__label">
                <span className="block-editor-block-icon">
                  <Icon icon={video} size={24} />
                </span>
                Video Slide
              </div>
              <div className="components-placeholder__instructions">
                {__("Enter the video source URL", "lg-blocks")}
              </div>
              <div className="components-placeholder__fieldset">
                <form onSubmit={onVideoUrlSubmit}>
                  <input
                    type="url"
                    className="components-placeholder__input"
                    aria-label={__("Video Source URL", "lg-blocks")}
                    placeholder={__("Enter Video URL Source here…")}
                  />
                  <Button variant="primary" type="submit">
                    {__("Submit", "lg-blocks")}
                  </Button>
                </form>
                {videoUrl}
              </div>
            </div>
          </div>
        </div>
      </>
    );
  } else {
    return (
      <>
        <VSInspectorControls
          attributes={attributes}
          setAttributes={setAttributes}
        />
        <VSBlockControls setAttributes={setAttributes} />
        <div className={classes}>
          <div className="wp-block-lg-blocks-advanced-slide__video video-slide">
            {<div id={videoId}></div>}
          </div>
          {hasSlideContent && (
            <div className="wp-block-lg-blocks-advanced-slide__content">
              <InnerBlocks allowedBlocks={ALLOWED_BLOCKS} template={TEMPLATE} />
            </div>
          )}
        </div>
      </>
    );
  }
};

export default withSelect((select, props) => {
  const { clientId } = props;
  const coreEditor = select("core/block-editor");
  const parentBlock = coreEditor.getBlockParentsByBlockName(
    clientId,
    "lg-blocks/advanced-slider"
  );
  const isChildSelected = coreEditor.hasSelectedInnerBlock(clientId, true);

  let hasSlideContent = false;
  if (parentBlock.length > 0 && parentBlock[0]) {
    const parentBlockId = parentBlock[0];
    const parentBlockAttributes = coreEditor.getBlockAttributes(parentBlockId);
    hasSlideContent = parentBlockAttributes.hasSlideContent;
  }

  return { hasSlideContent: hasSlideContent, isChildSelected: isChildSelected };
})(Edit);
