/**
 * classnames - https://www.npmjs.com/package/classnames
 * A simple JavaScript utility for conditionally joining classNames together.
 */
import classnames from "classnames";

import { InnerBlocks, useBlockProps } from "@wordpress/block-editor";
import { RawHTML } from "@wordpress/element";
import { isArray } from "lodash";

const Save = props => {
  const { attributes, innerBlocks, className } = props;
  const {
    hasThumbs,
    hasArrows,
    hasDots,
    sliderConfig,
    thumbsConfig,
    hasDesktopThumbGallery
  } = attributes;

  const classes = classnames({
    [className]: !!className,
    "lg-advanced-slider": true,
    "thumb-gallery": hasThumbs,
    "desktop-thumb-gallery": hasDesktopThumbGallery,
    "wp-block-lg-blocks-advanced-slider": true
  });

  const blockProps = useBlockProps.save(
    { className: classes }
  );


  const thumbClasses = classnames({
    thumb__wrapper: true,
    "swiper-slide": true,
    "no-swiper-slide-style": hasDesktopThumbGallery
  });

  const thumbWrapperClasses = classnames({
    "swiper-wrapper": true,
    "no-swiper-slide-style": hasDesktopThumbGallery
  });

  const renderThumbs = () => {
    console.log(props);
    if (!isArray(innerBlocks)) {
      return;
    }
    return innerBlocks.map((block, index) => {
      console.log(block);
      return (
        <div key={index} className={thumbClasses}>
          <img
            src={block.attributes.mediaUrl}
            alt={block.attributes?.mediaAlt}
            className="thumb__img"
          />
        </div>
      );
    });
  };

  return (
    <div {...blockProps}>
      <div className="main swiper" data-swiper={JSON.stringify(sliderConfig)}>
        {hasArrows && (
          <>
            <div className="swiper-button-prev"></div>
            <div className="swiper-button-next"></div>
          </>
        )}
        {hasDots && <div className="swiper-pagination"></div>}
        <div className="swiper-wrapper">
          <InnerBlocks.Content />
        </div>
      </div>
      {hasThumbs && (
        <div
          className="thumb swiper"
          data-swiper={JSON.stringify(thumbsConfig)}
        >
          <div className={thumbWrapperClasses}>{renderThumbs()}</div>
        </div>
      )}
    </div>
  );
};

export default Save;
