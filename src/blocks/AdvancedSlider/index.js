/**
 * Internal dependencies
 */

import Edit from "./edit";
import Save from "./save.js";
import metadata from './block.json';


import './style.scss';


/**
 * Wordpress dependencies
 */
import { registerBlockType } from "@wordpress/blocks";
import { __ } from "@wordpress/i18n";

registerBlockType( metadata.name, {
  edit: Edit,
  save: Save
});
