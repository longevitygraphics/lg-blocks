/**
 * External Dependencies.
 */
import classnames from "classnames";

/**
 * Wordpress Dependencies.
 */
import { InnerBlocks, MediaPlaceholder, useBlockProps } from "@wordpress/block-editor";
import { createBlock } from "@wordpress/blocks";
import { __ } from "@wordpress/i18n";
import { isBlobURL } from "@wordpress/blob";
import { Spinner } from "@wordpress/components";
import { withSelect, useDispatch } from "@wordpress/data";

import ASBlockControls from "./block-controls";


/**
 *
 */
import "./style.editor.scss";

const ALLOWED_BLOCKS = [
  "core/paragraph",
  "core/heading",
  "core/buttons",
  "core/image",
  "core/list",
  "core/group",
  "lg-blocks/breadcrumb"
];

const TEMPLATE = [["core/heading"]];



const Edit = props => {
  const {
    attributes,
    setAttributes,
    className,
    hasSlideContent,
    isSelected,
    isChildSelected,
    parentBlockId
  } = props;

  const { mediaUrl, mediaId, mediaAlt } = attributes;
  const { insertBlock } = useDispatch('core/block-editor');

  setAttributes({ hasSlideContent: props.hasSlideContent });

  const onSelectImage = images => {
    console.log(images);

    images.map((image, index) => {
      const { id, alt, url } = image;

      let sizeData = Object.values(image.sizes);

      var srcSetArray = [];
      var sizesArray = [];
      sizeData.forEach((element, index) => {
        srcSetArray.push(`${element.url} ${element.width}w`);
        if (index !== sizeData.length) {
          sizesArray.push(`(max-width: ${element.width}px) ${element.width}px`);
        } else {
          sizesArray.push(`${element.width}px`);
        }
      });

      let srcSet = srcSetArray.join(",");
      let sizes = sizesArray.join(",");
      images[index].data = { id, alt, url, srcSet, sizes };

    })


    setAttributes({
      mediaId: images[0].data.id,
      mediaAlt: images[0].data.alt,
      mediaUrl: images[0].data.url,
      srcSet: images[0].data.srcSet,
      sizes: images[0].data.sizes
    });

    for (let index = 1; index < images.length; index++) {
      images[index].block = createBlock("lg-blocks/advanced-slide", { mediaId: images[index].data.id, mediaAlt: images[index].data.alt, mediaUrl: images[index].data.url, srcSet: images[index].data.srcSet, sizes: images[index].data.sizes });
      insertBlock(images[index].block, undefined, parentBlockId);
    }



  };

  const onUploadError = () => {
    console.error("Image Upload Failed, unknown reason.");
  };

  const classes = classnames({
    [className]: !!className,
    active: isSelected || isChildSelected
  });

  const blockProps = useBlockProps(
    { className: classes }
  )

  return (
    <div {...blockProps} >
      <div className="wp-block-lg-blocks-advanced-slide__image">
        {mediaUrl ? (
          <>
            <ASBlockControls onSelectImage={onSelectImage} mediaId={mediaId} />
            <img
              src={mediaUrl}
              alt={mediaAlt}
              className={mediaId ? `wp-image-${mediaId}` : null}
            />
            {isBlobURL(mediaUrl) && <Spinner />}
          </>
        ) : (
          <MediaPlaceholder
            icon="format-image"
            multiple={true}
            onSelect={onSelectImage}
            onError={onUploadError}
            allowedTypes={["image"]}
            labels={{
              title: __("Select slider image", "lg-blocks"),
              instructions: __(
                "Upload or select image from the media library.",
                "lg-blocks"
              )
            }}
          />
        )}
      </div>
      {mediaUrl && hasSlideContent && (
        <div className="wp-block-lg-blocks-advanced-slide__content">
          <InnerBlocks allowedBlocks={ALLOWED_BLOCKS} template={TEMPLATE} />
        </div>
      )}
    </div>
  );
};

export default withSelect((select, props) => {
  const { clientId } = props;
  const coreEditor = select("core/block-editor");
  const parentBlock = coreEditor.getBlockParentsByBlockName(
    clientId,
    "lg-blocks/advanced-slider"
  );
  const isChildSelected = coreEditor.hasSelectedInnerBlock(clientId, true);

  let hasSlideContent = false;
  if (parentBlock.length > 0 && parentBlock[0]) {
    const parentBlockId = parentBlock[0];
    const parentBlockAttributes = coreEditor.getBlockAttributes(parentBlockId);
    hasSlideContent = parentBlockAttributes.hasSlideContent;
  }

  return { hasSlideContent: hasSlideContent, isChildSelected: isChildSelected, parentBlockId: parentBlock[0] };
})(Edit);
