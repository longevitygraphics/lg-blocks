/**
 * External Dependencies
 */
import classnames from "classnames";

/**
 * Wordpress Dependencies
 */
import { InnerBlocks } from "@wordpress/block-editor";


function Save(props) {
  const { attributes } = props;
  const {
    mediaId,
    mediaUrl,
    mediaAlt,
    hasSlideContent,
    srcSet,
    sizes
  } = attributes;
  return (
    <div className="swiper-slide">
      <div className="swiper-slide__image">
        <img
          src={mediaUrl}
          alt={mediaAlt}
          className={mediaId ? `wp-image-${mediaId}` : null}
          srcSet={srcSet}
          sizes={sizes}
        />
      </div>
      <div className="swiper-slide__content">
        {hasSlideContent && <InnerBlocks.Content />}
      </div>
    </div>
  );
}

export default Save;
