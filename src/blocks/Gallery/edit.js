import { useBlockProps } from "@wordpress/block-editor";
import { InnerBlocks } from "@wordpress/block-editor";

import "./style.editor.scss";




const Edit = () => {

  const blockProps = useBlockProps();


  return (
    <div {...blockProps}>
      <InnerBlocks 
        allowedBlocks={["core/gallery"]}
        template={["core/gallery"]}
      />
    </div>
  );
}


export default Edit;
