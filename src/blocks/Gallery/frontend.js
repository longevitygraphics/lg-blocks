console.log("Lightbox Gallery Script Loaded!");

import PhotoSwipeLightbox from "photoswipe/lightbox";
import PhotoSwipe from "photoswipe";

import "photoswipe/dist/photoswipe.css";

document.addEventListener('DOMContentLoaded', () => {
  let gallery = document.querySelectorAll(".wp-block-gallery");
  console.log(gallery);


  const lightbox = new PhotoSwipeLightbox({
    gallery: ".wp-block-gallery",
    children: 'img',

    pswpModule: () => PhotoSwipe
  });

  lightbox.addFilter('domItemData', (itemData, element, linkEl) => {

    if (element) {
      itemData.src = element.src;
      itemData.w = element.naturalWidth;
      itemData.h = element.naturalHeight;
      itemData.thumbCropped = true;
    }

    return itemData;
  });

  lightbox.init();

  window.lightbox = lightbox;
})
  





