import { CardDivider } from "@wordpress/components";

const save = props => {
  const { attributes } = props;
  const { formAction, botInputName, submitText, inputPlaceholder } = attributes;
  return (
    <div className="lg-mailchimp-block">
      <div id="mc_embed_signup">
        <form
          action={formAction}
          method="post"
          id="mc-embedded-subscribe-form"
          name="mc-embedded-subscribe-form"
          className="validate"
          target="_blank"
          noValidate
        >
          <div id="mc_embed_signup_scroll">
            <label htmlFor="mce-EMAIL">Subscribe</label>
            <input
              type="email"
              value=""
              name="EMAIL"
              className="email"
              id="mce-EMAIL"
              placeholder={inputPlaceholder}
              required
            />

            <div aria-hidden="true">
              <input type="text" name={botInputName} tabIndex="-1" value="" />
            </div>
            <div className="clear">
              <input
                type="submit"
                value={submitText}
                name="subscribe"
                id="mc-embedded-subscribe"
                className="button"
              />
            </div>
          </div>
        </form>
      </div>
    </div>
  );
};

export default save;
