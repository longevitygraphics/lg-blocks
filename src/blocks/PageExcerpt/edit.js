import { useBlockProps, } from "@wordpress/block-editor";
import { withSelect } from "@wordpress/data";

import "./style.editor.scss";

const Edit = ({excerpt}) => {

    const blockProps = useBlockProps();

    return (
        <div {...blockProps }><p>{ excerpt !== undefined ? excerpt.raw : "This is where the page excerpt will go."}</p></div>
    )
}

export default withSelect((select, props) => {
    const editor = select("core/editor");
    const core = select("core");

    let id = editor.getCurrentPostId();

    let pages = core.getEntityRecords('postType', 'page');
    if (!Array.isArray(pages)) {
      return {
        id: undefined,
        excerpt: undefined
      };
    }

    let page = pages.filter( (obj) => { return obj.id === id});

    if (!Array.isArray(page) || page.length === 0) {
      return {
        id: undefined,
        excerpt: undefined
      }
    }

    return {
      id: id,
      excerpt : page[0].excerpt
    };
  })(Edit);