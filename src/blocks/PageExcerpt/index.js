/**
 * Internal dependencies
 */

import Edit from "./edit";
import metadata from './block.json';
import { postExcerpt as icon } from '@wordpress/icons';

/**
 * Wordpress dependencies
 */
import { registerBlockType } from "@wordpress/blocks";
import { __ } from "@wordpress/i18n";

registerBlockType(metadata.name, {
    edit: Edit,
    save() {
        return null;
    },
    icon
});
