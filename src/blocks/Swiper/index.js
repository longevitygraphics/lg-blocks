import "./style.editor.scss";
import { registerBlockType } from "@wordpress/blocks";
import { __ } from "@wordpress/i18n";

import edit from "./edit";
import save from "./save";

import { TEMPLATE_SLIDE } from "./defaults/templateSlide.jsx";
import { TEMPLATE_BREAKPOINTS } from "./defaults/templateSwiperBreakpoints.jsx";
import { TEMPLATE_AUTOPLAY } from "./defaults/templateSlideAutoPlay.jsx";

const attributes = {
  slideData: {
    type: "array",
    source: "children",
    selector: ".swiper-wrapper",
    default: [TEMPLATE_SLIDE] // @object -- Reference ./defaults/templateSlide.jsx
  },
  sliderConfig: {
    type: "object",
    default: {
      slidesPerView: 1,
      loop: true,
      defaultResponsive: true,
      spaceBetween: 15,
      initialSlide: 0,
      centeredSlides: true,
      autoHeight: true,
      doAutoPlay: false,
      hasArrows: true,
      hasDots: true,
      navColor: "##FF0000",
      responsive: TEMPLATE_BREAKPOINTS, // @object  -- Reference ./defaults/templateSwiperBreakpoints.jsx
      autoplay: TEMPLATE_AUTOPLAY // @object -- Reference ./defaults/templateSwiperAutoPlay.jsx,
    }
  }
};

registerBlockType("lg-blocks/swiper", {
  title: __("Swiper", "lg-blocks"),
  description: __("Block for Swiper layouts", "lg-blocks"),
  category: "lg-category",
  keywords: [
    __("Slider", "lg-blocks"),
    __("carousel", "lg-blocks"),
    __("swiper", "lg-blocks")
  ],
  icon: "slides",
  attributes,
  edit,
  save
});
