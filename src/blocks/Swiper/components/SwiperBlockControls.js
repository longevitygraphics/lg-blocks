import { Toolbar, Button, ButtonGroup } from "@wordpress/components";
import { __ } from "@wordpress/i18n";
import {
  BlockControls,
  MediaUploadCheck,
  MediaUpload
} from "@wordpress/block-editor";

const SwiperBlockControls = props => {
  const {
    attributes,
    setAttributes,
    sliderHandle,
    getSizesandSrcSet,
    createSlide,
    initSlider,
    SlideNextOrNotify,
    SlidePrevOrNotify
  } = props;

  const { slideData } = attributes;

  const onSlideDelete = () => {
    //console.log("Attempting to Delete Slide " + sliderHandle.activeIndex);

    const newSlides = [...slideData];

    newSlides.splice(sliderHandle.activeIndex, 1);

    //console.log(newSlides);

    setAttributes({ slideData: newSlides });
  };

  const onSlideUpdate = image => {
    //console.log("Attempting to Update Slide " + sliderHandle.activeIndex);

    let slideProps =
      slideData[sliderHandle.activeIndex].props.children[0].props;

    slideProps.alt = image.alt;
    slideProps["data-id"] = image.id;

    let { sizes, srcset } = getSizesandSrcSet(image);
    let sizesRef = image.sizes;

    slideProps.src = sizesRef.full.url;
    slideProps.srcset = srcset;
    slideProps.sizes = sizes;

    //console.log(slideProps);

    let newSlides = [...slideData];

    newSlides[sliderHandle.activeIndex].props.children[0].props = slideProps;

    setAttributes({ slideData: newSlides });
  };

  return (
    <BlockControls>
      <Toolbar className="swiper-blockcontrols-toolbar">
        <MediaUploadCheck>
          <MediaUpload
            onSelect={image => {
              onSlideUpdate(image);
            }}
            allowedTypes={["image"]}
            //value={id}
            render={({ open }) => {
              return (
                <Button
                  label={__("Edit Image", "lg-blocks")}
                  onClick={open}
                  icon="edit"
                />
              );
            }}
          />
        </MediaUploadCheck>
        <Button
          label={__("Re-Init Slider", "lg-blocks")}
          onClick={initSlider}
          icon="star-filled"
        />
        <ButtonGroup>
          <Button
            label={__("Previous Slide", "lg-blocks")}
            onClick={SlidePrevOrNotify}
            icon="controls-back"
          />
          <Button
            label={__("Next Slide", "lg-blocks")}
            onClick={SlideNextOrNotify}
            icon="controls-forward"
          />
        </ButtonGroup>
        <ButtonGroup>
          <Button
            label={__("Add Slide", "lg-blocks")}
            onClick={createSlide}
            icon="plus"
          />
          <Button
            label={__("Remove Image", "lg-blocks")}
            onClick={onSlideDelete}
            icon="trash"
            isDestructive={true}
          />
        </ButtonGroup>
      </Toolbar>
    </BlockControls>
  );
};

export default SwiperBlockControls;
