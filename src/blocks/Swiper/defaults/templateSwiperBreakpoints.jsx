export const TEMPLATE_BREAKPOINTS = {
  xs: {
    slidesPerView: 1,
    width: 320
  },
  sm: {
    slidesPerView: 1,
    width: 375
  },
  md: {
    slidesPerView: 3,
    width: 768
  },
  lg: {
    slidesPerView: 4,
    width: 1024
  },
  xl: {
    slidesPerView: 6,
    width: 1440
  }
};
