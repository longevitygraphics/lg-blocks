import { Component } from "@wordpress/element";

class Save extends Component {
  render = () => {
    const { attributes } = this.props;

    const { slideData, sliderConfig } = attributes;
    const renderSlides = slideData => {
      return slideData.map((slide, index) => {
        let { src, alt, sizes, srcset, width } = slide.props.children[0].props;
        let dataId = slide.props.children[0].props["data-id"];
        return (
          <div className="swiper-slide" key={index}>
            <img
              src={src}
              alt={alt}
              data-id={dataId}
              sizes={sizes}
              srcSet={srcset}
              width={width}
            />
          </div>
        );
      });
    };

    return (
      <div
        className="swiper-container"
        data-swiper-config={JSON.stringify(sliderConfig)}
      >
        <div className="swiper-wrapper">{renderSlides(slideData)}</div>

        {sliderConfig.hasArrows && (
          <>
            <div
              className="swiper-button-prev"
              style={{ color: sliderConfig.navColor }}
            ></div>
            <div
              className="swiper-button-next  "
              style={{ color: sliderConfig.navColor }}
            ></div>
          </>
        )}
        {sliderConfig.hasDots && <div className="swiper-pagination"></div>}
      </div>
    );
  };
}

export default Save;
