import {Component} from "@wordpress/element";
import classnames from "classnames";
import {isBlobURL} from "@wordpress/blob";
import {IconButton, Spinner, Toolbar} from "@wordpress/components";
import {
  BlockControls,
  ContrastChecker,
  InnerBlocks,
  InspectorControls,
  MediaPlaceholder,
  MediaUpload,
  MediaUploadCheck,
  PanelColorSettings,
  withColors,
  BlockVerticalAlignmentToolbar
} from "@wordpress/block-editor";
import {__} from "@wordpress/i18n";

import "./style.editor.scss";

class Edit extends Component {
  onSelectImage = image => {
    const {id, alt, url} = image;
    this.props.setAttributes({mediaId: id, mediaAlt: alt, mediaUrl: url});
  };
  onSelectURL = mediaUrl => {
    this.props.setAttributes({
      mediaUrl,
      mediaId: null,
      mediaAlt: ""
    });
  };
  removeImage = () => {
    const {setAttributes} = this.props;
    setAttributes({
      mediaUrl: "",
      mediaAlt: "",
      mediaId: null
    });
  };

  onChangeAlt = mediaAlt => {
    this.props.setAttributes({mediaAlt});
  };

  onSelectBackgroundImage = image => {
    const {id, url} = image;
    this.props.setAttributes({
      backgroundImageId: id,
      backgroundImageUrl: url,
      align: "full"
    });
  };

  removeBackgroundImage = () => {
    const {setAttributes} = this.props;
    setAttributes({
      backgroundImageUrl: "",
      backgroundImageId: null,
      align: null
    });
  };

  setBGColor = (color) => {
    const {setBackgroundColor, setAttributes} = this.props;
    setBackgroundColor(color);
    if (color === undefined) {
      setAttributes({align: null});
    } else {
      setAttributes({align: "full"});
    }
  }

  render() {
    const {
      attributes,
      className,
      setAttributes,
      backgroundColor,
      textColor,
      setTextColor
    } = this.props;
    const {
      sameHeight,
      mediaId,
      mediaUrl,
      mediaAlt,
      mediaHorizontalAlignment,
      mediaVerticalAlignment,
      backgroundImageId,
      backgroundImageUrl
    } = attributes;
    const ALLOWED_BLOCKS = [
      "core/paragraph",
      "core/heading",
      "core/button",
      "core/buttons",
      "lg-blocks/fancy-heading",
      "lg-blocks/responsive-image",
      "core/list",
      "gravityforms/form",
      "core/image",
      "core/group"
    ];
    const TEMPLATE = [
      ["core/heading", {content: "Heading goes here..."}],
      [
        "core/paragraph",
        {
          content:
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt"
        }
      ],
      ["core/button", {url: "#", text: "button text"}]
    ];
    const sameHeightToolbarControls = [
      [
        {
          icon: "columns",
          title: __("Same Height", "lg-blocks"),
          isActive: sameHeight,
          onClick: () => setAttributes({sameHeight: !sameHeight})
        }
      ]
    ];
    const alignmentToolbarControls = [
      [
        {
          icon: "align-pull-left",
          title: __("Show image on left", "lg-blocks"),
          isActive: mediaHorizontalAlignment === "left",
          onClick: () => setAttributes({mediaHorizontalAlignment: "left"})
        },
        {
          icon: "align-pull-right",
          title: __("Show image on right", "lg-blocks"),
          isActive: mediaHorizontalAlignment === "right",
          onClick: () => setAttributes({mediaHorizontalAlignment: "right"})
        }
      ]
    ];
    const classes = classnames({
      "img-text": true,
      "img-text--sameHeight": sameHeight
    });

    const innerClasses = classnames({
      "img-text__inner": true,
      [className]: !!className,
      "align-items-start": !sameHeight && mediaVerticalAlignment === "top",
      "align-items-center": !sameHeight && mediaVerticalAlignment === "center",
      "align-items-end": !sameHeight && mediaVerticalAlignment === "bottom"
    });
    const mediaClasses = classnames({
      "img-text__media": true,
      "order-md-12 text-md-right": mediaHorizontalAlignment === "right"
    });
    const contentClasses = classnames({
      "img-text__content": true,
      "justify-content-start": sameHeight && mediaVerticalAlignment === "top",
      "justify-content-center":
        sameHeight && mediaVerticalAlignment === "center",
      "justify-content-end": sameHeight && mediaVerticalAlignment === "bottom"
    });
    const contentStyles = {};
    const styles = {
      color: textColor.color
    };
    if (sameHeight) {
      contentStyles.backgroundColor = backgroundColor.color;
    } else {
      styles.backgroundColor = backgroundColor.color;
    }
    if (backgroundImageUrl) {
      if (sameHeight) {
        contentStyles.backgroundImage = `url(${backgroundImageUrl})`;
      } else {
        styles.backgroundImage = `url(${backgroundImageUrl})`;
      }
    }

    //console.log(this.props);
    return (
      <>
        <InspectorControls>
          <PanelColorSettings
            title={__("Color Settings", "lg-blocks")}
            initialOpen={false}
            colorSettings={[
              {
                value: backgroundColor.color,
                onChange: (val) => this.setBGColor(val),
                label: __("Background Color", "lg-blocks")
              },
              {
                value: textColor.color,
                onChange: setTextColor,
                label: __("Text Color", "lg-blocks")
              }
            ]}
          >
            <ContrastChecker
              textColor={textColor.color}
              backgroundColor={backgroundColor.color}
            />
          </PanelColorSettings>
        </InspectorControls>
        <BlockControls>
          <Toolbar>
            <MediaUploadCheck>
              <MediaUpload
                onSelect={this.onSelectBackgroundImage}
                allowedTypes={["image"]}
                value={backgroundImageId}
                render={({open}) => {
                  return (
                    <IconButton
                      className="components-icon-button components-toolbar__control"
                      label={__("Add/Edit background Image", "lg-blocks")}
                      onClick={open}
                      icon="format-image"
                    />
                  );
                }}
              />
            </MediaUploadCheck>

            {backgroundImageUrl && (
              <IconButton
                className="components-icon-button components-toolbar__control"
                label={__("Remove Background Image", "lg-blocks")}
                onClick={this.removeBackgroundImage}
                icon="trash"
              />
            )}
          </Toolbar>
          <Toolbar controls={sameHeightToolbarControls}/>
          <BlockVerticalAlignmentToolbar
            onChange={alignment =>
              setAttributes({mediaVerticalAlignment: alignment})
            }
            value={mediaVerticalAlignment}
          />
          <Toolbar controls={alignmentToolbarControls}/>
          {mediaUrl && (
            <Toolbar>
              {mediaId && (
                <MediaUploadCheck>
                  <MediaUpload
                    onSelect={this.onSelectImage}
                    allowedTypes={["image"]}
                    value={mediaId}
                    render={({open}) => {
                      return (
                        <IconButton
                          className="components-icon-button components-toolbar__control"
                          label={__("Edit Image", "lg-blocks")}
                          onClick={open}
                          icon="edit"
                        />
                      );
                    }}
                  />
                </MediaUploadCheck>
              )}
              <IconButton
                className="components-icon-button components-toolbar__control"
                label={__("Remove Image", "lg-blocks")}
                onClick={this.removeImage}
                icon="trash"
              />
            </Toolbar>
          )}
        </BlockControls>
        <div className={classes} style={styles}>
          <div className={innerClasses}>
            <div className={mediaClasses}>
              {mediaUrl ? (
                <>
                  <img src={mediaUrl} alt={mediaAlt} className={mediaId ? `wp-image-${mediaId}` : null}/>
                  {isBlobURL(mediaUrl) && <Spinner/>}
                </>
              ) : (
                <MediaPlaceholder
                  icon="format-image"
                  onSelect={this.onSelectImage}
                  onSelectURL={this.onSelectURL}
                  onError={this.onUploadError}
                  //accept="image/*"
                  allowedTypes={["image"]}
                  labels={{
                    title: "Team Member Image",
                    instructions: "upload member portrait"
                  }}
                  //notices={noticeUI}
                />
              )}
            </div>
            <div className={contentClasses} style={contentStyles}>
              <InnerBlocks allowedBlocks={ALLOWED_BLOCKS} template={TEMPLATE}/>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default withColors("backgroundColor", {textColor: "color"})(Edit);
