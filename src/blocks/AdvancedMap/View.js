/**
 * External dependencies
 */
import { Component, useEffect, useRef, useState } from "react";
import useSuperCluster from "use-supercluster";

import classnames from "classnames";
import GoogleMapReact from "google-map-react";

import Marker from "./components/FrontEndMarker";
import ClusterMarker from "./components/ClusterMarker";

const classes = classnames({});
const View = props => {
  const {
    Markers,
    MarkerAssets,
    Key,
    Center,
    Zoom,
    MapStyle,
    hasAdditionalMarkerIcons,
    AdditionalMarkerIcons
  } = props;

  const [activeMarker, setActiveMarker] = useState(undefined);
  const [bounds, setBounds] = useState(null);
  const [currentZoom, setZoom] = useState(Zoom);

  console.log("Maps View Script Loaded");

  const points = Markers.map((marker, index) => {
    return {
      type: "Feature",
      properties: {
        cluster: false,
        markerId: index,
        name: marker.name,
        address: marker.address,
        tel: marker.tel,
        email: marker.email,
        website: marker.website,
        services: marker.serviceTypes
      },
      geometry: {
        type: "Point",
        coordinates: [parseFloat(marker.lng), parseFloat(marker.lat)]
      }
    };
  });

  const clusterData = {
    points,
    bounds,
    zoom: currentZoom,
    options: { radius: 75, maxZoom: 20 }
  };

  const { clusters, supercluster } = useSuperCluster(clusterData);

  const changeActiveMarker = selected => {
    setActiveMarker(selected);
  };

  const style = "undefined" === typeof MapStyle ? [] : JSON.parse(MapStyle);

  const mapRef = useRef();

  const setStyle = () => {
    if (mapRef.current) {
      mapRef.current.setOptions({ styles: style });
    }
  };

  useEffect(() => {
    setStyle();
  });

  const renderMarkers = () => {
    return Markers.map((marker, index) => {
      return (
        <Marker
          key={index}
          markerId={index}
          lat={marker.lat}
          lng={marker.lng}
          name={marker.name}
          address={marker.address}
          tel={marker.tel}
          email={marker.email}
          website={marker.website}
          services={marker.serviceTypes}
          hasAdditionalMarkerIcons={hasAdditionalMarkerIcons}
          AdditionalMarkerIcons={AdditionalMarkerIcons}
          icon={MarkerAssets.mediaUrl}
          activeMarker={activeMarker}
          setActiveMarker={value => {
            changeActiveMarker(value);
          }}
        />
      );
    });
  };

  const renderClusters = () => {
    return clusters.map((cluster, index) => {
      const [longitude, latitude] = cluster.geometry.coordinates;
      const {
        cluster: isCluster,
        point_count: pointCount
      } = cluster.properties;

      if (isCluster) {
        return (
          <ClusterMarker
            key={cluster.id}
            lat={latitude}
            lng={longitude}
            icon={MarkerAssets.mediaUrl}
            pointCount={pointCount}
          />
        );
      }

      return (
        <Marker
          key={index}
          markerId={index}
          lat={latitude}
          lng={longitude}
          name={cluster.properties.name}
          address={cluster.properties.address}
          tel={cluster.properties.tel}
          email={cluster.properties.email}
          website={cluster.properties.website}
          services={cluster.properties.services}
          hasAdditionalMarkerIcons={hasAdditionalMarkerIcons}
          AdditionalMarkerIcons={AdditionalMarkerIcons}
          icon={MarkerAssets.mediaUrl}
          activeMarker={activeMarker}
          setActiveMarker={value => {
            changeActiveMarker(value);
          }}
        />
      );
    });
  };

  return (
    <div className={classes} >
      <GoogleMapReact
        bootstrapURLKeys={{ key: Key }}
        center={Center}
        zoom={Zoom}
        yesIWantToUseGoogleMapApiInternals
        onGoogleApiLoaded={({ map }) => {
          mapRef.current = map;
          window.mapRef = mapRef.current;
          setStyle();
        }}
        onChange={({ zoom, bounds }) => {
          setZoom(zoom);
          setBounds([
            bounds.nw.lng,
            bounds.se.lat,
            bounds.se.lng,
            bounds.nw.lat
          ]);
        }}
      >
        {renderClusters()}
      </GoogleMapReact>
    </div>
  );
};

export default View;
