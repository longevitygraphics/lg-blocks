const MarkerIcon = () => {
  return (
    <svg
      version="1.1"
      id="MarkerIcon"
      x="0px"
      y="0px"
      viewBox="0 0 100 100"
      style={{ enableBackground: "new 0 0 100 100" }}
      xmlSpace="preserve"
    >
      <g id="pin">
        <path
          className="st0"
          d="M86.9,37.6c0,21.5-24.6,49.9-33.5,59.5c-1.6,1.7-4.3,1.8-6,0.2c-0.1-0.1-0.2-0.2-0.2-0.2
		c-8.9-9.5-33.5-37.8-33.5-59.5c0.3-20.2,17-36.3,37.2-36C70.6,1.9,86.5,17.9,86.9,37.6z"
          style={{
            fill: "#1D3767",
            stroke: "#1D3767",
            strokeWidth: "2.963",
            strokeLinecap: "round",
            strokeLinejoin: "round"
          }}
        />
        <circle
          className="st0"
          cx="50.3"
          cy="38.2"
          r="17.2"
          style={{
            fill: "#FFF",
            stroke: "#1D3767",
            strokeWidth: "2.963",
            strokeLinecap: "round",
            strokeLinejoin: "round"
          }}
        />
      </g>
    </svg>
  );
};

export default MarkerIcon;
