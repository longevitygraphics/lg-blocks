/* global easySurveyProps */

/**
 * External dependencies
 */

/**
 * WordPress dependencies
 */
import { createRoot } from "react-dom";

/**
 * Internal dependencies
 */
import View from "../View";

document.addEventListener("DOMContentLoaded", function () {
  // Finds the block containers, and render the React component in them.
  document
  .querySelectorAll(`.lg-advanced-map-block:not(.root-created)`) // exclude any elements that already have a root created
  .forEach(blockContainer => {
      const instanceId = blockContainer.getAttribute("data-block-instance");

      // @ts-ignore this is a global variable.
      // eslint-disable-next-line no-undef
      const props = lgMapProps[instanceId];
      if (!props) {
        return;
      }

      const root = createRoot(blockContainer);

      root.render(<View {...props} />);

      blockContainer.classList.add('root-created');


    });
});
