import react from "react";

const ClusterMarker = ({ icon, pointCount }) => {
  const digits =
    pointCount >= 100
      ? "three-digit"
      : pointCount >= 10
      ? "two-digit"
      : "one-digit";

  return (
    <div className={`lg-cluster-marker ${digits}`}>
      {typeof icon !== undefined && icon !== undefined && (
        <div className={`lg-map-marker__icon `}>
          <img src={icon} alt="Cluster Marker Icon" />
          <div className={`clusterpoints__text`}>{pointCount}</div>
        </div>
      )}
      {typeof icon === undefined ||
        (icon === undefined && (
          <>
            <p>Missing Marker Icon</p>
          </>
        ))}
    </div>
  );
};

export default ClusterMarker;
