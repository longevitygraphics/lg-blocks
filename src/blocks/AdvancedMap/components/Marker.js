import { useState } from "@wordpress/element";
import MarkerIcon from "../assets/markerIcon";

const Marker = props => {
  const {
    lat,
    lng,
    name,
    address,
    tel,
    icon,
    email,
    website,
    services,
    hasAdditionalMarkerIcons,
    AdditionalMarkerIcons
  } = props;

  const [showDetails, setShowDetails] = useState(false);

  const toggleShowOnClick = () => {
    setShowDetails(!showDetails);
  };

  const getServiceIconImage = name => {
    let markerIcons = [...AdditionalMarkerIcons];
    let markerIcon = markerIcons.filter(obj => {
      return obj.name === name;
    });
    return (
      <img
        src={markerIcon[0].mediaUrl}
        id={markerIcon[0].mediaId}
        alt={markerIcon[0].name}
      />
    );
  };

  const renderServices = () => {
    let final = [];
    services.forEach(service => {
      final.push(
        <li className="lg-map-marker__service">
          {getServiceIconImage(service)}
        </li>
      );
    });
    return final;
  };

  return (
    <>
      <div
        lat={lat}
        lng={lng}
        className={`lg-map-marker ${showDetails ? "adjust" : ""}`}
        onClick={toggleShowOnClick}
      >
        <div className={`lg-map-marker__icon ${showDetails ? "hide" : "show"}`}>
          {icon && <img src={icon} alt="Map Marker" />}
          {!icon && <MarkerIcon />}
        </div>

        <div
          className={`lg-map-marker__content ${showDetails ? "show" : "hide"}`}
        >
          <div
            className="lg-map-marker__dismiss"
            onClick={() => {
              setShowDetails(true);
            }}
          >
            X
          </div>
          <div className="outer-triangle" />
          <div className="inner-triangle" />
          <div className="lg-map-marker__name">{name}</div>
          <div className="lg-map-marker__address">{address}</div>
          <div className="lg-map-marker__tel">{tel}</div>
          <div className="lg-map-marker__email">{email}</div>
          <div className="lg-map-marker__website">{website}</div>
          {hasAdditionalMarkerIcons && (
            <ul className="lg-map-marker__services">{renderServices()}</ul>
          )}
        </div>
      </div>
    </>
  );
};

export default Marker;
