export const MarkerDataTemplate = {
  lat: undefined,
  lng: undefined,
  name: "",
  address: "",
  tel: "",
  serviceTypes: []
};

export const MarkerIconTemplate = {
  name: "Icon Name",
  mediaId: 0,
  mediaUrl: ""
};
