import { Component } from "@wordpress/element";

import MarkerIcon from "../assets/markerIcon";

class Marker extends Component {
  constructor(props) {
    super(props);

    this.showing =
      this.props.activeMarker === this.props.markerId ? true : false;

    this.state = { showDetails: this.showing };
  }

  componentDidUpdate(prevProps) {
    if (this.props.activeMarker !== prevProps.activeMarker) {
      this.showing =
        this.props.activeMarker === this.props.markerId ? true : false;
      this.setState({ showDetails: this.showing });
    }
  }

  setActiveMarkerOnClick = () => {
    this.props.setActiveMarker(this.props.markerId);
    console.log(`${this.props.markerId} Clicked`);
  };

  unsetActiveMarker = () => {
    this.props.setActiveMarker(undefined);
    console.log(`${this.props.markerId} Dismiss Clicked`);
  };

  renderServices = () => {
    let final = [];
    this.props.services.forEach(service => {
      final.push(
        <li className="lg-map-marker__service">
          {this.getServiceIconImage(service)}
        </li>
      );
    });
    return final;
  };

  getServiceIconImage = name => {
    let markerIcons = [...this.props.AdditionalMarkerIcons];
    let markerIcon = markerIcons.filter(obj => {
      return obj.name === name;
    });
    return (
      <img
        src={markerIcon[0].mediaUrl}
        id={markerIcon[0].mediaId}
        alt={markerIcon[0].name}
      />
    );
  };

  render() {
    return (
      <>
        <div
          lat={this.props.lat}
          lng={this.props.lng}
          className={`lg-map-marker ${this.state.showDetails ? "adjust" : ""}`}
        >
          <div
            className={`lg-map-marker__icon ${
              this.state.showDetails ? "hide" : "show"
            }`}
            onClick={this.setActiveMarkerOnClick}
          >
            {this.props.icon && <img src={this.props.icon} alt="Map Marker" />}
            {!this.props.icon && <MarkerIcon />}
          </div>

          <div
            className={`lg-map-marker__content ${
              this.state.showDetails ? "show" : "hide"
            }`}
          >
            <div
              className="lg-map-marker__dismiss"
              onClick={() => {
                this.unsetActiveMarker();
              }}
            >
              X
            </div>
            <div className="outer-triangle" />
            <div className="inner-triangle" />
            <div className="lg-map-marker__name">{this.props.name}</div>
            <div className="lg-map-marker__address">{this.props.address}</div>
            <div className="lg-map-marker__tel">
              <a href={`tel:${this.props.tel}`}>{this.props.tel}</a>
            </div>
            {this.props.email && (
              <div className="lg-map-marker__email">
                <a href={`mailto:${this.props.email}`}>{this.props.email}</a>{" "}
              </div>
            )}
            {this.props.website && (
              <div className="lg-map-marker__website">
                <a href={this.props.website} target="_blank" rel="noreferrer">
                  Website
                </a>
              </div>
            )}
            {this.props.hasAdditionalMarkerIcons && (
              <ul className="lg-map-marker__services">
                {this.renderServices()}
              </ul>
            )}
          </div>
        </div>
      </>
    );
  }
}

export default Marker;
