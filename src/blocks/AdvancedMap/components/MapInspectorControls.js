import {
  InspectorControls,
  MediaUpload,
  MediaUploadCheck
} from "@wordpress/block-editor";
import {
  Panel,
  PanelBody,
  TextControl,
  TextareaControl,
  Button,
  ResponsiveWrapper,
  RangeControl,
  Tooltip,
  ToggleControl,
  CheckboxControl
} from "@wordpress/components";

import { __ } from "@wordpress/i18n";

import {
  MarkerDataTemplate,
  MarkerIconTemplate
} from "../components/MarkerDataTemplate";

const MapInspectorControls = props => {
  const { attributes, setAttributes, media } = props;
  const {
    Key,
    Markers,
    MarkerAssets,
    Center,
    Zoom,
    MapStyle,
    hasAdditionalMarkerIcons,
    AdditionalMarkerIcons
  } = attributes;

  const modifyAttribute = (attribute, value) => {
    setAttributes({ [attribute]: value });
    return;
  };

  const modifyAttributeIfValidJson = (attribute, value) => {
    // Gets string, must convert to valid array or fail meaningfully
    try {
      JSON.parse(value);
    } catch (e) {
      // FAIL MEANINGFULLY
      console.log("Failed to update Style, Invalid Styling");
      setAttributes({ [attribute]: "[]" });
      return;
    }
    console.log("Should have updated styling");
    setAttributes({ [attribute]: value });
  };

  const changeMarkerCount = (value, markers) => {
    console.log([markers, value]);
    let newMarkerData = [...markers];
    if (value <= markers.length) {
      // if new value is less than current length, reduce array to length
      let truncatedMarkerData = newMarkerData.slice(0, value);
      console.log(truncatedMarkerData);
      setAttributes({ Markers: truncatedMarkerData });
      return;
    }

    if (value >= markers.length) {
      // if new value is greater than current length, extend array with template objects to new length
      for (let count = 0; count < value; count++) {
        if (value > markers.length) {
          newMarkerData.push(MarkerDataTemplate);
        }
      }
      console.log(newMarkerData);
      setAttributes({ Markers: newMarkerData });
      return;
    }

    console.log("Value Didnt Change!");
    return;
  };

  const changeMarkerIconCount = (value, markers) => {
    console.log([markers, value]);
    let newMarkerData = [...markers];
    if (value <= markers.length) {
      // if new value is less than current length, reduce array to length
      let truncatedMarkerData = newMarkerData.slice(0, value);
      console.log(truncatedMarkerData);
      setAttributes({ Markers: truncatedMarkerData });
      return;
    }

    if (value >= markers.length) {
      // if new value is greater than current length, extend array with template objects to new length
      for (let count = 0; count < value; count++) {
        if (value > markers.length) {
          newMarkerData.push(MarkerIconTemplate);
        }
      }
      console.log(newMarkerData);
      setAttributes({ AdditionalMarkerIcons: newMarkerData });
      return;
    }

    console.log("Value Didnt Change!");
    return;
  };

  const changeMarkerData = (attribute, value, index, markers) => {
    let newMarkerData = [...markers];

    newMarkerData[index][attribute] = value;

    console.log([Markers, newMarkerData]);

    setAttributes({ Markers: newMarkerData });
    return;
  };

  const changeAdditionalMarkerIconData = (
    attribute,
    value,
    index,
    markerIcons
  ) => {
    let newAdditionalMarkerData = [...markerIcons];

    newAdditionalMarkerData[index][attribute] = value;

    console.log([AdditionalMarkerIcons, newAdditionalMarkerData]);

    setAttributes({ AdditionalMarkerIcons: newAdditionalMarkerData });
  };

  const onSelectMedia = media => {
    let newAssets = {
      mediaId: media.id,
      mediaUrl: media.url
    };
    setAttributes({ MarkerAssets: newAssets });
  };

  const removeMedia = () => {
    let newAssets = {
      mediaId: 0,
      mediaUrl: undefined
    };
    setAttributes({ MarkerAssets: newAssets });
  };

  const renderMarkerIconCheckboxes = (markerIcons, markers, markerCount) => {
    let final = [];
    for (let count = 0; count < markerIcons.length; count++) {
      final.push(
        <CheckboxControl
          key={count}
          label={markerIcons[count].name}
          checked={markers[markerCount].serviceTypes.includes(
            markerIcons[count].name
          )}
          onChange={value => {
            let currentServices = [...markers[markerCount].serviceTypes];
            if (currentServices.includes(markerIcons[count].name)) {
              let position = currentServices.indexOf(markerIcons[count].name);
              if (position !== -1) {
                currentServices.splice(position, 1);
              }
            } else {
              currentServices.push(markerIcons[count].name);
            }
            changeMarkerData(
              "serviceTypes",
              currentServices,
              markerCount,
              markers
            );
          }}
        />
      );
    }
    return final;
  };

  const renderMarkerControls = markers => {
    let final = [];
    for (let count = 0; count < markers.length; count++) {
      let display = count + 1;
      final.push(
        <PanelBody title={__(`Marker ${display}`)} key={count}>
          <TextControl
            label={__(`Maker ${display} Latitute`)}
            value={markers[count].lat}
            onChange={value => {
              changeMarkerData("lat", value, count, markers);
            }}
          />
          <TextControl
            label={__(`Maker ${display} Longitude`)}
            value={markers[count].lng}
            onChange={value => {
              changeMarkerData("lng", value, count, markers);
            }}
          />
          <TextControl
            label={__(`Maker ${display} Name`)}
            value={markers[count].name}
            onChange={value => {
              console.log(value);
              changeMarkerData("name", value, count, markers);
            }}
          />
          <TextareaControl
            label={__(`Marker ${display} Address`)}
            value={markers[count].address}
            onChange={value => {
              changeMarkerData("address", value, count, markers);
            }}
          />
          <TextControl
            label={__(`Maker ${display} Phone`)}
            value={markers[count].tel}
            onChange={value => {
              changeMarkerData("tel", value, count, markers);
            }}
          />
          <TextControl
            label={__(`Maker ${display} Email`)}
            value={markers[count].email}
            onChange={value => {
              changeMarkerData("email", value, count, markers);
            }}
          />
          <TextControl
            label={__(`Maker ${display} Website`)}
            value={markers[count].website}
            onChange={value => {
              changeMarkerData("website", value, count, markers);
            }}
          />
          {hasAdditionalMarkerIcons && (
            <div className="additional-icons-checkboxes">
              {renderMarkerIconCheckboxes(
                AdditionalMarkerIcons,
                markers,
                count
              )}
            </div>
          )}
        </PanelBody>
      );
    }
    return final;
  };

  const renderAdditionalMarkerIcons = markerIcons => {
    let final = [];
    for (let count = 0; count < markerIcons.length; count++) {
      let display = count + 1;
      final.push(
        <PanelBody title={__(`Additional Marker Icon ${display}`)} key={count}>
          <TextControl
            label={__(`Maker Icon ${display} Name`)}
            value={markerIcons[count].name}
            onChange={value => {
              changeAdditionalMarkerIconData("name", value, count, markerIcons);
            }}
          />
          <span>Icon</span>
          <MediaUploadCheck>
            <MediaUpload
              allowedTypes={["image"]}
              onSelect={media => {
                changeAdditionalMarkerIconData(
                  "mediaId",
                  media.id,
                  count,
                  markerIcons
                );
                changeAdditionalMarkerIconData(
                  "mediaUrl",
                  media.url,
                  count,
                  markerIcons
                );
              }}
              value={markerIcons[count].mediaId}
              render={({ open }) => (
                <Button
                  onClick={open}
                  className={
                    markerIcons[count].mediaId === 0
                      ? "editor-post-featured-image__toggle"
                      : "editor-post-featured-image__preview"
                  }
                >
                  {markerIcons[count].mediaId === 0 && (
                    <span>Upload an Image</span>
                  )}
                  {markerIcons[count].mediaUrl !== undefined && (
                    <img
                      src={markerIcons[count].mediaUrl}
                      style={{ maxWidth: "100px", maxHeight: "100px" }}
                    />
                  )}
                </Button>
              )}
            />
          </MediaUploadCheck>
          {markerIcons[count].mediaId != 0 && (
            <MediaUploadCheck>
              <Button
                isLink
                isDestructive
                onClick={() => {
                  changeAdditionalMarkerIconData(
                    "mediaId",
                    0,
                    count,
                    markerIcons
                  );
                  changeAdditionalMarkerIconData(
                    "mediaUrl",
                    "",
                    count,
                    markerIcons
                  );
                }}
              >
                {__("Remove Image", "lg-blocks")}
              </Button>
            </MediaUploadCheck>
          )}
        </PanelBody>
      );
    }

    return final;
  };

  return (
    <InspectorControls>
      <Panel header="Longevity Map">
        <PanelBody title={__("Map Options", "lg-blocks")}>
          <p>Zoom: {Zoom}</p>
          <p>
            Center: {Center[0]}, {Center[1]}
          </p>

          <TextControl
            label={__("API Key", "lg-blocks")}
            value={Key}
            onChange={value => {
              modifyAttribute("Key", value);
            }}
          />
          <TextControl
            label={__("# of Markers", "lg-blocks")}
            type="number"
            value={Markers.length}
            onChange={value => {
              changeMarkerCount(value, Markers);
            }}
          />
          <RangeControl
            label={__("Map Zoom", "lg-blocks")}
            step={1}
            max={22}
            min={3}
            onChange={value => {
              modifyAttribute("Zoom", value);
            }}
            value={Zoom}
          />

          <TextareaControl
            label={__("Map Styling", "lg-blocks")}
            value={MapStyle}
            onSelect={e => {
              e.target.select();
            }}
            onChange={value => {
              modifyAttributeIfValidJson("MapStyle", value);
            }}
            help="This section can only be updated by pasting a valid array from
                https://snazzymaps.com/"
          />
          <div style={{ paddingBottom: "3em" }}>
            <span>Marker Icon</span>
            <MediaUploadCheck>
              <MediaUpload
                allowedTypes={["image"]}
                onSelect={onSelectMedia}
                value={MarkerAssets.mediaId}
                render={({ open }) => (
                  <Button
                    onClick={open}
                    className={
                      MarkerAssets.mediaId === 0
                        ? "editor-post-featured-image__toggle"
                        : "editor-post-featured-image__preview"
                    }
                  >
                    {MarkerAssets.mediaId === 0 && <span>Upload an Image</span>}
                    {media !== undefined && (
                      <img
                        src={props.media.source_url}
                        style={{ maxWidth: "100px", maxHeight: "100px" }}
                      />
                    )}
                  </Button>
                )}
              />
            </MediaUploadCheck>
            {attributes.mediaId != 0 && (
              <MediaUploadCheck>
                <Button isLink isDestructive onClick={removeMedia}>
                  {__("Remove Image", "lg-blocks")}
                </Button>
              </MediaUploadCheck>
            )}
          </div>
          <ToggleControl
            label="Additional Marker Icons?"
            checked={hasAdditionalMarkerIcons}
            onChange={value => {
              modifyAttribute("hasAdditionalMarkerIcons", value);
            }}
          />
          {hasAdditionalMarkerIcons && (
            <TextControl
              label={__("# of Marker Icons", "lg-blocks")}
              type="number"
              value={AdditionalMarkerIcons.length}
              onChange={value => {
                changeMarkerIconCount(value, AdditionalMarkerIcons);
              }}
            />
          )}
        </PanelBody>
        {hasAdditionalMarkerIcons &&
          renderAdditionalMarkerIcons(AdditionalMarkerIcons)}
        {renderMarkerControls(Markers)}
      </Panel>
    </InspectorControls>
  );
};

export default MapInspectorControls;
