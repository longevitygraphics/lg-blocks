/**
 * External dependencies
 */
import classnames from "classnames";
import GoogleMapReact from "google-map-react";
import { useRef, useEffect } from "@wordpress/element";

/**
 * Local dependencies
 */
import MapBlockControls from "./components/MapBlockControls";
import MapInspectorControls from "./components/MapInspectorControls.js";
import Marker from "./components/Marker";

import "./style.editor.scss";

const Edit = props => {
  const { className, attributes, setAttributes, media, clientId } = props;
  const {
    Key,
    Markers,
    MarkerAssets,
    Zoom,
    Center,
    BlockId,
    MapStyle,
    hasAdditionalMarkerIcons,
    AdditionalMarkerIcons
  } = attributes;

  if (!BlockId) {
    setAttributes({ BlockId: clientId });
  }

  const classes = classnames({
    "lg-map-advanced": true,
    [className]: !!className
  });

  const mapRef = useRef();

  const maybeSetStyle = () => {
    console.log("Set Style");

    if (mapRef.current) {
      mapRef.current.setOptions({ styles: JSON.parse(MapStyle) });
    }
  };

  const maybeSetCenter = (map, maps) => {
    if (mapRef.current) {
      var bound = new maps.LatLngBounds();
      Markers.forEach( (marker) => {
        bound.extend( new maps.LatLng(marker.lat, marker.lng ));
      }) 

      map.fitBounds(bound);
      console.log(map);
      setAttributes({
        Zoom: map.zoom,
        Center: [49.2290674, -122.8256967]
      });
    }
  }

  useEffect(() => {
    maybeSetStyle();
  });

  const renderMarkers = () => {
    return Markers.map((marker, index) => {
      return (
        <Marker
          key={index}
          lat={marker.lat}
          lng={marker.lng}
          name={marker.name}
          address={marker.address}
          tel={marker.tel}
          email={marker.email}
          website={marker.website}
          services={marker.serviceTypes}
          hasAdditionalMarkerIcons={hasAdditionalMarkerIcons}
          AdditionalMarkerIcons={AdditionalMarkerIcons}
          icon={MarkerAssets.mediaUrl}
        />
      );
    });
  };

  const onMapChange = mapData => {
    setAttributes({
      Zoom: mapData.zoom,
      Center: [mapData.center.lat, mapData.center.lng]
    });
  };
  console.log(props.attributes.Center);
  return (
    <>
      <MapBlockControls />
      <MapInspectorControls
        attributes={attributes}
        setAttributes={setAttributes}
        media={media}
      />
      <div className={classes} style={{ minHeight: "200px", minWidth: "100px", width: "100%", height: "100%" }}>
        <GoogleMapReact
          bootstrapURLKeys={{ key: Key }}
          center={Center}
          zoom={Zoom}
          yesIWantToUseGoogleMapApiInternals
          onGoogleApiLoaded={({ map, maps }) => {
            mapRef.current = map;

            maybeSetStyle();
            maybeSetCenter(map, maps);
          }}
          onChange={mapData => {
            console.log(mapData);
            onMapChange(mapData);
          }}
        >
          {renderMarkers()}
        </GoogleMapReact>
      </div>
    </>
  );
};

export default Edit;
