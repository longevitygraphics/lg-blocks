<?php
/**
 * Gets the inline script for the block attributes.
 *
 * @param array $props The props to add to the variable.
 * @return string The inline script.
 */



function get_inline_script( $props ) {
	ob_start();
	?>
		if ( 'undefined' === typeof lgMapProps ) {
			var lgMapProps = {};
			var count = 0
		}

		var key = "<?php echo esc_js( $props['BlockId'] ); ?>";
		//key = key + count;
		count++;

		lgMapProps[key] = <?php echo wp_json_encode( $props ); ?>;
		window.lgMapProps = lgMapProps;
		<?php
		return ob_get_clean();
}

function lg_blocks_render_advanced_map( $attributes ) {

	wp_enqueue_script(
		'lg-frontend-maps',
		plugins_url( 'AdvancedMap/assets/frontend.js', __DIR__ ),
		array( 'react', 'react-dom', 'wp-element', 'wp-i18n', 'wp-polyfill' ),
		'c8bc1c8b920e235a183347ea5e43dd43',
		false
	);

	wp_add_inline_script(
		'lg-frontend-maps',
		get_inline_script( $attributes ),
		'before'
	);

	ob_start();
	?>
		<div class="lg-advanced-map-block" data-block-instance="<?php echo esc_attr( $attributes['BlockId'] ); ?>"></div>
			<?php

			return ob_get_clean();
}


