/**
 * Wordpress Dependencies
 */
import { registerBlockType } from "@wordpress/blocks";
import metadata from './block.json';
import { __ } from "@wordpress/i18n";

/**
 * Internal Dependencies
 */
import "./style.scss";
import edit from "./edit";

import { withSelect } from "@wordpress/data";

registerBlockType( metadata.name, {
 
  edit: withSelect((select, props) => {
    return {
      media: props.attributes.MarkerAssets.mediaId
        ? select("core").getMedia(props.attributes.MarkerAssets.mediaId)
        : undefined
    };
  })(edit),
  save() {
    return null;
  }
});
