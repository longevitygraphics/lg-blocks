export default {
  Key: {
    type: "string",
    default: ""
  },
  Zoom: {
    type: "number",
    default: 14
  },
  Center: {
    type: "array",
    default: [49.2454626, -122.9432142]
  },
  MapStyle: {
    type: "string",
    default: "[]"
  },
  Markers: {
    type: "array",
    default: [
      {
        lat: "49.24548841166365",
        lng: "-122.94102394117962",
        name: "BC Office",
        address: "4288 Lozells Avenue, Suite 205, \nVancouver, BC V5A 0C7",
        tel: "604-444-0004",
        serviceTypes: []
      }
    ]
  },
  MarkerAssets: {
    type: "object",
    default: {
      mediaId: 0,
      mediaUrl: ""
    }
  },
  BlockId: {
    type: "string"
  },
  hasAdditionalMarkerIcons: {
    type: "boolean",
    default: false
  },
  AdditionalMarkerIcons: {
    type: "array",
    default: [
      {
        name: "Icon 1",
        mediaId: 0,
        mediaUrl: ""
      }
    ]
  }
};
