import { InspectorControls } from "@wordpress/block-editor";
import { __ } from "@wordpress/i18n";
import {
  Button,
  ButtonGroup,
  Icon,
  PanelBody,
  RangeControl,
  ToggleControl,
  __experimentalUnitControl as UnitControl,
} from "@wordpress/components";
import { Component } from "@wordpress/element";
import icons from "../../../utils/icons";

class ParentInspectorControls extends Component {
  constructor(props) {
    super(props);
    this.state = { currentSelector: "mobile" };
  }

  getColumnCount = () => {
    switch (this.state.currentSelector) {
      case "mobile":
        return this.props.attributes.columnsMobile;
      case "tablet":
        return this.props.attributes.columnsTablet;
      case "desktop":
        return this.props.attributes.columnsDesktop;
    }
  };

  getGap = () => {
    switch (this.state.currentSelector) {
      case "mobile":
        return this.props.attributes.gapMobile;
      case "tablet":
        return this.props.attributes.gapTablet;
      case "desktop":
        return this.props.attributes.gapDesktop;
    }
  };

  updateColumnCount = value => {
    switch (this.state.currentSelector) {
      case "mobile":
        this.props.setAttributes({ columnsMobile: value });
        return;
      case "tablet":
        this.props.setAttributes({ columnsTablet: value });
        return;
      case "desktop":
        this.props.setAttributes({ columnsDesktop: value });
        return;
    }
  };

  updateGap = value => {
    switch (this.state.currentSelector) {
      case "mobile":
        this.props.setAttributes({ gapMobile: value });
        return;
      case "tablet":
        this.props.setAttributes({ gapTablet: value });
        return;
      case "desktop":
        this.props.setAttributes({ gapDesktop: value });
        return;
    }
  };

  render() {
    const { setAttributes, attributes } = this.props;
    const { columns, addImage, contentOverlay, addBackground } = attributes;
    return (
      <InspectorControls>
        <PanelBody>
          <div className="lg-button-group-wrap">
            <ButtonGroup>
              <Button
                isSmall
                title="Mobile(Default)"
                isPrimary={this.state.currentSelector === "mobile"}
                onClick={() => {
                  this.setState({ currentSelector: "mobile" });
                }}
              >
                <Icon icon={icons.mobile} />
              </Button>
              <Button
                isSmall
                title="Tablet"
                isPrimary={this.state.currentSelector === "tablet"}
                onClick={() => {
                  this.setState({ currentSelector: "tablet" });
                }}
              >
                <Icon icon={icons.tablet} />
              </Button>
              <Button
                isSmall
                title="Desktop"
                isPrimary={this.state.currentSelector === "desktop"}
                onClick={() => {
                  this.setState({ currentSelector: "desktop" });
                }}
              >
                <Icon icon={icons.desktop} />
              </Button>
            </ButtonGroup>

            <RangeControl
              label={__("Columns", "lg-blocks")}
              value={this.getColumnCount()}
              onChange={value => {
                this.updateColumnCount(value);
              }}
              min={1}
              max={8}
            />
            <UnitControl
                label={__("Column Gap", "lg-blocks")}
                onChange={ (value) => {
                  this.updateGap(value);
                }}
                value = {this.getGap()}
                units = {[
                  { value: 'px', label: 'px', default: 16},
                  { value: '%', label: '%', default: 5},
                  { value: 'rem', label: "rem", default: 1}
                ]}
                size = "small"
            />
          </div>

          <ToggleControl
            label={__("Add a Background Image?", "lg-blocks")}
            onChange={() => setAttributes({ addBackground: !addBackground })}
            checked={addBackground}
          />
          <ToggleControl
            label={__("Add Image to Cards?", "lg-blocks")}
            onChange={() => setAttributes({ addImage: !addImage })}
            checked={addImage}
          />

          {addImage && (
            <ToggleControl
              label={__("Make Content overlay?", "lg-blocks")}
              onChange={() =>
                setAttributes({
                  contentOverlay: !contentOverlay
                })
              }
              checked={contentOverlay}
            />
          )}
        </PanelBody>
      </InspectorControls>
    );
  }
}

export default ParentInspectorControls;
