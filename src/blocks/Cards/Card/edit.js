import { useEffect } from "@wordpress/element";
import { useBlockProps } from "@wordpress/block-editor";
import classnames from "classnames";
import {
  BlockControls,
  InnerBlocks,
  MediaPlaceholder,
  InspectorControls,
  MediaUpload,
  MediaUploadCheck,
  PanelColorSettings,
  withColors,
  URLInputButton
} from "@wordpress/block-editor";
import {
  Button,
  Toolbar,
  RangeControl,
  SelectControl,
  PanelBody,
  TextControl,
  Spinner
} from "@wordpress/components";
import { __ } from "@wordpress/i18n";
import { compose } from "@wordpress/compose";
import { withSelect } from "@wordpress/data";

import DivOrAnchorWrapper from "./div-or-anchor-wrapper";


const ALLOWED_BLOCKS = [
  "core/paragraph",
  "core/heading",
  "core/list",
  "core/button",
  "core/textColumns",
  "core/columns",
  "core/group",
  "core/embed",
  "core/image",
  "lg-blocks/font-awesome",
];
const BLOCK_TEMPLATE = [
  ["core/heading", { content: "Enter Card title...", level: 3 }],
  [
    "core/paragraph",
    {
      content:
        "This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer..."
    }
  ]
];

const Edit = ({
  setAttributes,
  contentOverlay,
  attributes,
  className,
  overlayColor,
  setOverlayColor,
  addImage
}) => {
  const {
    dimRatio,
    imageURL,
    imageAlt,
    imageID,
    cardAnimation,
    activateOverlay,
    iconClass,
    href
  } = attributes;

  useEffect(() => {
    setAttributes({
      activateOverlay: contentOverlay
    });
  }, [setAttributes, contentOverlay]);

  const onSelectImage = image => {
    const { id, url, alt } = image;
    setAttributes({
      imageURL: url,
      imageID: id,
      imageAlt: alt
    });
  };

  const onChangeAnimation = cardAnimation => {
    setAttributes({
      cardAnimation
    });
  };

  const removeImage = () => {
    setAttributes({
      imageURL: "",
      imageID: null,
      imageAlt: ""
    });
  };

  const classes = classnames({
    col: true,
    [className]: !!className
  });

  const cardClasses = classnames({
    card: true,
    activateOverlay: activateOverlay ? true : false
  });

  const cardWrapperClasses = classnames({
    "card-content": true,
    [cardAnimation === undefined ? "" : cardAnimation]: true
  });

  const overlayClasses = classnames({
    [overlayColor.color === undefined
      ? "has-white-background-color"
      : overlayColor.class]: true,
    backgroundOverlay: true
  });

  const contentStyles = {};
  contentStyles["--overlayDimRatio"] = (dimRatio / 100).toString();

  const blockProps = useBlockProps({ className: classes, ...cardClasses });

  return (
    <>
      <InspectorControls>
        <PanelColorSettings
          title={__("Overlay Color", "lg-blocks")}
          initialOpen={true}
          colorSettings={[
            {
              value: overlayColor.color,
              onChange: (...args) => {
                setAttributes({
                  customGradient: undefined
                });
                setOverlayColor(...args);
              },
              label: __("Overlay Color", "lg-blocks")
            }
          ]}
        >
          <RangeControl
            label={__("Background Opacity", "lg-blocks")}
            value={dimRatio}
            onChange={newDimRation =>
              setAttributes({ dimRatio: newDimRation })
            }
            min={0}
            max={100}
            required
          />
        </PanelColorSettings>
        <PanelBody title={__("Card Animations", "lg-blocks")}>
          <SelectControl
            label={__("Card Animation", "lg-blocks")}
            value={cardAnimation}
            options={[
              { label: "None", value: "" },
              { label: "Slide In Top", value: "slide-in-text-top" },
              { label: "Slide In Bottom", value: "slide-in-text-bottom" },
              { label: "Slide In Left", value: "slide-in-text-left" },
              { label: "Slide In Right", value: "slide-in-text-right" }
            ]}
            onChange={onChangeAnimation}
          />
        </PanelBody>
        <PanelBody title={__("Card Image", "lg-blocks")}>
          {!!imageID && (
            <MediaUploadCheck >
              <MediaUpload
                title={__('Card Image', 'image-selector-example')}
                onSelect={onSelectImage}
                value={imageID}
                render={({ open }) => (
                  <Button
                    className="components-icon-button components-toolbar__control"
                    label={__("Edit Image", "lg-blocks")}
                    onClick={open}
                    icon="edit"
                  >
                    {!imageID && (__('Set Image', 'lg-blocks'))}
                  </Button>
                )}
              />
            </MediaUploadCheck>
          )}
          {!!imageID &&
            <MediaUploadCheck>
              <Button onClick={removeImage} isLink isDestructive>
                {__('Remove Image', 'image-selector-example')}
              </Button>
            </MediaUploadCheck>
          }

        </PanelBody>
        <PanelBody title={__("Icon", "lg-blocks")}>
          <TextControl
            label="Icon Class or other classes"
            value={iconClass}
            onChange={iconClass => {
              setAttributes({ iconClass });
            }}
          />
        </PanelBody>
      </InspectorControls>
      <BlockControls>
        <Toolbar>
          <URLInputButton
            url={href}
            onChange={(url, post) =>
              setAttributes({
                href: url,
                text: (post && post.title) || "Click here"
              })
            }
          />
        </Toolbar>
        {imageURL && (
          <Toolbar>
            {imageID && (
              <MediaUploadCheck>
                <MediaUpload
                  onSelect={onSelectImage}
                  allowedTypes={["image"]}
                  value={imageID}
                  render={({ open }) => {
                    return (
                      <Button
                        className="components-icon-button components-toolbar__control"
                        label={__("Edit Image", "lg-blocks")}
                        onClick={open}
                        icon="edit"
                      />
                    );
                  }}
                />
              </MediaUploadCheck>
            )}
            <Button
              className="components-icon-button components-toolbar__control"
              label={__("Remove Image", "lg-blocks")}
              onClick={removeImage}
              icon="trash"
            />
          </Toolbar>
        )}
      </BlockControls>
        <DivOrAnchorWrapper href={href} className={cardClasses} {...blockProps}>
          {addImage && !imageURL && (
            <MediaPlaceholder
              icon="format-image"
              allowedTypes={["image"]}
              onSelect={onSelectImage}
              labels={{
                title: "card image",
                instructions: "upload card image"
              }}
            />
          )}
          {imageURL && addImage && (
            <img
              src={imageURL}
              alt={imageAlt}
              className={imageID ? `wp-image-${imageID}` : null}
            />
          )}
          <div className={cardWrapperClasses}>
            {overlayColor && (
              <div style={contentStyles} className={overlayClasses}></div>
            )}
            {iconClass && <div className={iconClass}></div>}
            <div className="card-body">
              <InnerBlocks
                allowedBlocks={ALLOWED_BLOCKS}
                template={BLOCK_TEMPLATE}
              />
            </div>
          </div>
        </DivOrAnchorWrapper>
        {/* <div className={cardClasses} href={href}>

          </div>*/}
    </>
  );
}


export default compose(
  withColors({ overlayColor: "background-color" }),
  withSelect((select, props) => {
    const childClientId = props.clientId;
    const parentClientId = select("core/block-editor").getBlockRootClientId(
      childClientId
    );
    const imageID = props.attributes.id;
    const parentContentOverlay = select(
      "core/block-editor"
    ).getBlocksByClientId(parentClientId)[0].attributes.contentOverlay;

    return {
      addImage: select("core/block-editor").getBlocksByClientId(
        parentClientId
      )[0].attributes.addImage,
      contentOverlay: parentContentOverlay,
      image: imageID ? select("core").getMedia(imageID) : null
    };
  })
)(Edit);
