import classnames from "classnames";
import { useBlockProps } from "@wordpress/block-editor";
import { InnerBlocks, getColorClassName } from "@wordpress/block-editor";
import DivOrAnchorWrapper from "./div-or-anchor-wrapper";

function save(props) {
  const { attributes } = props;
  const {
    backgroundImageUrl,
    overlayColor,
    dimRatio,
    imageID,
    imageAlt,
    imageURL,
    activateOverlay,
    cardAnimation,
    iconClass,
    href
  } = attributes;

  const classes = classnames({
    col: true
  });
  const overlayColorSlug = overlayColor === undefined ? "white" : overlayColor;
  const cardWrapperClasses = classnames({
    "card-content": true,
    "wp-block-lg-blocks-card": true,
    [cardAnimation === undefined ? "" : cardAnimation]: true
  });
  const cardClasses = classnames({
    card: true,
    activateOverlay: activateOverlay ? true : false
  });
  const overlayClasses = classnames({
    [getColorClassName("background-color", overlayColorSlug)]: true,
    backgroundOverlay: true
  });
  const contentStyles = {};
  contentStyles.backgroundImage = backgroundImageUrl
    ? `url(${backgroundImageUrl})`
    : null;
  contentStyles["--overlayDimRatio"] = (dimRatio / 100).toString();

  const blockProps = useBlockProps.save(
    { className: cardClasses }
  )

  return (

    <DivOrAnchorWrapper href={href} className={cardClasses} {...blockProps}>
      {imageURL && <img src={imageURL} alt={imageAlt} className={imageID ? `wp-image-${imageID}` : null} />}
      <div className={cardWrapperClasses}>
        {overlayColor && (
          <div style={contentStyles} className={overlayClasses}></div>
        )}
        {iconClass && <div className={iconClass}></div>}
        <div className="card-body">
          <InnerBlocks.Content />
        </div>
      </div>
    </DivOrAnchorWrapper>

  );
}

export default save;
