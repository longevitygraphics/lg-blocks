const DivOrAnchorWrapper = (props) => {
  const {href, children, className} = props;
  if (href) {
    return <a className={className} href={href} onClick={e => {
      e.preventDefault();
      e.stopPropagation();
      e.nativeEvent.stopImmediatePropagation();
    }}>{children}</a>
  } else {
    return <div className={className}>{children}</div>
  }
}

export default DivOrAnchorWrapper;
