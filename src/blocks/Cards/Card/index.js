import { registerBlockType, registerBlockStyle } from "@wordpress/blocks";
import { __ } from "@wordpress/i18n";
import edit from "./edit";
import save from "./save";

const attributes = {
  href: {
    type: "string",
    selector: ".wp-block-lg-blocks-card > a",
    source: "attribute",
    attribute: "href"
  },
  overlayColor: {
    type: "string"
  },
  customOverlayColor: {
    type: "string"
  },
  dimRatio: {
    type: "number",
    default: 20
  },
  imageURL: {
    type: "string"
  },
  imageAlt: {
    type: "string",
    source: "attribute",
    selector: "img",
    attribute: "alt"
  },
  imageID: {
    type: "number"
  },
  cardAnimation: {
    type: "string"
  },
  activateOverlay: {
    type: "boolean"
  },
  iconClass: {
    string: "string"
  }
};

registerBlockType("lg-blocks/card", {
  title: __("Card", "lg-blocks"),
  description: __("Card", "lg-blocks"),
  category: "lg-category",
  keywords: [__("Card", "lg-blocks")],
  icon: "format-image",
  parent: ["lg-blocks/cards"],
  attributes,
  edit,
  save
});

registerBlockStyle("lg-blocks/card", {
  name: "overlay",
  label: "Overlay"
});
