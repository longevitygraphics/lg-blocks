import { registerBlockType } from "@wordpress/blocks";
import { __ } from "@wordpress/i18n";
import {  InnerBlocks, useBlockProps } from "@wordpress/block-editor";

import classnames from "classnames";

import Edit from "./edit";
import metadata from './block.json';

import './style.scss';

registerBlockType(metadata.name, {

  edit: Edit,
  save(props) {
    const { attributes } = props;
    //console.log(props);
    const {
      addBackground,
      backgroundImage,
      columnsDesktop,
      columnsMobile,
      columnsTablet,
      gapMobile,
      gapTablet,
      gapDesktop,
    } = attributes;
    const contentStyle =
      backgroundImage && addBackground
        ? {
          backgroundImage: "url(" + backgroundImage + ")",
          backgroundRepeat: "no-repeat",
          backgroundPosition: "center",
          backgroundSize: "cover"
        }
        : {};

    contentStyle['--gap'] = gapMobile;
    contentStyle['--gapTablet'] = gapTablet;
    contentStyle['--gapDesktop'] = gapDesktop; 

    const classes = classnames({
      row: true,
      [`row-cols-${columnsMobile}`]: true,
      [`row-cols-md-${columnsTablet}`]: true,
      [`row-cols-lg-${columnsDesktop}`]: true,
      "has-bg-image": backgroundImage && addBackground ? true : false
    });

    const blockProps = useBlockProps.save(
      {'className': `lg-cards has-${columnsMobile}-columns has-${columnsTablet}-tablet-columns has-${columnsDesktop}-desktop-columns`}

    );

    //blockProps.className = blockProps.className.split(" ").filter((value)=> { return value.includes("row") ? "" : value  }).join(" ");

    return (
      <div {...blockProps  } style={contentStyle}>
          <InnerBlocks.Content />
      </div>
    );
  }
});
