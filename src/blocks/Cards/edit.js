import classnames from "classnames";

import {
  BlockControls,
  MediaUploadCheck,
  InnerBlocks,
  MediaUpload,
  MediaPlaceholder
} from "@wordpress/block-editor";
import { __ } from "@wordpress/i18n";
import { Button, Toolbar } from "@wordpress/components";


import "./style.editor.scss";

/**
 * Local dependencies
 */
 import ParentInspectorControls from "./components/ParentInspectorControls";

const Edit = ({ className, attributes, setAttributes, clientId }) => {
    const {
      columnsDesktop,
      addImage,
      contentOverlay,
      addBackground,
      backgroundImage,
      backgroundImageID,
      gapDesktop
    } = attributes;
    //console.log(backgroundImage);
    const classes = classnames({
      "lg-cards": true,
      [className]: !!className
    });

    const classesInner = classnames({
      /*"row": true,*/
      [`has-${columnsDesktop}-columns`]: true,
      "has-bg-image": backgroundImage && addBackground ? true : false
    });

    const contentStyle =
      addBackground && backgroundImage
        ? {
            backgroundImage: "url(" + backgroundImage + ")",
            backgroundRepeat: "no-repeat",
            backgroundPosition: "center",
            backgroundSize: "cover"
          }
        : undefined;

    const selectBackgroundImage = image => {
      const { url, id } = image;
      setAttributes({
        backgroundImage: url,
        backgroundImageID: id
      });
    };

    const removeImage = () => {
      setAttributes({
        backgroundImage: "",
        backgroundImageID: null
      });
    };

    return (
      <div style={{...contentStyle,  '--gap': gapDesktop }} className={classes}>
        <ParentInspectorControls
          attributes={attributes}
          setAttributes={setAttributes}
        />

        <div className={classesInner}>
          <BlockControls>
            {(backgroundImage || addImage ) && (
              <Toolbar>
                {backgroundImageID && (
                  <MediaUploadCheck>
                    <MediaUpload
                      onSelect={image => {
                        selectBackgroundImage(image);
                      }}
                      allowedTypes={["image"]}
                      value={backgroundImageID}
                      render={({ open }) => {
                        return (
                          <Button
                            className="components-icon-button components-toolbar__control"
                            label={__("Edit Image", "lg-blocks")}
                            onClick={open}
                            icon="edit"
                          />
                        );
                      }}
                    />
                  </MediaUploadCheck>
                )}
                <Button
                  className="components-icon-button components-toolbar__control"
                  label={__("Remove Image", "lg-blocks")}
                  onClick={() => removeImage()}
                  icon="trash"
                />
              </Toolbar>
            )}
          </BlockControls>
          {addBackground && !backgroundImage && (
            <MediaPlaceholder
              icon="format-image"
              allowedTypes={["image"]}
              onSelect={image => {
                selectBackgroundImage(image);
              }}
              labels={{
                title: "card image",
                instructions: "upload card image"
              }}
            />
          )}
          <InnerBlocks
            allowedBlocks={["lg-blocks/card"]}
            template={[["lg-blocks/card"], ["lg-blocks/card"]]}
            parentClientId={clientId}
          />
        </div>
      </div>
    );
  }

  export default Edit;