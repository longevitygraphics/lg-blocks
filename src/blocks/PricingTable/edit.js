import dropRight from "lodash/dropRight";
import times from "lodash/times";
import { __ } from "@wordpress/i18n";
import { Component } from "@wordpress/element";
import classnames from "classnames";
import { InnerBlocks, InspectorControls } from "@wordpress/block-editor";
import { PanelBody, RangeControl } from "@wordpress/components";
import { withDispatch } from "@wordpress/data";
import { createBlock } from "@wordpress/blocks";

import "./style.editor.scss";

const ALLOWED_BLOCKS = ["lg-blocks/pricing-table-item"];

class Edit extends Component {
  render() {
    const { attributes, setAttributes, className, updateTables } = this.props;
    const { count } = attributes;
    //console.log(this.props);
    const classes = classnames({
      [className]: !!className,
      [`has-${count}-columns`]: true,
      'wp-block-lg-blocks-pricing-table': true
    });
    return (
      <>
        <InspectorControls>
          <PanelBody>
            <RangeControl
              label={__("Number of plans", "lg-blocks")}
              value={count}
              onChange={newCount => {
                setAttributes({ count: newCount });
                updateTables(count, newCount);
              }}
              min={1}
              max={5}
            />
          </PanelBody>
        </InspectorControls>
        <div className={classes}>
          <InnerBlocks
            template={[
              ["lg-blocks/pricing-table-item"],
              ["lg-blocks/pricing-table-item"]
            ]}
            templateLock="insert"
            allowedBlocks={ALLOWED_BLOCKS}
            __experimentalMoverDirection={count > 1 ? "horizontal" : "vertical"}
          />
        </div>
      </>
    );
  }
}

export default withDispatch((dispatch, ownProps, registry) => ({
  updateTables(previousTables, newTables) {
    const { clientId } = ownProps;
    const { replaceInnerBlocks } = dispatch("core/block-editor");
    const { getBlocks } = registry.select("core/block-editor");

    let innerBlocks = getBlocks(clientId);

    const isAddingTable = newTables > previousTables;

    if (isAddingTable) {
      innerBlocks = [
        ...innerBlocks,
        ...times(newTables - previousTables, () => {
          return createBlock("lg-blocks/pricing-table-item");
        })
      ];
    } else {
      // The removed table will be the last of the inner blocks.
      innerBlocks = dropRight(innerBlocks, previousTables - newTables);
    }

    replaceInnerBlocks(clientId, innerBlocks, false);
  }
}))(Edit);
