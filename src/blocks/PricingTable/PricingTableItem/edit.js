import {__} from "@wordpress/i18n";
import {Component} from "@wordpress/element";
import classnames from "classnames";
import {
  RichText,
  InnerBlocks,
  InspectorControls,
  PanelColorSettings,
  ContrastChecker,
  withColors
} from "@wordpress/block-editor";
import {PanelBody, ToggleControl} from "@wordpress/components";

import "./style.editor.scss";

const ALLOWED_BLOCKS = ["core/button"];

class Edit extends Component {
  render() {
    const {
      attributes,
      setAttributes,
      className,
      backgroundColor,
      textColor,
      setBackgroundColor,
      setTextColor
    } = this.props;
    const {title, price, specialPrice, features, showPricingHeader} = attributes;
    //console.log(this.props);
    const classes = classnames({
      [className]: !!className
    });

    const priceClasses = classnames({
      "pricing-table__price": true,
      "pricing-table__price--line": !!specialPrice
    });

    return (
      <>
        <InspectorControls>
          <PanelBody title={__("Pricing Item Settings", "lg-blocks")}>
            <ToggleControl
              label={__("Show pricing header?")}
              checked={showPricingHeader}
              onChange={value => this.props.setAttributes({showPricingHeader: value})}
            />
          </PanelBody>
          <PanelColorSettings
            title={__("Color Settings", "lg-blocks")}
            intialOpen={false}
            colorSettings={[
              {
                value: backgroundColor.color,
                onChange: setBackgroundColor,
                label: __("Header Background Color", "lg-blocks")
              },
              {
                value: textColor.color,
                onChange: setTextColor,
                label: __("Header Text Color")
              }
            ]}
          >
            <ContrastChecker
              textColor={textColor.color}
              backgroundColor={backgroundColor.color}
            />
          </PanelColorSettings>
        </InspectorControls>

        <div className={classes}>
          {showPricingHeader &&
          <div
            className="pricing-table__header"
            style={{
              backgroundColor: backgroundColor.color,
              color: textColor.color
            }}
          >
            <RichText
              tagName="h4"
              value={title}
              className="pricing-table__title"
              style={{
                color: textColor.color
              }}
              placeholder={__("Plan title", "lg-blocks")}
              onChange={newTitle => setAttributes({title: newTitle})}
              keepPlaceholderOnFocus
            />
            <RichText
              tagName="div"
              value={price}
              className={priceClasses}
              placeholder={__("Price", "lg-blocks")}
              onChange={newPrice => setAttributes({price: newPrice})}
              keepPlaceholderOnFocus
            />
            <RichText
              tagName="div"
              value={specialPrice}
              className="pricing-table__specialPrice"
              placeholder={__("Special Price", "lg-blocks")}
              onChange={newSpecialPrice =>
                setAttributes({specialPrice: newSpecialPrice})
              }
              keepPlaceholderOnFocus
            />
          </div>
          }

          <RichText
            tagName="ul"
            multiline="li"
            value={features}
            className="pricing-table__features"
            placeholder={__("Add features", "lg-blocks")}
            onChange={newFeatures => setAttributes({features: newFeatures})}
            keepPlaceholderOnFocus
          />
          <InnerBlocks
            templateLock={false}
            allowedBlocks={ALLOWED_BLOCKS}
            templateInsertUpdatesSelection={false}
          />
        </div>
      </>
    );
  }
}

export default withColors("backgroundColor", {textColor: "color"})(Edit);
