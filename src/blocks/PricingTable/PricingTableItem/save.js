import {
  RichText,
  InnerBlocks,
  getColorClassName
} from "@wordpress/block-editor";
import classnames from "classnames";

function save({ attributes }) {
  const {
    title,
    price,
    specialPrice,
    features,
    textColor,
    customTextColor,
    backgroundColor,
    customBackgroundColor,
    showPricingHeader
  } = attributes;
  const priceClasses = classnames({
    "pricing-table__price": true,
    "pricing-table__price--line": !!specialPrice
  });
  const backgroundClass = getColorClassName(
    "background-color",
    backgroundColor
  );
  const textClass = getColorClassName("color", textColor);

  const headerClasses = classnames({
    "pricing-table__header": true,
    [backgroundClass]: backgroundClass,
    [textClass]: textClass
  });
  const titleClasses = classnames({
    "pricing-table__title": true,
    [textClass]: textClass
  });
  return (
    <div>
      {showPricingHeader &&
      <div
        className={headerClasses}
        style={{
          backgroundColor: backgroundClass ? undefined : customBackgroundColor,
          color: textClass ? undefined : customTextColor
        }}
      >
        <RichText.Content tagName="h4" value={title} className={titleClasses} />
        <RichText.Content
          tagName="div"
          value={price}
          className={priceClasses}
        />
        <RichText.Content
          tagName="div"
          value={specialPrice}
          className="pricing-table__specialPrice"
        />
      </div>
      }

      <RichText.Content
        tagName="ul"
        value={features}
        className="pricing-table__features"
      />
      <div>
        <InnerBlocks.Content />
      </div>
    </div>
  );
}

export default save;
