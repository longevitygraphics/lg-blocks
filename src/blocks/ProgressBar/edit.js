import ProgressBar from "@ramonak/react-progress-bar";

import { useBlockProps } from "@wordpress/block-editor";

import { InspectorControls } from "@wordpress/block-editor";
import { PanelBody, RangeControl } from "@wordpress/components";
import { __ } from "@wordpress/i18n";

import "./style.editor.scss";

const Edit = (props) => {

  const blockProps = useBlockProps();
  const { attributes, setAttributes } = props;
  const { percentage } = attributes;

  console.log(props);

  return (
    <>
      <InspectorControls>
        <PanelBody title={__("Progress Bar", "lg-blocks")}>
          <RangeControl
            label={__("Percentage", "lg-blocks")}
            onChange={e => {
              setAttributes({ percentage: e });
            }}
            value={percentage}
            min={0}
            max={100}
            step={1}
          />
        </PanelBody>
      </InspectorControls>
      <div {...blockProps}>


        <ProgressBar
          completed={percentage}
          bgColor="#578B44"
          borderRadius="6px"
          height="22px"
          baseBgColor="#F5F5F5"
          labelSize="16px"
          labelAlignment="center"
          animateOnRender={true}
          labelColor="#fff"
          labelClassName={"lg-progress-marker"}
        />
      </div>
    </>
  );

}

export default Edit;
