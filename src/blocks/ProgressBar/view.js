import { render } from "react-dom";

import ProgressBar from "@ramonak/react-progress-bar";


import "./style.scss";

const LGProgressBar = (props) => {

  // const { attributes } = props;
  // const { percentage } = attributes;
  console.log(props);
  const { percentage, className } = props;

  return(
    <div className={`wp-block-lg-blocks-progress-bar ${className}`}>
      <ProgressBar 
        completed={percentage} 
        bgColor="#578B44" 
        borderRadius="6px" 
        height= "22px" 
        baseBgColor="#F5F5F5" 
        labelSize="16px" 
        labelAlignment="center" 
        animateOnRender={true} 
        labelColor="#fff"
        labelClassName={"lg-progress-marker"}
      />
    </div>
  );
}

console.log("Progress Bar Script Loaded.")

document.addEventListener("DOMContentLoaded", function() {
  // Finds the block containers, and render the React component in them.
  document.querySelectorAll(`.lg-progress-bar-block`).forEach(blockContainer => {
      const blockProps = blockContainer.getAttribute("data-block-attributes");
      console.log(blockProps);

      // @ts-ignore this is a global variable.
      // eslint-disable-next-line no-undef
      const props = JSON.parse(blockProps);


      render(<LGProgressBar {...props} />, blockContainer);
    });
});


