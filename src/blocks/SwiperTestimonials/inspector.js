/**
 * Wordpress dependencies
 */
import {Component} from "@wordpress/element";
import {InspectorControls} from "@wordpress/block-editor";
import {PanelBody, RangeControl, SelectControl, TextControl, ToggleControl} from "@wordpress/components";
import {__} from "@wordpress/i18n";

class Inspector extends Component {
  layoutOptions = [
    {label: " Slider", value: "slider"},
    {label: " List", value: "list"}
  ];



  render() {
    const {attributes, posts, postTypes} = this.props;
    const {
      layoutType,
      showTitle,
      sliderFetchCount,
      sliderSlidesPerView,
      sliderSlidesToScroll,
      sliderDots,
      sliderAutoplay,
      sliderAutoplayDelay,
      sliderArrows,
      sliderFade,
      sliderInfinite,
      sliderLazyload,
      sliderCenterMode,
      sliderCenterPadding,
      showImage
    } = attributes;


    const onSliderSettingToggle = (setting, value) => {
      this.props.setAttributes({ [setting]: value });
    };

    const getPostTypes = () => {
      if (!postTypes) return [];
      let options = [];
      const restrictedTypes = ["page", "attachment", "wp_block"];
      postTypes.map(postType => {
        if (!restrictedTypes.includes(postType.slug)) {
          options.push({
            label: postType.name,
            value: postType.slug
          });
        }
      });
      return options;
    };
    return (
      <InspectorControls>
        <PanelBody title={__("Slider Options", "lg-blocks")}>
          <SelectControl
            label="Layout Type"
            value={layoutType}
            options={this.layoutOptions}
            onChange={layoutType => this.props.setAttributes({layoutType})}
          />
        </PanelBody>
        <PanelBody title={__("Post Type Options", "lg-blocks")}>
          <SelectControl
            label={__("Post Type?", "lg-blocks")}
            onChange={value => onSliderSettingToggle("postType", value)}
            value={attributes.postType}
            options={getPostTypes()}
          />
        </PanelBody>
        <PanelBody title={__("Display Options", "lg-blocks")}>
          <ToggleControl
            label={__("Show Title?", "lg-blocks")}
            onChange={value => onSliderSettingToggle("showTitle", value)}
            checked={attributes.showTitle}
          />
        </PanelBody>
        {"slider" === layoutType && (
          <PanelBody title={__("Slider Options", "lg-blocks")}>
            <RangeControl
              label={__("Number of Slides to Fetch", "lg-blocks")}
              onChange={value => onSliderSettingToggle("sliderFetchCount", value)}
              value={sliderFetchCount}
              min="1"
              max="15"
              step="1"
            />
            <RangeControl
              label={__("Number of Slides to Show", "lg-blocks")}
              onChange={value =>
                onSliderSettingToggle("sliderSlidesPerView", value)
              }
              value={sliderSlidesPerView}
              min="1"
              max={sliderFetchCount}
              step="1"
            />
            <RangeControl
              label={__("Number of Slides to Scroll", "lg-blocks")}
              onChange={value =>
                onSliderSettingToggle("sliderSlidesToScroll", value)
              }
              value={sliderSlidesToScroll}
              min="1"
              max={sliderSlidesPerView}
              step="1"
            />
            <ToggleControl
              label={__("Dots?", "lg-blocks")}
              onChange={value => onSliderSettingToggle("sliderDots", value)}
              checked={attributes.sliderDots}
            />
            <ToggleControl
              label={__("Arrows?", "lg-blocks")}
              onChange={value => onSliderSettingToggle("sliderArrows", value)}
              checked={attributes.sliderArrows}
            />
            <ToggleControl
              label={__("Fade Transition?", "lg-blocks")}
              onChange={value => onSliderSettingToggle("sliderFade", value)}
              checked={attributes.sliderFade}
            />

            <ToggleControl
              label={__("Autoplay?", "lg-blocks")}
              onChange={value =>
                onSliderSettingToggle("sliderAutoplay", value)
              }
              checked={attributes.sliderAutoplay}
            />
            <ToggleControl
              label={__("Infinite?", "lg-blocks")}
              onChange={value =>
                onSliderSettingToggle("sliderInfinite", value)
              }
              checked={attributes.sliderInfinite}
            />

            <ToggleControl
              label={__("User Excerpt?", "lg-blocks")}
              onChange={value => onSliderSettingToggle("excerpt", value)}
              checked={attributes.excerpt}
            />
            <ToggleControl
              label={__("Show Image?", "lg-blocks")}
              onChange={value => onSliderSettingToggle("showImage", value)}
              checked={attributes.showImage}
            />
            {sliderAutoplay && (
              <TextControl
                label={__("AutoPlay Delay?", "lg-blocks")}
                onChange={value =>
                  onSliderSettingToggle("sliderAutoplayDelay", value)
                }
                value={attributes.sliderAutoplayDelay}
              />
            )}
          </PanelBody>
        )}

      </InspectorControls>
    );
  }
}

export default Inspector;
