const swiperTestimonialsEls = document.querySelectorAll('.swiper-testimonials');
alert("swiper testimonial!");
if ('undefined' != typeof (swiperTestimonialsEls) && null != swiperTestimonialsEls) {
  swiperTestimonialsEls.forEach((swiperTestimonialsEl) => {
    const swiperTestimonialsId = swiperTestimonialsEl.getAttribute('id')
    const swiperTestimonialsConfigStr = swiperTestimonialsEl.getAttribute('data-swiper-config');
    if (swiperTestimonialsConfigStr) {
      const swiperTestimonialsConfig = JSON.parse(swiperTestimonialsConfigStr);
      const defaultConfig = {
        slidesPerView: 1,
        slidesPerGroup: 1,
        spaceBetween: 30

      }
      const finalConfig = {...defaultConfig, ...swiperTestimonialsConfig};
      //console.log(finalConfig);
      console.log("Swiper Testimonials". finalConfig);
      window.swipertestimonial = new Swiper(swiperTestimonialsEl, finalConfig);
    }
  })


}
