<?php
//print_r($attributes);

// global $swiper_testimonials_script_loaded;

	

$swiper_config = array();
$swiper_id     = 'swiper-testimonials-' . wp_rand( 100, 1000 );
$additional_classes = array_key_exists('className', $attributes) ? $attributes['className'] : '';

if ( $attributes['sliderSlidesPerView'] ) {
	$swiper_config['slidesPerView'] = $attributes['sliderSlidesPerView'];
}

if ( $attributes['sliderSlidesToScroll'] ) {
	$swiper_config['slidesPerGroup'] = $attributes['sliderSlidesToScroll'];
}
if ( $attributes['sliderArrows'] && true === $attributes['sliderArrows'] ) {
	$swiper_config['navigation'] = array(
		'nextEl' => '#' . $swiper_id . ' .swiper-button-next',
		'prevEl' => '#' . $swiper_id . ' .swiper-button-prev',
	);
}
if ( $attributes['sliderDots'] && true === $attributes['sliderDots'] ) {
	$swiper_config['pagination'] = array(
		'el'        => '#' . $swiper_id . ' .swiper-pagination',
		'type'      => 'bullets',
		'clickable' => 'true',
	);
}

if ( $attributes['sliderFade'] && true === $attributes['sliderFade'] ) {
	//$swiper_config['effect'] = 'fade';
}

if ( $attributes['sliderAutoplay'] && true === $attributes['sliderAutoplay'] ) {
	$swiper_config['autoplay'] = true;
	if ( $attributes['sliderAutoplayDelay'] ) {
		$swiper_config['autoplay'] = array(
			'delay' => $attributes['sliderAutoplayDelay'],
		);
	}
}
if ( $attributes['sliderInfinite'] && true === $attributes['sliderInfinite'] ) {
	$swiper_config['loop'] = true;
}


//print_r( $swiper_config );
$swiper_config       = wp_json_encode( $swiper_config );

$swiper_testimonials = "<div id='" . $swiper_id . "' class='swiper-container swiper-testimonials $additional_classes' data-swiper-config='$swiper_config'> <div class='swiper-wrapper'>";

// if (!$swiper_testimonials_script_loaded) {
// 	$swiper_testimonials .= '<script src="' . plugin_dir_url(__DIR__) . 'frontend.js" />';
// 	$swiper_testimonials_script_loaded = true;
// }

while ( $slides_query->have_posts() ) {
	$slides_query->the_post();
	if ( $attributes['excerpt'] && true === $attributes['excerpt'] ) {
		$swiper_testimonials_slide_content = get_the_excerpt();
	} else {
		$swiper_testimonials_slide_content = get_the_content();
	}
	$swiper_testimonials_slide_title = '';
	if ( $attributes['showTitle'] && true === $attributes['showTitle'] ) {
		$swiper_testimonials_slide_title = '<h5 class="title">' . get_the_title() . '</h5>';
	}
	$swiper_testimonial_image = '';
	if($attributes['showImage'] && true===$attributes['showImage']){
		if(has_post_thumbnail()){
			$swiper_testimonial_image = "<div class='image mb-2'>" . get_the_post_thumbnail() . "</div>";
		} else {
			$swiper_testimonial_image = "<span class='dashicons dashicons-admin-users'></span>";
		}
	}

	$swiper_testimonials .=
		'<div class="swiper-slide">' .
		$swiper_testimonial_image .
		'<div class="content">' .
		$swiper_testimonials_slide_content .
		$swiper_testimonials_slide_title .
		'</div>' .
		'</div>';
}
$swiper_testimonials .= '</div>';

// swiper navigation.
if ( $attributes['sliderDots'] && true === $attributes['sliderDots'] ) {
	$swiper_testimonials .= '<div class="swiper-pagination"></div>';
}

// swiper pagination.
if ( $attributes['sliderArrows'] && true === $attributes['sliderArrows'] ) {
	$swiper_testimonials .= '<div class="swiper-button-next"></div><div class="swiper-button-prev"></div>';
}


$swiper_testimonials .= '</div>';


