import { withSelect } from "@wordpress/data";

import { useEffect, useState } from "@wordpress/element";
import {
  AlignmentToolbar,
  BlockControls,
  PanelColorSettings,
  InspectorControls
} from "@wordpress/block-editor";
import { isNull, isEmpty, isUndefined } from "lodash";
import { __ } from "@wordpress/i18n";

import "./style.editor.scss";

const Edit = props => {
  const { postData, postTypes, pages } = props;
  const { alignment } = props.attributes;
  const onChangeAlignment = newAlignment => {
    props.setAttributes({
      alignment: newAlignment === undefined ? "none" : "text-" + newAlignment
    });
  };

  const defaultTypes = ["post", "page", "media", "wp_block"];

  const [parentName, setParentName] = useState(null);

  useEffect(() => {
    if (!isNull(postTypes) && !isEmpty(postTypes) && !isUndefined(postTypes)) {
      const postTypeIndex = postTypes.findIndex(
        item => item.slug === postData.type
      );

      if (!defaultTypes.includes(postData.type)) {
        const parent = postTypes[postTypeIndex].name;

        setParentName(parent);
      }
    }

    if (!isNull(pages) && !isEmpty(pages) && !isUndefined(pages)) {
      if (postData.parent !== 0) {
        const parentIndex = pages.findIndex(
          item => item.id === postData.parent
        );
        if (pages[parentIndex] !== undefined) {
          const parent = pages[parentIndex].title.raw;

          setParentName(parent);
        }
      }
    }
  });

  return (
    <>
      <BlockControls>
        <AlignmentToolbar value={alignment} onChange={onChangeAlignment} />
      </BlockControls>

      <div className={alignment}>
        <a>Home</a>
        <span> / </span>
        {parentName && (
          <>
            <a>{parentName}</a>
            <span> / </span>
          </>
        )}
        <span>{postData.title}</span>
      </div>
    </>
  );
};

export default withSelect(select => {
  const { getPostTypes, getEntityRecords } = select("core");
  const { getCurrentPost } = select("core/editor");

  return {
    postTypes: getPostTypes(),
    postData: getCurrentPost(),
    pages: getEntityRecords("postType", "page")
  };
})(Edit);
