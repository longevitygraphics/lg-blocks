<?php
/**
 * Serverside Rendering file for Yoast Breadcrumbs Block
 *
 * @package lg_blocks
 */

?>
<?php

/**
 * Render Callback function for Yoast Breadcrumbs Blocks
 *
 * @param array $attributes Gutenberg Block attributes registered in register_block() in main plugin.php.
 */
function lg_blocks_render_breadcrumb_block( $attributes ) {

	global $post;
	setup_postdata( $post );
	$breadcrumb_content = do_shortcode( '[wpseo_breadcrumb]' );

	return '<div class="lg-breadcrumb ' . $attributes['alignment'] . '" data-post-id="' . $post->ID . '">' . $breadcrumb_content . '</div>';

}
