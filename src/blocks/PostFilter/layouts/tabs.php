<?php

 if ( $terms && is_array( $terms ) ) :
	$post_filter_content .=
	'<div class="filter-tabs d-none d-md-flex ' . $tab_alignment . '">
		<a class="filter" data-slug="all">
			All
		</a>';

	foreach ( $terms as $key => $value ) :
		$post_filter_content .=
		'<a class="filter" data-slug="cat_' . $value->term_id . '">
			' . $value->name . '
		</a>';

		endforeach;
		$post_filter_content .=
		'</div>
		<div class="filter-tabs-mobile d-md-none">
			<h3 class="post-filter-mobile-toggle bg-primary d-flex justify-content-between align-items-center p-2">
				<span class="current-filter text-white"></span><i class="fas fa-chevron-down text-white"></i>
			</h3>
			<div class="filter-tabs-content">
				<a class="filter d-block px-3" data-slug="all">
					All
				</a>';

	foreach ( $terms as $key => $value ) :
		$post_filter_content .=
		'<a class="filter d-block px-3" data-slug="cat_' . $value->term_id . '">
				' . $value->name . '
			</a>';

			endforeach;
			$post_filter_content .=
			'</div>
			</div>';

		endif;
