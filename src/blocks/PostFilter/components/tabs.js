import { Component, Fragment, createRef } from "@wordpress/element";
import ReactDOM from "react-dom";

class Tabs extends Component {

    switchCat = (e) => {
        this.props.setAttributes({
          currentCat: e.target.getAttribute('data-slug')
        })
      }

    render () {
        const { taxTerms, attributes } = this.props;
        const { currentCat } = attributes
        
        
        return (
            <Fragment>
            {taxTerms && taxTerms.length > 0 &&
                <Fragment>
                  <div className="filter-tabs d-none d-md-flex">
                    <a onClick={e => this.switchCat(e)} className={`filter ${currentCat === 'all' && 'active'}`} data-slug="all">
                      All
                    </a>
                    {taxTerms.map((taxTerm, i) => {
                      const activeTab = `cat_${taxTerm.id}` === currentCat ? 'active' : '';
                      return(
                        <a onClick={e => this.switchCat(e)} key={i} className={`filter ${activeTab}`} data-slug={`cat_${taxTerm.id}`}>
                          {taxTerm.name}
                        </a>
                      )
                    })
    
                    }
                  </div>
                  <div className="filter-tabs-mobile d-md-none">
                    <h3 className="post-filter-mobile-toggle bg-primary d-flex justify-content-between align-items-center p-2">
                      <span className="current-filter text-white"></span><i className="fas fa-chevron-down text-white"></i>
                    </h3>
                    <div className="filter-tabs-content">
                      <a className="filter d-block px-3" data-slug="all">
                        All
                      </a>
                      {taxTerms.map((taxTerm, i) => {
                        return(
                          <a key={i} className="filter" data-slug={`cat_${taxTerm.id}`}>
                            {taxTerm.name}
                          </a>
                        )
                        })
                      }
                    </div>
                </div>
              </Fragment>
              }
              </Fragment>
        )
    }
}

export default Tabs;