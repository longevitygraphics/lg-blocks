import "./style.editor.scss";
import { registerBlockType } from "@wordpress/blocks";
import { __ } from "@wordpress/i18n";
import EditwithSelect from "./edit";
import metadata from './block.json';

import './style.scss';


registerBlockType(metadata.name, {
  edit: EditwithSelect,
  save() {
    return null;
  }
});
