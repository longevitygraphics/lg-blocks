/**
 * External dependencies
 */
import classnames from "classnames";

/**
 * Wordpress dependencies
 */
import { Component, Fragment } from "@wordpress/element";
import { withColors } from "@wordpress/block-editor";
/**
 * Internal dependencies
 */
import Inspector from "./inspector";
import Tabs from "./components/tabs";
import { withSelect } from "@wordpress/data";
import "./style.editor.scss";

class Edit extends Component {

  render() {
    const { isSelected, posts, attributes, taxonomies } = this.props; // post over here is the post type selected not 'post'
    const { taxonomy, currentCat } = attributes;
    
    const taxRestBase = () => {
      const matchedTax = taxonomies && taxonomies.length > 0 ? taxonomies.find((tax) => {
        return tax.slug == taxonomy;
      }) : null
      if(matchedTax){
        return matchedTax.rest_base // there will only be one that matches
      }
    } 
    const taxTerms = wp.data.select('core').getEntityRecords('taxonomy', taxonomy);
    const getFeaturedMedia = post => {
      const media = wp.data.select("core").getMedia(post.featured_media);
      if (media) {
        return <img src={media.source_url} alt={media.alt_text} />;
      }
      return <div> Loading... </div>;
    };
    // console.log(posts)
    return (
      <Fragment>
        {isSelected && <Inspector {...this.props} />}
        <div className="post-filter">
          <Tabs 
            taxTerms={taxTerms}
            {...this.props}
          />
          <div className="grid py-3 row">
            
            {posts &&
              posts.length > 0 &&
              posts.map((post, i) => {
                const catKeyName = taxRestBase();
                
                const categories = post[`${catKeyName}`] && post[`${catKeyName}`].map((cat) => {
                  return 'cat_' + cat
                })
                
                const hasCurrentCat = categories && categories.length > 0 && categories.find(cat => cat === currentCat);
                console.log(currentCat)
                const showImage = hasCurrentCat || currentCat === 'all' ? 'show' : '';
                return (
                  <div key={i} className={`grid-item col-md-6 col-lg-4 all ${showImage} ${categories && categories.length > 0 && categories.join(' ')}`}>
                      {getFeaturedMedia(post)}
                  </div>
                ) 
              })}
          </div>
        </div>
      </Fragment>
    );
  }
}
const EditSelect = (select, props) => {
  const { attributes } = props;
  const { postType } = attributes;

  
  return {
    posts: select("core").getEntityRecords("postType", postType),
    postTypes: select("core").getPostTypes(),
    taxonomies: select("core").getTaxonomies()
  };
};
const EditwithColors = withColors("backgroundColor", { textColor: "color" })(
  Edit
);
const EditwithSelect = withSelect(EditSelect)(EditwithColors);

export default EditwithSelect;
