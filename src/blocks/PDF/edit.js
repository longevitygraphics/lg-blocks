import { useBlockProps, MediaPlaceholder, MediaUpload, MediaUploadCheck } from "@wordpress/block-editor";
import { Document, Page } from 'react-pdf';
import { useState } from "@wordpress/element";



import { pdfjs } from 'react-pdf';

pdfjs.GlobalWorkerOptions.workerSrc = new URL(
  'pdfjs-dist/build/pdf.worker.min.js',
  import.meta.url,
).toString();

import "./style.editor.scss";

import 'react-pdf/dist/Page/AnnotationLayer.css';
import 'react-pdf/dist/Page/TextLayer.css';

const Edit = (props) => {
  const { attributes, setAttributes } = props;
  const { pdfURL } = attributes;
  const blockProps = useBlockProps();

  const [numPages, setNumPages] = useState();
  const [pageNumber, setPageNumber] = useState(1);


  const onDocumentLoadSuccess = (numPages) => {
    setNumPages(numPages);
  }

  if (pdfURL !== undefined) {
    return(
      <div {...blockProps}>
        <Document file={pdfURL} onLoadSuccess={onDocumentLoadSuccess}>
          <Page pageNumber={pageNumber} />
        </Document>
      </div>
    );
  }

  return(
    <div>
      <MediaPlaceholder 
        onSelect={(media ) => {
          console.log(media);
          setAttributes({ pdfURL: media.link})
        }}
        accept="application/pdf"
        allowedTypes={['application/pdf']}
        value={pdfURL}
        notices={[]}
      />
    </div>
  )

}


export default Edit;
