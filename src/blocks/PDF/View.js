import { pdfjs } from 'react-pdf';
import { Document, Page } from 'react-pdf';
import { useState, useRef, useEffect } from 'react';

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowUpRightFromSquare, faCaretLeft, faCaretRight } from "@fortawesome/pro-solid-svg-icons";


pdfjs.GlobalWorkerOptions.workerSrc = new URL(
    'pdfjs-dist/build/pdf.worker.min.js',
    import.meta.url,
).toString();


import 'react-pdf/dist/Page/AnnotationLayer.css';
import 'react-pdf/dist/Page/TextLayer.css';



const View = (props) => {

    const [numPages, setNumPages] = useState();
    const [pageNumber, setPageNumber] = useState(1);

    const containerRef = useRef({});

    const onDocumentLoadSuccess = ({ numPages }) => {
        console.log
        setNumPages(numPages);
    };

    const handleNavigation = (change) => {
        if (pageNumber + change === 0 || pageNumber + change > numPages) {
            return
        }
        let documentElement = containerRef.current.children[0];
        let pages = containerRef.current.querySelectorAll('.react-pdf__Page');
        let currIndex = pageNumber - 1;
        let index = currIndex + change;


        documentElement.scrollTo({ top: pages[index].offsetTop, behavior: 'smooth' });
    };

    const handleSpecificNavigation = (page) => {
        if (page <= 0 || page > numPages) {
            return;
        }
        let documentElement = containerRef.current.children[0];
        let pages = containerRef.current.querySelectorAll('.react-pdf__Page');

        documentElement.scrollTo({ top: pages[page - 1].offsetTop, behavior: 'instant' });
        getMostVisiblePage();

    }

    const renderPages = () => {
        let pages = [];
        for (let index = 1; index <= numPages; index++) {
            pages.push(
                <Page pageNumber={index} width={containerRef.current.clientWidth ?? 100} scale={1} id={index} key={index} />
            );
        }
        return pages;
    }

    const getMostVisiblePage = () => {
        let maxVisiblePercentage = 0;
        let mostVisiblePage = null;
        let mostVisiblePageNumber = pageNumber;

        // Get all the react-pdf__Page elements
        const pages = document.querySelectorAll('.react-pdf__Page');

        pages.forEach(page => {
            const rect = page.getBoundingClientRect();

            // Calculate the visible height of the element
            const visibleHeight = Math.min(rect.bottom, window.innerHeight) - Math.max(rect.top, 0);
            const totalHeight = rect.height;

            // Calculate percentage of the element that is visible
            const visiblePercentage = (visibleHeight / totalHeight) * 100;

            if (visiblePercentage > maxVisiblePercentage) {
                maxVisiblePercentage = visiblePercentage;
                mostVisiblePage = page;
                mostVisiblePageNumber = parseInt(page.getAttribute('data-page-number'));
            }
        });

        if (mostVisiblePage) {
            console.log("The page number most in view is:", mostVisiblePageNumber);
            if (mostVisiblePageNumber !== pageNumber) {
                setPageNumber(mostVisiblePageNumber);
            }
        } else {
            console.log("No page is in view.");
        }
    }

    useEffect(() => {
        if (containerRef.current !== undefined) {
            console.log(containerRef.current.children[0]);
            containerRef.current.children[0].addEventListener('scroll', getMostVisiblePage);
            return () => {
                containerRef.current.children[0].removeEventListener('scroll', getMostVisiblePage);
            }
        }

    })





    return (
        <div ref={containerRef}>
            <Document file={props.url} onLoadSuccess={onDocumentLoadSuccess}>
                <div className="controls">
                    <div className='controls__page-number'><span>Page</span> <input value={pageNumber}
                        onChange={
                            (e) => {
                                handleSpecificNavigation(e.target.value);
                                e.target.select();
                            }}
                        onClick={
                            (e) => {
                                e.target.select();
                            }
                        }
                    />
                        <span>/</span> <span>{numPages}</span>
                    </div>
                    <div className="back" onClick={() => handleNavigation(-1)}><FontAwesomeIcon icon={faCaretLeft} /></div>
                    <div className="next" onClick={() => handleNavigation(1)}><FontAwesomeIcon icon={faCaretRight} /></div>
                </div>
                <div className="open"><a target='_blank' href={props.url}><FontAwesomeIcon icon={faArrowUpRightFromSquare} color={"#fff"} /></a></div>
                {renderPages()}
            </Document>
        </div>
    )
}

export default View;