import jQuery from "jquery";

// document.addEventListener("click", (e) => {
//     console.log(e.target.tagName);
//     e.preventDefault();
//     e.stopPropagation();
// })

jQuery(document).ready(function () {

    console.log("Mega Menu FE Script Loaded!");
    const menus = document.querySelectorAll(".wp-block-lg-blocks-mega-menu");

    menus.forEach( menu => {
        let toggle = $(menu).find(".megamenu-open-toggle");
        $(toggle).on("click", (e) => {
            e.stopPropagation();
            $(menu).toggleClass("open"); 
        })
    })

});


