import { useBlockProps, InnerBlocks } from "@wordpress/block-editor";

import "./style.scss";

const Save = () => {

  const blockProps = useBlockProps.save()

  return <div {...blockProps}> <InnerBlocks.Content /><i class="fa-regular fa-bars megamenu-open-toggle"></i>  </div>;
}

export default Save;
