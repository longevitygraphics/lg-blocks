import { useBlockProps, InnerBlocks } from "@wordpress/block-editor";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { withSelect } from "@wordpress/data";

import "./style.editor.scss";

const ALLOWED_BLOCKS = ['lg-blocks/mega-link'];


const Edit = (props) => {
  const { isSelected, isChildSelected } = props;

  const isSelectedOrChildSelected = isSelected || isChildSelected;


  const blockProps = useBlockProps()//{   className: isSelectedOrChildSelected ? "selected" : "" });
  

  return(
    <div {...blockProps}>
      <InnerBlocks allowedBlocks={ALLOWED_BLOCKS} />
      <FontAwesomeIcon icon={"fa-regular fa-bars"} />
    </div>
    );
}


export default withSelect((select, props) => {
  const { clientId } = props;
  const coreEditor = select("core/block-editor");
  return {
    isChildSelected: coreEditor.hasSelectedInnerBlock(clientId, true),
  };
})(Edit);


