import { useBlockProps, InnerBlocks, RichText, InspectorControls } from "@wordpress/block-editor";
import { PanelBody, ToggleControl } from "@wordpress/components";
import { withSelect } from "@wordpress/data";
import { __ } from "@wordpress/i18n";


import classnames from "classnames";
import "./style.editor.scss";

const ALLOWED_BLOCKS = ['core/group', 'lg-blocks/mega-link', 'lg-blocks/font-awesome'];


const Edit = (props) => {

  const { attributes, setAttributes, isSelected, isChildSelected } = props;
  const { isSubmenu, linkText,  } = attributes;

  const isSelectedOrChildSelected = isSelected || isChildSelected;

  const classes = classnames({
    "wp-block-lg-blocks-mega-submenu": isSubmenu,
    "selected": isSubmenu && isSelectedOrChildSelected
  })


  const blockProps = useBlockProps({
    className: classes
  });


  return (
    <>
      <InspectorControls>
        <PanelBody title={__("Block Settings", "lg-blocks")}>
          <ToggleControl
            label={__(`Submenu`)}
            checked={isSubmenu}
            help={isSubmenu ? "Allows for submenu links." : "Does not allow for submenu links."}
            onChange={(value) => {
              console.log(value);
              setAttributes({ isSubmenu: !isSubmenu })
            }}
          />
        </PanelBody>
      </InspectorControls>

      {!isSubmenu && (
        <RichText {...blockProps} tagName="div" value={linkText} onChange={(value) => setAttributes({ linkText: value })} placeholder="Link Text..." />
      )}
      {isSubmenu && (
        <div {...blockProps}>
        <InnerBlocks  allowedBlocks={ALLOWED_BLOCKS} />
        </div>
      )}


    </>
  );
}


export default withSelect((select, props) => {
  const { clientId } = props;
  const coreEditor = select("core/block-editor");
  return {
    isChildSelected: coreEditor.hasSelectedInnerBlock(clientId, true),
  };
})(Edit);
