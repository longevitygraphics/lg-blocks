import { useBlockProps, InnerBlocks, RichText } from "@wordpress/block-editor";

import "./style.scss";

const Save = (props) => {
  const { attributes } = props;
  const { linkText, isSubmenu } = attributes;
  const blockProps = useBlockProps.save({
    className: isSubmenu ? "wp-block-lg-blocks-mega-submenu" : ""
  });

  return (
    <>
      {!isSubmenu && (
        <RichText.Content tagName="div" value={linkText} {...blockProps}/>
      )}
      {isSubmenu && (
        <div {...blockProps}>
        <InnerBlocks.Content />
        </div>
      )}
    </>
    );
}

export default Save;
