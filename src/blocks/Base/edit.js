import { useBlockProps } from "@wordpress/block-editor";

import "./style.editor.scss";


const Edit = () => {

  const blockProps = useBlockProps();
  
  return <div {...blockProps}>LONGEVITY GRAPHICS</div>;
}


export default Edit;
