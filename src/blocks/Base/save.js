import { useBlockProps } from "@wordpress/block-editor";

import "./style.scss";

const Save = () => {

  const blockProps = useBlockProps.save()

  return <div {...blockProps}>LONGEVITY GRAPHICS</div>;
}

export default Save;
