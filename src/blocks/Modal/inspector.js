import { InspectorControls } from "@wordpress/block-editor";
import { __ } from "@wordpress/i18n";
import { PanelBody, TextControl, ToggleControl, } from "@wordpress/components";


const ModalInspectorControls = (props) => {
    const { attributes, setAttributes } = props.data;

    const { hasTitle, fullscreen } = attributes

    return (
        <InspectorControls>
            <PanelBody title={__("Modal Options", "lg-blocks")}>
                <ToggleControl
                    label="Modal Title?"
                    checked={hasTitle}
                    onChange={value => {
                        setAttributes({ hasTitle: value });
                    }}
                />

                <ToggleControl
                    label="Fullscreen Modal?"
                    checked={fullscreen}
                    onChange={value => {
                        setAttributes({ fullscreen: value });
                    }}
                />

            </PanelBody>
        </InspectorControls>

    );

}

export default ModalInspectorControls;
