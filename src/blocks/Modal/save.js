/**
 * External dependencies
 */
import { useBlockProps, InnerBlocks } from "@wordpress/block-editor"

import { v4 as uuid } from 'uuid';

import "./style.scss";


const save = (props) => {

  const { attributes  } = props;
  const { hasTitle, title, dismiss, fullscreen} = attributes;

  const blockProps = useBlockProps.save(
    {
      className: `modal fade`,
    }
  );

  return (
      <div {...blockProps} id={blockProps.id ? `${blockProps.id}` : `${uuid()}`} tabindex="-1" aria-labelledby={hasTitle ?  blockProps.id ? `${blockProps.id}Label` : `${uuid()}Label` : null} aria-hidden="true">
        <div class={`modal-dialog ${fullscreen ? "modal-fullscreen" : null}`}>
          <div class="modal-content"> 
            <div class="modal-header">
            { hasTitle && (
              <p class="modal-title" id={blockProps.id ? `${blockProps.id}Label` : `${uuid()}Label`}>{title}</p>
              )}
              <a class="close" data-bs-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">{dismiss}</span>
              </a>
            </div>
           
            <div class="modal-body">
              <InnerBlocks.Content />
            </div>
          </div>
        </div>
      </div>
  );
}

export default save;
