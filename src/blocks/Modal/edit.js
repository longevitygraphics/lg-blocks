/**
 * External dependencies
 */
import {  InnerBlocks,  useBlockProps, RichText } from "@wordpress/block-editor";
import { __ } from "@wordpress/i18n";

/**
 * Local dependencies
 */
import { Component } from "@wordpress/element";
import ModalInspectorControls from "./inspector";

import "./style.editor.scss";

const Edit = (props) => {

  const blockProps = useBlockProps();
  const {
    attributes,
    setAttributes,
  } = props;


  const { hasTitle, title, dismiss } = attributes;

  return (
    <>
      <ModalInspectorControls  data={{ attributes, setAttributes }}/>
      <div {...blockProps}>
        {hasTitle && (
          <RichText 
            placeholder={__("Modal Title...", "lg-blocks")}
            tagName="p"
            value={title}
            onChange={ (value) => { setAttributes({ title: value})}}
          />
        )}
        <RichText 
            placeholder={__("Modal Dismiss...", "lg-blocks")}
            tagName="span"
            value={dismiss}
            onChange={ (value) => { setAttributes({ dismiss: value})}}
          />
        <InnerBlocks template={[['core/group']]} />
      </div>
    </>
  )
}

export default Edit;
