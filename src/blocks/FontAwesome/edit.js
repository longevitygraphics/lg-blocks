import { useBlockProps, InspectorControls, PanelColorSettings, URLInputButton, BlockControls } from "@wordpress/block-editor";
import { PanelBody, TextControl, SelectControl } from "@wordpress/components";
import { __ } from "@wordpress/i18n";

import { library } from '@fortawesome/fontawesome-svg-core';

import { far } from "@fortawesome/pro-regular-svg-icons";
import { fas } from "@fortawesome/pro-solid-svg-icons";
import { fal } from "@fortawesome/pro-light-svg-icons";
import { fat } from "@fortawesome/pro-thin-svg-icons";



import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

library.add(far, fas, fal, fat);

import "./style.editor.scss";


const Edit = (props) => {

  const { attributes, setAttributes } = props;
  const { icon, iconStyle, iconColor, url } = attributes

  const blockProps = useBlockProps();

  return (
    <>
      <InspectorControls>
        <PanelColorSettings
          title={__('Color Settings')}
          colorSettings={[
            {
              value: iconColor,
              onChange: (colorValue) => setAttributes({ iconColor: colorValue }),
              label: __('Icon Color'),
            },
          ]}
        >
        </PanelColorSettings>
        <PanelBody title={__("Icon Settings", "lg-blocks")}>
          <TextControl
            label={__(`Icon`)}
            value={icon}
            onChange={value => {
              setAttributes({ icon: value });
            }}
          />
          <SelectControl
            label={__(`Icon Style`)}
            value={iconStyle}
            options={[
              { label: 'Regular', value: 'far' },
              { label: 'Solid', value: 'fas' },
              { label: 'Light', value: 'fal' },
              { label: 'Thin', value: 'fat' },
            ]}
            onChange={(value) => setAttributes({ iconStyle: value })}
          />

          <TextControl
            value={url}
            onChange={(value) => setAttributes({ url: value })}
          />
          
        </PanelBody>
      </InspectorControls>
      <BlockControls>
        <URLInputButton 
          url={url}
          onChange={ (url, post) => setAttributes({ url })}
        />
      </BlockControls>
      <div {...blockProps}><FontAwesomeIcon icon={[iconStyle, icon]} color={iconColor} style={ iconColor === "#ffffff" ? { stroke: "#000"} : { }} /></div>
    </>
  );
}


export default Edit;
