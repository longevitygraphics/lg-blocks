import { useBlockProps } from "@wordpress/block-editor";

import "./style.scss";

const Save = (props) => {

  const { attributes } = props;
  const { icon, iconStyle, iconColor, url } = attributes

  const blockProps = useBlockProps.save();

  if (url) {
    return (
      <div {...blockProps}>
        <a href={url}>
          <i class={`${iconStyle} fa-${icon}`} style={{ color: iconColor }} />
        </a>
      </div>
    )

  }

  return <div {...blockProps}><i class={`${iconStyle} fa-${icon}`} style={{ color: iconColor }}></i></div>;
}

export default Save;
