/**
 * Wordpress dependencies
 */
import {InnerBlocks, __experimentalGetGradientClass, getColorClassName} from "@wordpress/block-editor";


/**
 * External dependencies
 */
import getYouTubeID from "get-youtube-id";
import classnames from "classnames";


function save(props) {
  const {attributes} = props;
  const {
    videoUrl,
    minHeight,
    gradient,
    customGradient,
    overlayColor,
    customOverlayColor,
    dimRatio
  } = attributes;

  const overlayStyle = {};
  //overlay bg color
  const overlayColorClass = getColorClassName(
    'background-color',
    overlayColor
  );
  if ( ! overlayColorClass ) {
    overlayStyle.backgroundColor = customOverlayColor;
  }

  //overlay gradient
  const gradientClass = __experimentalGetGradientClass(gradient);
  if (customGradient) {
    overlayStyle.background = customGradient;
  }

  //overlay opacity
  overlayStyle.opacity = (dimRatio / 100).toString();

  const youtubeBackgroundStyle = {
    "height": "100%",
    "width": "100%",
    "zIndex": "0",
    "position": "absolute",
    "overflow": "hidden",
    "inset": "0px",
    "pointerEvents": "none",
    "backgroundImage": "url(https://img.youtube.com/vi/" + getYouTubeID(videoUrl) + "/maxresdefault.jpg)",
    "backgroundSize": "cover",
    "backgroundRepeat": "no-repeat",
    "backgroundPosition": "center center"
  };
  return (
    <div style={{minHeight}}>
      {(overlayColor || customOverlayColor || gradient || customGradient) && (
        <span
          aria-hidden="true"
          className={classnames(
            'video-banner__overlay',
            gradientClass,
            overlayColorClass
          )}
          style={overlayStyle}
        />
      )}
      <div className="youtube-background" data-youtube={videoUrl} style={youtubeBackgroundStyle}></div>
      <div className="content">
        <InnerBlocks.Content/>
      </div>
    </div>
  );
}

export default save;
