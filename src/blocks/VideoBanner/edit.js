/**
 * Wordpress dependencies
 */
import {
  InnerBlocks,
  URLInput,
  withColors,
  __experimentalGetGradientClass,
  getColorClassName, BlockControls
} from "@wordpress/block-editor";
import {compose} from "@wordpress/compose";
import {Icon, video, edit} from '@wordpress/icons';
import {__} from "@wordpress/i18n";
import {useState} from "@wordpress/element";
import {ToolbarButton, ToolbarGroup} from "@wordpress/components";

/**
 * External Dependencies
 */
import classnames from "classnames";
import getYouTubeID from "get-youtube-id";

/**
 * Local dependencies
 */
import Inspector from "./inspector";

import "./style.editor.scss";

function Edit(props) {
  const {className, attributes, setAttributes, isSelected} = props;
  const {videoUrl, minHeight, overlayColor, customOverlayColor, dimRatio, gradient, customGradient} = attributes;

  const [url, setUrl] = useState("");
  const [showVideoUrlInput, setShowVideoUrlInput] = useState(false);

  const onVideoUrlSubmit = (event) => {
    event.preventDefault();
    if (!url) {
      return;
    }
    //Verify if url is really a youtube video url
    if (!getYouTubeID(url)) {
      return;
    }
    setAttributes({videoUrl: url});
    setShowVideoUrlInput(false);
  }

  const blockControls = (
    <BlockControls>
      <ToolbarGroup>
        <ToolbarButton
          icon={edit}
          className="components-icon-button components-toolbar__control"
          label={__("Edit Youtube video URL", "lg-blocks")}
          onClick={() => {
            setShowVideoUrlInput(!showVideoUrlInput);
            setUrl(videoUrl);
          }}
        />
      </ToolbarGroup>
    </BlockControls>
  );

  if (videoUrl && !showVideoUrlInput) {
    const youtubeBackgroundStyle = {
      "height": "100%",
      "width": "100%",
      "zIndex": "0",
      "position": "absolute",
      "overflow": "hidden",
      "inset": "0px",
      "pointerEvents": "none",
      "backgroundImage": "url(https://img.youtube.com/vi/" + getYouTubeID(videoUrl) + "/maxresdefault.jpg)",
      "backgroundSize": "cover",
      "backgroundRepeat": "no-repeat",
      "backgroundPosition": "center center"
    };

    const overlayStyle = {};
    //overlay bg color
    const overlayColorClass = getColorClassName(
      'background-color',
      overlayColor
    );
    if (!overlayColorClass) {
      overlayStyle.backgroundColor = customOverlayColor;
    }

    //overlay gradient
    const gradientClass = __experimentalGetGradientClass(gradient);
    if (customGradient) {
      overlayStyle.background = customGradient;
    }

    //overlay opacity
    overlayStyle.opacity = (dimRatio / 100).toString();


    return (
      <>
        {isSelected && blockControls}
        {isSelected && <Inspector {...props}/>}
        <div className={className} style={{minHeight: minHeight}}>

          {(overlayColor || customOverlayColor || gradient || customGradient) && (
            <span
              aria-hidden="true"
              className={classnames(
                'video-banner__overlay',
                gradientClass,
                overlayColorClass
              )}
              style={overlayStyle}
            />
          )}

          <div className="youtube-background" data-youtube={videoUrl} style={youtubeBackgroundStyle}></div>
          <div className="content">
            <InnerBlocks/>
          </div>
        </div>
      </>

    );
  } else {
    return (
      <>
        {isSelected && blockControls}
        {isSelected && <Inspector {...props}/>}
        <div className="video-input">
          <div className="components-placeholder is-large">
            <div className="components-placeholder__label">
              <span className="block-editor-block-icon">
                <Icon icon={video} size={24}/>
              </span>
              Video Banner
            </div>
            <div className="components-placeholder__instructions">
              {__("Enter the Youtube video URL", "lg-blocks")}
            </div>
            <div className="components-placeholder__fieldset">
              <form onSubmit={onVideoUrlSubmit}>
                <input
                  type="url"
                  value={url}
                  className="components-placeholder__input"
                  aria-label={__("Youtube video URL", "lg-blocks")}
                  placeholder={__('Youtube video URL here…', "lg-blocks")}
                  onChange={(event) => setUrl(event.target.value)}
                />
                <button type="submit" className="components-button is-primary">Submit</button>
              </form>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default compose(
  withColors({overlayColor: "background-color"})
)(Edit);
