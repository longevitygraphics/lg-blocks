/**
 * Wordpress dependencies
 */
import {Component} from "@wordpress/element";
import {
  InspectorControls,
  __experimentalUseGradient,
  __experimentalPanelColorGradientSettings as PanelColorGradientSettings,
} from "@wordpress/block-editor";
import {PanelBody, ToggleControl, RangeControl, __experimentalUnitControl as UnitControl} from "@wordpress/components";
import {__} from "@wordpress/i18n";

function Inspector(props) {

    const {attributes, setAttributes, overlayColor, setOverlayColor} = props;
    const {minHeight, dimRatio} = attributes;

    const units = [
      {value: 'px', label: 'px', default: 600},
      {value: 'vh', label: 'vh', default: 50},
    ];


    const {
      gradientClass,
      gradientValue,
      setGradient,
    } = __experimentalUseGradient();

    return (
      <InspectorControls>
        <PanelBody title={__("Video Banner settings", "lg-blocks")}>
          <UnitControl
            label={__("Minimum Height?", "lg-blocks")}
            units={units}
            size="small"
            value={minHeight}
            onChange={value => {
              console.log(value);
              setAttributes({minHeight: value});
            }}
          />
        </PanelBody>

        <PanelColorGradientSettings
          title={__('Overlay')}
          initialOpen={true}
          settings={[
            {
              colorValue: overlayColor.color,
              gradientValue,
              onColorChange: setOverlayColor,
              onGradientChange: setGradient,
              label: __('Color'),
            },
          ]}
        >
          <RangeControl
            label={__('Opacity')}
            value={dimRatio}
            onChange={(newDimRation) =>
              setAttributes({
                dimRatio: newDimRation,
              })
            }
            min={0}
            max={100}
            step={5}
            required
          />
        </PanelColorGradientSettings>

      </InspectorControls>
    );

}

export default Inspector;
