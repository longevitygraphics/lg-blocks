/**
 * Wordpress dependencies
 */
import { Component, Fragment } from "@wordpress/element";
import { InspectorControls } from "@wordpress/block-editor";
import { Button, ButtonGroup, PanelBody, Icon, ToggleControl, TextControl } from "@wordpress/components";
import {__} from "@wordpress/i18n";

/**
 * Local dependencies
 */
import icons from '../../utils/icons';

class Inspector extends Component {
    setResponsiveMode = (mode) => {
        this.props.setAttributes({
            currentView: mode
        })
    }
    render(){
        const { attributes } = this.props;
        const { includeTextOverlay, tabletBreakPoint, mobileBreakPoint } = attributes;
        console.log(includeTextOverlay)
        return (
            <InspectorControls>
                <PanelBody title={__("Choose Device", "lg-blocks")}>
                    <ButtonGroup>
                    <Button
                        isSmall
                        title="Mobile"
                        onClick={() => this.setResponsiveMode("mobile")}
                    >
                        <Icon icon={icons.mobile}/>
                    </Button>
                    <Button
                        isSmall
                        title="Tablet"
                        onClick={() => this.setResponsiveMode("tablet")}
                    >
                        <Icon icon={icons.tablet}/>
                    </Button>
                    <Button
                        isSmall
                        title="Desktop"
                        onClick={() => this.setResponsiveMode("desktop")}
                    >
                        <Icon icon={icons.desktop}/>
                    </Button>
                    </ButtonGroup>
                </PanelBody>
                <PanelBody title={__("Breakpoints", "lg-blocks")}>
                    <TextControl 
                        label="Tablet Breakpoint"
                        value={tabletBreakPoint}
                        onChange={(value) => this.props.setAttributes({
                            tabletBreakPoint: value
                        })}
                    />
                    <TextControl 
                        label="Mobile Breakpoint"
                        value={mobileBreakPoint}
                        onChange={(value) => this.props.setAttributes({
                            mobileBreakPoint: value
                        })}
                    />
                </PanelBody>
                <PanelBody title={__("Text Overlay", "lg-blocks")}>
                    <ToggleControl 
                        label={__("Text Overlay?")}
                        onChange={() =>  this.props.setAttributes({
                            includeTextOverlay: !includeTextOverlay
                        }) }
                        checked={includeTextOverlay}
                    />
                </PanelBody>
            </InspectorControls>
        )
    }
}

export default Inspector;