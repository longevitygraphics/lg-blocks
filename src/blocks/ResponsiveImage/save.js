/**
 * Wordpress dependencies
 */
import { Component, Fragment } from "@wordpress/element";
import { InnerBlocks } from "@wordpress/block-editor";

class Save extends Component {
    render(){
        const { attributes } = this.props;
        const { desktopImage, tabletBreakPoint, tabletImage, mobileBreakPoint, mobileImage, includeTextOverlay } = attributes;
        return (
            <div className='lg-responsive-image-block'>
                <picture className='lg-responsive-image-block__image'>
                {mobileImage && mobileImage.url !== '' &&
                    <source 
                        media={`(max-width: ${mobileBreakPoint ? mobileBreakPoint : 576 }px)`}
                        srcSet={mobileImage.url}
                        alt={mobileImage.alt}
                    />
                }
                {tabletImage && tabletImage.url !== '' &&
                    <source 
                        media={`(max-width: ${tabletBreakPoint ? tabletBreakPoint : 768 }px)`}
                        srcSet={tabletImage.url}
                        alt={tabletImage.alt}
                    />
                }
                {desktopImage && desktopImage.url !== '' &&
                    <img 
                        src={desktopImage.url} 
                        alt={desktopImage.alt}
                    />
                }
                </picture>
                {includeTextOverlay && 
                    <div className="lg-responsive-image-block__content">
                        <InnerBlocks.Content />
                    </div>
                }
            </div>
        )
    }
}

export default Save;