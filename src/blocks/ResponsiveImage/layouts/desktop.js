/**
 * Wordpress dependencies
 */
import { Component, Fragment } from "@wordpress/element";
import {BlockControls, InnerBlocks, MediaPlaceholder, MediaUpload, MediaUploadCheck} from "@wordpress/block-editor";
import {__} from "@wordpress/i18n";
import {isBlobURL} from "@wordpress/blob";
import {IconButton, Spinner, Toolbar} from "@wordpress/components";
import {withSelect} from "@wordpress/data";

class DesktopLayout extends Component {
    onSelectDesktopImage = image => {
        const { id, alt, url } = image;
        this.props.setAttributes({ 
            desktopImage: {
                id,
                alt,
                url
            }
         })
    }

    removeDesktopImage = () => {
        this.props.setAttributes({ 
            desktopImage: {
                id: '',
                alt: '',
                url: ''
            }
         })
    }
    
    render(){
        const { attributes } = this.props;
        const { desktopImage } = attributes
        return (
            <>
                {desktopImage.url && desktopImage.url !== '' ? (
                    <>
                    <BlockControls>
                        <Toolbar>
                            <MediaUploadCheck>
                                <MediaUpload
                                    onSelect={this.onSelectDesktopImage}
                                    allowedTypes={["image"]}
                                    value={desktopImage.id}
                                    render={({open}) => {
                                    return (
                                        <IconButton
                                            className="components-icon-button components-toolbar__control"
                                            label={__("Add/Edit desktop Image", "lg-blocks")}
                                            onClick={open}
                                            icon="format-image"
                                        />
                                    );
                                    }}
                                />
                                {desktopImage.id && (
                                    <IconButton
                                        className="components-icon-button components-toolbar__control"
                                        label={__("Remove Desktop Image", "lg-blocks")}
                                        onClick={this.removeDesktopImage}
                                        icon="trash"
                                    />
                                )}
                            </MediaUploadCheck>
                        </Toolbar>
                    </BlockControls>
                    <img src={desktopImage.url} alt={desktopImage.alt} />
                    {isBlobURL(desktopImage.url) && <Spinner />}
                    </>
                ) : (
                    <MediaPlaceholder
                        icon="format-image"
                        multiple={false}
                        onSelect={this.onSelectDesktopImage}
                        onError={this.onUploadError}
                        allowedTypes={["image"]}
                        labels={{
                            title: __("Select slider image", "lg-blocks"),
                            instructions: __("Upload or select image from media library.", "lg-blocks")
                        }}
                    />
                )
                }
            </>
        )
    }
}

export default DesktopLayout;