/**
 * Wordpress dependencies
 */
import { Component, Fragment } from "@wordpress/element";
import {BlockControls, InnerBlocks, MediaPlaceholder, MediaUpload, MediaUploadCheck} from "@wordpress/block-editor";
import {__} from "@wordpress/i18n";
import {isBlobURL} from "@wordpress/blob";
import {IconButton, Spinner, Toolbar} from "@wordpress/components";
import {withSelect} from "@wordpress/data";

class MobileLayout extends Component {
    onSelectMobileImage = image => {
        const { id, alt, url } = image;
        this.props.setAttributes({ 
            mobileImage: {
                id,
                alt,
                url
            }
         })
    }

    removeMobileImage = () => {
        this.props.setAttributes({ 
            mobileImage: {
                id: '',
                alt: '',
                url: ''
            }
         })
    }
    
    render(){
        const { attributes } = this.props;
        const { mobileImage, desktopImage, tabletImage } = attributes
        return (
            <>
            {mobileImage.url && mobileImage.url !== '' ? (
                <>
                <BlockControls>
                    <Toolbar>
                        <MediaUploadCheck>
                            <MediaUpload
                                onSelect={this.onSelectMobileImage}
                                allowedTypes={["image"]}
                                value={mobileImage.id}
                                render={({open}) => {
                                return (
                                    <IconButton
                                        className="components-icon-button components-toolbar__control"
                                        label={__("Add/Edit mobile Image", "lg-blocks")}
                                        onClick={open}
                                        icon="format-image"
                                    />
                                );
                                }}
                            />
                            {mobileImage.id && (
                                <IconButton
                                    className="components-icon-button components-toolbar__control"
                                    label={__("Remove Mobile Image", "lg-blocks")}
                                    onClick={this.removeMobileImage}
                                    icon="trash"
                                />
                            )}
                        </MediaUploadCheck>
                    </Toolbar>
                </BlockControls>
                <img src={mobileImage.url} alt={mobileImage.alt} />
                {isBlobURL(mobileImage.url) && <Spinner />}
                </>
            ) : (
                <>
                <BlockControls>
                    <Toolbar>
                        <MediaUploadCheck>
                            <MediaUpload
                                onSelect={this.onSelectMobileImage}
                                allowedTypes={["image"]}
                                value={mobileImage.id}
                                render={({open}) => {
                                return (
                                    <IconButton
                                        className="components-icon-button components-toolbar__control"
                                        label={__("Add/Edit mobile Image", "lg-blocks")}
                                        onClick={open}
                                        icon="format-image"
                                    />
                                );
                                }}
                            />
                        </MediaUploadCheck>
                    </Toolbar>
                </BlockControls>
                {tabletImage.url ? (
                    <img src={tabletImage.url} alt={tabletImage.alt} />
                ) : (
                    <img src={desktopImage.url} alt={desktopImage.alt} />
                ) 
                }
                </>
            )
            }
            </>
        )
    }
}

export default MobileLayout;