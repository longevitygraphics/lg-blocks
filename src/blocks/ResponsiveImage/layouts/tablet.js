/**
 * Wordpress dependencies
 */
import { Component, Fragment } from "@wordpress/element";
import {BlockControls, InnerBlocks, MediaPlaceholder, MediaUpload, MediaUploadCheck} from "@wordpress/block-editor";
import {__} from "@wordpress/i18n";
import {isBlobURL} from "@wordpress/blob";
import {IconButton, Spinner, Toolbar} from "@wordpress/components";
import {withSelect} from "@wordpress/data";

class TabletLayout extends Component {
    onSelectTabletImage = image => {
        const { id, alt, url } = image;
        this.props.setAttributes({ 
            tabletImage: {
                id,
                alt,
                url
            }
         })
    }

    removeTabletImage = () => {
        this.props.setAttributes({ 
            tabletImage: {
                id: '',
                alt: '',
                url: ''
            }
         })
    }
    
    render(){
        const { attributes } = this.props;
        const { tabletImage, desktopImage } = attributes
        return (
            <>
            {tabletImage.url && tabletImage.url !== '' ? (
                <>
                <BlockControls>
                    <Toolbar>
                        <MediaUploadCheck>
                            <MediaUpload
                                onSelect={this.onSelectTabletImage}
                                allowedTypes={["image"]}
                                value={tabletImage.id}
                                render={({open}) => {
                                return (
                                    <IconButton
                                        className="components-icon-button components-toolbar__control"
                                        label={__("Add/Edit tablet Image", "lg-blocks")}
                                        onClick={open}
                                        icon="format-image"
                                    />
                                );
                                }}
                            />
                            {tabletImage.id && (
                                <IconButton
                                    className="components-icon-button components-toolbar__control"
                                    label={__("Remove Tablet Image", "lg-blocks")}
                                    onClick={this.removeTabletImage}
                                    icon="trash"
                                />
                            )}
                        </MediaUploadCheck>
                    </Toolbar>
                </BlockControls>
                <img src={tabletImage.url} alt={tabletImage.alt} />
                {isBlobURL(tabletImage.url) && <Spinner />}
                </>
            ) : (
                <>
                <BlockControls>
                    <Toolbar>
                        <MediaUploadCheck>
                            <MediaUpload
                                onSelect={this.onSelectTabletImage}
                                allowedTypes={["image"]}
                                value={tabletImage.id}
                                render={({open}) => {
                                return (
                                    <IconButton
                                        className="components-icon-button components-toolbar__control"
                                        label={__("Add/Edit tablet Image", "lg-blocks")}
                                        onClick={open}
                                        icon="format-image"
                                    />
                                );
                                }}
                            />
                        </MediaUploadCheck>
                    </Toolbar>
                </BlockControls>
                <img src={desktopImage.url} alt={desktopImage.alt} />
                </>
            )
            }
            </>
        )
    }
}

export default TabletLayout;