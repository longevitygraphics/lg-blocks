import "./style.editor.scss";
import { registerBlockType } from "@wordpress/blocks";
import { __ } from "@wordpress/i18n";
import edit from "./edit";
import save from "./save";
import metadata from './block.json';

import './style.scss';

registerBlockType(metadata.name, {
    
    edit,
    save
})