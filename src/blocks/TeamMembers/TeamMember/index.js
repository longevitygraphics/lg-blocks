import { registerBlockType } from "@wordpress/blocks";
import { __ } from "@wordpress/i18n";
import edit from "./edit";
import { RichText } from "@wordpress/block-editor";
import { Dashicon } from "@wordpress/components";
import metadata from './block.json';



registerBlockType(metadata.name, {
  edit,
  save: ({ attributes }) => {
    //console.log(attributes);
    const { title, subTitle, info, url, id, alt, social } = attributes;
    return (
      <div className="wp-block-lg-blocks-team-member">
        {url && (
          <img src={url} alt={alt} className={id ? `wp-image-${id}` : null} />
        )}
        {title && (
          <RichText.Content
            tagName="h2"
            value={title}
            className={"wp-block-lg-blocks-team-member__title"}
          />
        )}
        {subTitle && (
          <RichText.Content
            tagName="h4"
            value={subTitle}
            className={"wp-block-lg-blocks-team-member__sub-title"}
          />
        )}
        {info && (
          <RichText.Content
            tagName="p"
            value={info}
            className={"wp-block-lg-blocks-team-member__info"}
          />
        )}

        {social.length > 0 && (
          <div className={"wp-block-lg-blocks-team-member__social"}>
            <ul>
              {social.map((item, index) => {
                return (
                  <li key={index} data-icon={item.icon}>
                    <a
                      href={item.link}
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      <Dashicon icon={item.icon} size={20} />
                    </a>
                  </li>
                );
              })}
            </ul>
          </div>
        )}
      </div>
    );
  }
  
});
