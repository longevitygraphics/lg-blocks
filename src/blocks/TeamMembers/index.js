import { registerBlockType, createBlock } from "@wordpress/blocks";
import { __ } from "@wordpress/i18n";
import { InnerBlocks, InspectorControls } from "@wordpress/block-editor";
import { PanelBody, RangeControl } from "@wordpress/components";
import metadata from './block.json';

import './style.scss';

import "./style.editor.scss";


registerBlockType(metadata.name, {
  transforms: {
    from: [
      {
        type: "block",
        blocks: ["core/gallery"],
        transform: ({ images }) => {
          let inner = images.map(({ alt, id, url }) => {
            return createBlock("lg-blocks/team-member", { alt, id, url });
          });
          return createBlock(
            "lg-blocks/team-members",
            {
              columns: images.length
            },
            inner
          );
        }
      },
      {
        type: "block",
        blocks: ["core/image"],
        isMultiBlock: true,
        transform: attributes => {
          let inner = attributes.map(({ alt, id, url }) => {
            return createBlock("lg-blocks/team-member", { alt, id, url });
          });
          return createBlock(
            "lg-blocks/team-members",
            {
              columns: attributes.length
            },
            inner
          );
        }
      }
    ]
  },
  edit({ className, attributes, setAttributes }) {
    const { columns } = attributes;
    return (
      <div className={`wp-block-lg-blocks-team-members has-${columns}-columns`}>
        <InspectorControls>
          <PanelBody>
            <RangeControl
              label={__("Columns", "lg-blocks")}
              value={columns}
              onChange={columns => setAttributes({ columns })}
              min={1}
              max={6}
            />
          </PanelBody>
        </InspectorControls>
        <InnerBlocks
          allowedBlocks={["lg-blocks/team-member"]}
          template={[["lg-blocks/team-member"], ["lg-blocks/team-member"]]}
        />
      </div>
    );
  },
  save({ attributes }) {
    const { columns } = attributes;
    return (
      <div className={`has-${columns}-columns wp-block-lg-blocks-team-members`}>
        <InnerBlocks.Content />
      </div>
    );
  }
});
