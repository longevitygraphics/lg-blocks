/**
 * Wordpress Dependencies
 */
import {registerBlockType} from "@wordpress/blocks";
import {__} from "@wordpress/i18n";

/**
 * Internal Dependencies
 */
import "./style.editor.scss";
import edit from "./edit";
import save from "./save";
import metadata from './block.json';

registerBlockType( metadata.name, {
  edit: edit,
  save: save
});
