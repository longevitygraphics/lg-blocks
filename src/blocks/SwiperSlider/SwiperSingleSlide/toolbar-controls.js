/**
 * Wordpress Dependencies
 */
import { Component } from "@wordpress/element";
import {
  BlockControls,
  BlockVerticalAlignmentToolbar,
  MediaUpload,
  MediaUploadCheck
} from "@wordpress/block-editor";
import { Button, Toolbar, ToolbarButton } from "@wordpress/components";
import { __ } from "@wordpress/i18n";

class ToolbarControls extends Component {
  onSelectBackgroundImage = image => {
    const { id, url } = image;
    this.props.setAttributes({
      mediaId: id,
      mediaUrl: url
    });
  };

  removeBackgroundImage = () => {
    const { setAttributes } = this.props;
    setAttributes({
      mediaUrl: "",
      mediaId: null
    });
  };
  render() {
    const { onSelectBackgroundImage, attributes } = this.props;
    const { mediaId } = attributes;
    return (
      <BlockControls>
        <Toolbar>
          <MediaUploadCheck>
            <MediaUpload
              onSelect={this.onSelectBackgroundImage}
              allowedTypes={["image"]}
              value={mediaId}
              render={({ open }) => {
                return (
                  <Button
                    className="components-icon-button components-toolbar__control"
                    label={__("Add/Edit background Image", "lg-blocks")}
                    onClick={open}
                    icon="format-image"
                  />
                );
              }}
            />
          </MediaUploadCheck>
        </Toolbar>
      </BlockControls>
    );
  }
}

export default ToolbarControls;
