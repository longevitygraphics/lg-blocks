/**
 * External Dependencies.
 */
import classnames from "classnames";

/**
 * Wordpress Dependencies.
 */
import { Component } from "@wordpress/element";
import { InnerBlocks, MediaPlaceholder } from "@wordpress/block-editor";
import { __ } from "@wordpress/i18n";
import { isBlobURL } from "@wordpress/blob";
import { Spinner } from "@wordpress/components";
import { withSelect } from "@wordpress/data";

/**
 *
 */
import ToolbarControls from "./toolbar-controls";

const ALLOWED_BLOCKS = [
  "core/paragraph",
  "core/heading",
  "core/buttons",
  "core/image",
  "core/list",
  "core/group"
];
const TEMPLATE = [["core/heading"]];

class Edit extends Component {
  componentDidUpdate() {
    const {
      lgInnerBlocks,
      setAttributes,
      attributes,
      galleryMode
    } = this.props;
    if (attributes.galleryMode !== galleryMode) {
      setAttributes({ galleryMode: galleryMode });
    }
  }

  onSelectImage = image => {
    const { id, alt, url } = image;
    this.props.setAttributes({ mediaId: id, mediaAlt: alt, mediaUrl: url });
  };

  render() {
    const { className, attributes } = this.props;
    //console.log(this.props);
    const { mediaUrl, mediaId, mediaAlt, galleryMode } = attributes;
    const classes = classnames({
      [className]: !!className,
      "swiper-slide": true
    });
    return (
      <div className={classes}>
        <div className="swiper-slide__img">
          {mediaUrl ? (
            <>
              <ToolbarControls {...this.props} />
              <img
                src={mediaUrl}
                alt={mediaAlt}
                className={mediaId ? `wp-image-${mediaId}` : null}
              />
              <span>{mediaUrl}</span>
              {isBlobURL(mediaUrl) && <Spinner />}
            </>
          ) : (
            <MediaPlaceholder
              icon="format-image"
              multiple={false}
              onSelect={this.onSelectImage}
              //onSelectURL={this.onSelectURL}
              onError={this.onUploadError}
              //accept="image/*"
              allowedTypes={["image"]}
              labels={{
                title: __("Select slider image", "lg-blocks"),
                instructions: __(
                  "Upload or select image from media library.",
                  "lg-blocks"
                )
              }}
            />
          )}
        </div>
        {mediaUrl && !galleryMode && (
          <div className="swiper-slide__content">
            <InnerBlocks allowedBlocks={ALLOWED_BLOCKS} template={TEMPLATE} />
          </div>
        )}
      </div>
    );
  }
}

export default withSelect((select, props) => {
  const { clientId } = props;
  const coreEditor = select("core/block-editor");
  const parentBlock = coreEditor.getBlockParentsByBlockName(
    clientId,
    "lg-blocks/swiper-slider"
  );
  let galleryMode = false;
  if (parentBlock.length > 0 && parentBlock[0]) {
    const parentBlockId = parentBlock[0];
    const parentBlockAttributes = coreEditor.getBlockAttributes(parentBlockId);
    galleryMode = parentBlockAttributes.galleryMode;
  }
  return { galleryMode: galleryMode };
})(Edit);
