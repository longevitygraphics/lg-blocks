/**
 * External Dependencies
 */
import classnames from "classnames";

/**
 * Wordpress Dependencies
 */
import {InnerBlocks} from "@wordpress/block-editor";

const classes = classnames({
  "lg-base": true
});

function save(props) {
  const {attributes} = props;
  const {mediaId, mediaUrl, mediaAlt, galleryMode} = attributes;
  return (
    <div className="swiper-slide">
      <div className="swiper-slide__img">
        <img src={mediaUrl} alt={mediaAlt} className={mediaId ? `wp-image-${mediaId}` : null}/>
      </div>
      {!galleryMode && (
        <div className="swiper-slide__content">
          <InnerBlocks.Content/>
        </div>
      )}
    </div>
  );
}

export default save;
