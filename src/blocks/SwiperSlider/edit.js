/**
 * External dependencies
 */
import classnames from "classnames";
import Swiper from "swiper";

/**
 * Wordpress dependencies
 */
import { __ } from "@wordpress/i18n";
import { Component, createElement } from "@wordpress/element";
import { InnerBlocks, MediaPlaceholder } from "@wordpress/block-editor";
import { withSelect, withDispatch } from "@wordpress/data";
import { compose } from "@wordpress/compose";
import { createBlock } from "@wordpress/blocks";

/**
 * Local dependencies
 */
import ToolbarControls from "./toolbar-controls";
import Inspector from "./inspector";

import "./style.editor.scss";

class Edit extends Component {
  state = {
    preview: false,
    slides: null
  };

  componentDidMount() {
    //const swiperSlider = new Swiper(".swiper-container", {autoplay: false});
  }

  componentDidUpdate() {
    // /debugger
    if (this.state.preview) {
      const swiperSlider = new Swiper(".swiper-container", { autoplay: false });
    }
  }

  onPreviewClick = () => {
    let { preview } = this.state;
    const { previewSwiperSlider } = this.props;
    const previewSlides = previewSwiperSlider();
    preview = !preview;
    this.setState({ preview: preview, slides: previewSlides });
  };
  onMultiSelectImage = images => {
    const { createSwiperSlides } = this.props;
    createSwiperSlides(images);
  };

  onMultiUploadError = () => {
    console.error("Multi Upload Failed. Unknown Reason");
  };

  render() {
    console.log(this.props);
    const {
      className,
      previewSwiperSlider,
      isSelected,
      attributes,
      hasInnerBlocks
    } = this.props;
    const { sliderConfig, galleryMode, arrowsColor, dotsColor } = attributes;
    const {
      autoplay,
      navigation,
      pagination,
      slidesPerView,
      spaceBetween
    } = sliderConfig;
    const classes = classnames({
      [className]: !!className,
      "swiper-container": true,
      "swiper-gallery": galleryMode === true
    });
    let styles = {};
    if (arrowsColor) {
      styles["--swiper-navigation-color"] = arrowsColor;
    }
    if (dotsColor) {
      styles["--swiper-pagination-color"] = arrowsColor;
    }

    if (!hasInnerBlocks) {
      return (
        <div className="multiselect-slides">
          <MediaPlaceholder
            icon="format-image"
            multiple={true}
            onSelect={this.onMultiSelectImage}
            //onSelectURL={this.onSelectURL}
            onError={this.onMultiUploadError}
            //accept="image/*"
            allowedTypes={["image"]}
            labels={{
              title: __("Select slider images", "lg-blocks"),
              instructions: __(
                "Upload or select images from media library. You can select multiple images.",
                "lg-blocks"
              )
            }}
          />
          {isSelected && (
            <ToolbarControls
              {...this.props}
              onPreviewClick={this.onPreviewClick}
            />
          )}
          {isSelected && <Inspector {...this.props} />}
        </div>
      );
    }

    if (this.state.preview === true && this.state.slides != null) {
      let previewSwiperSlider = {
        __html: `<div class="swiper-wrapper">${this.state.slides}</div>`
      };
      return (
        <>
          {isSelected && (
            <ToolbarControls
              {...this.props}
              onPreviewClick={this.onPreviewClick}
              preview={this.state.preview}
            />
          )}
          <div
            className={classes}
            dangerouslySetInnerHTML={previewSwiperSlider}
          ></div>
        </>
      );
    }

    return (
      <div
        className={classes}
        data-swiper-config={JSON.stringify(sliderConfig)}
        style={styles}
      >
        {isSelected && (
          <ToolbarControls
            {...this.props}
            onPreviewClick={this.onPreviewClick}
          />
        )}
        {isSelected && <Inspector {...this.props} />}
        <div className="swiper-wrapper">
          <InnerBlocks
            allowedBlocks={["lg-blocks/swiper-single-slide"]}
            template={[]}
          />
        </div>
        {pagination && <div className="swiper-pagination"></div>}

        {navigation && (
          <>
            <div className="swiper-button-prev"></div>
            <div className="swiper-button-next"></div>
          </>
        )}
      </div>
    );
  }
}

export default compose([
  //applyWithColors,
  withSelect((select, props) => {
    const { clientId } = props;
    //console.log(select('core/block-editor').getBlocks(clientId));
    return {
      hasInnerBlocks: select("core/block-editor").getBlocks(clientId).length > 0
    };
  }),
  withDispatch((dispatch, ownProps, registry) => ({
    previewSwiperSlider() {
      const { clientId, setState } = ownProps;
      const { getBlocks } = registry.select("core/block-editor");
      //get all innerBlockIds
      const innerBlocks = getBlocks(clientId);
      let previewSlides = "";
      innerBlocks.forEach(innerBlock => {
        previewSlides += innerBlock.originalContent;
      });
      return previewSlides;
    },
    createSwiperSlides(images) {
      if (images) {
        let swiperSlides = [];
        const { clientId, attributes } = ownProps;
        const { galleryMode } = attributes;
        images.map(image => {
          let swiperSlide = createBlock("lg-blocks/swiper-single-slide", {
            mediaId: image.id,
            mediaUrl: image.url,
            mediaAlt: image.alt,
            galleryMode: galleryMode
          });
          swiperSlides = [swiperSlide, ...swiperSlides];
        });
        dispatch("core/block-editor").replaceInnerBlocks(
          clientId,
          swiperSlides
        );
      }
    }
  }))
])(Edit);
