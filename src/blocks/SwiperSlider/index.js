/**
 * Wordpress Dependencies
 */
import { registerBlockType } from "@wordpress/blocks";
import { __ } from "@wordpress/i18n";
import metadata from './block.json';

/**
 * Internal Dependencies
 */
import "./style.editor.scss";
import edit from "./edit";
import save from "./save";

import './style.scss';

registerBlockType(metadata.name, {
  edit,
  save
});
