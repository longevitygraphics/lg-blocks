/**
 * External Dependencies.
 */
import classnames from "classnames";

/**
 * Wordpress Dependencies
 */
import {InnerBlocks} from "@wordpress/block-editor";

function save(props) {
  //console.log(props);
  const {attributes} = props;
  const {sliderConfig, galleryMode, arrowsColor, dotsColor} = attributes;
  const {autoplay, navigation, pagination} = sliderConfig;
  const classes = classnames({
    "wp-block-lg-blocks-swiper-slider": true,
    "swiper-container": true,
    "swiper-gallery": galleryMode
  });
  let styles = {};
  if(arrowsColor) {
    styles['--swiper-navigation-color'] = arrowsColor;
  }
  if(dotsColor) {
    styles['--swiper-pagination-color'] = arrowsColor;
  }

  return (
    <div
      className={classes}
      data-swiper-config={JSON.stringify(sliderConfig)}
      style={styles}
    >
      <div className="swiper-wrapper">
        <InnerBlocks.Content/>
      </div>

      {pagination &&
      <div className="swiper-pagination"></div>
      }

      {navigation &&
      <>
        <div className="swiper-button-prev"></div>
        <div className="swiper-button-next"></div>
      </>
      }


    </div>
  );
}

export default save;
