/**
 * Wordpress Dependencies
 */
import { __ } from "@wordpress/i18n";
import { Component } from "@wordpress/element";
import {
  ColorPaletteControl,
  InspectorControls
} from "@wordpress/block-editor";
import {
  Button,
  ButtonGroup,
  Text,
  Icon,
  PanelBody,
  RangeControl,
  TextControl,
  ToggleControl,
  PanelColorSettings
} from "@wordpress/components";
import { isEmpty } from "lodash";

/**
 * Local dependencies
 */
import icons from "../../utils/icons";

class Inspector extends Component {
  constructor(props) {
    super(props);
    this.state = { responsiveMode: "default" };
  }

  setSliderAttributes = (parameter, value) => {
    const { attributes, setAttributes } = this.props;
    const { sliderConfig } = attributes;
    let sliderConfigLocal;
    let parameterValue;
    switch (parameter) {
      case "autoHeight":
      case "loop":
        parameterValue = "";
        ({ [parameter]: parameterValue, ...sliderConfigLocal } = sliderConfig);
        if (value) {
          sliderConfigLocal = {
            ...sliderConfigLocal,
            [parameter]: value
          };
        }
        break;
      case "fade":
        if (value === true) {
          sliderConfigLocal = {
            ...sliderConfigLocal,
            effect: "fade",
            fadeEffect: {
              crossFade: true
            }
          };
        }
        break;
      case "navigation": {
        let navigation;
        ({ navigation, ...sliderConfigLocal } = sliderConfig);
        if (value) {
          sliderConfigLocal = {
            ...sliderConfigLocal,
            navigation: {
              nextEl: ".swiper-button-next",
              prevEl: ".swiper-button-prev"
            }
          };
        }
        break;
      }
      case "pagination": {
        let pagination;
        ({ pagination, ...sliderConfigLocal } = sliderConfig);
        if (value) {
          sliderConfigLocal = {
            ...sliderConfigLocal,
            pagination: { clickable: true, el: ".swiper-pagination" }
          };
        }
        break;
      }
      case "speed": {
        let speed;
        ({ speed, ...sliderConfigLocal } = sliderConfig);
        if (value && value !== 300) {
          sliderConfigLocal = {
            ...sliderConfigLocal,
            speed: parseInt(value)
          };
        }
        break;
      }
      case "autoplay": {
        let autoplay;
        ({ autoplay, ...sliderConfigLocal } = sliderConfig);
        if (value) {
          const autoPlayDelay =
            autoplay != null && typeof autoplay == "object"
              ? autoplay.delay
              : 4000;
          sliderConfigLocal = {
            ...sliderConfigLocal,
            autoplay: { delay: autoPlayDelay }
          };
        }
        break;
      }
      case "slidesPerView": {
        let slidesPerView;
        ({ slidesPerView, ...sliderConfigLocal } = sliderConfig);
        if (this.state.responsiveMode === "default") {
          if (value > 1) {
            sliderConfigLocal = {
              ...sliderConfigLocal,
              slidesPerView: value
            };
          }
        } else {
          let tabletBreakpoint = {};
          let desktopBreakpoint = {};
          if (typeof sliderConfig.breakpoints !== "undefined") {
            ({
              "768": tabletBreakpoint = {},
              "1200": desktopBreakpoint = {}
            } = sliderConfig.breakpoints);
          }

          if (this.state.responsiveMode === "tablet") {
            tabletBreakpoint = {
              ...tabletBreakpoint,
              slidesPerView: value
            };
          }
          if (this.state.responsiveMode === "desktop") {
            desktopBreakpoint = {
              ...desktopBreakpoint,
              slidesPerView: value
            };
          }
          let breakpoints = {};
          if (tabletBreakpoint) {
            breakpoints[768] = tabletBreakpoint;
          }
          if (desktopBreakpoint) {
            breakpoints[1200] = desktopBreakpoint;
          }
          sliderConfigLocal = {
            ...sliderConfig,
            breakpoints: breakpoints
          };
        }
        break;
      }
    }
    setAttributes({ sliderConfig: sliderConfigLocal });
  };

  setResponsiveMode = responsiveMode => {
    this.setState({ responsiveMode: responsiveMode });
  };

  getSlidesPerView = () => {
    const { attributes } = this.props;
    const { sliderConfig } = attributes;
    let slidesPerView = 1;
    switch (this.state.responsiveMode) {
      case "default":
        slidesPerView = sliderConfig.slidesPerView
          ? sliderConfig.slidesPerView
          : 1;
        break;
      case "tablet":
      case "desktop": {
        const breakpoint = this.state.responsiveMode === "tablet" ? 768 : 1200;
        if (
          typeof sliderConfig.breakpoints !== "undefined" &&
          typeof sliderConfig.breakpoints[breakpoint] !== "undefined" &&
          typeof sliderConfig.breakpoints[breakpoint].slidesPerView !==
            "undefined"
        ) {
          slidesPerView = sliderConfig.breakpoints[breakpoint].slidesPerView;
        } else {
          slidesPerView = sliderConfig.slidesPerView
            ? sliderConfig.slidesPerView
            : 1;
        }
        break;
      }
    }
    return slidesPerView;
  };

  render() {
    const { attributes, setAttributes } = this.props;
    const {
      galleryMode,
      sliderConfig,
      dimRatio,
      overlayColor,
      customOverlayColor,
      arrowsColor,
      dotsColor
    } = attributes;
    return (
      <InspectorControls>
        <PanelBody title={__("Slider Options", "lg-blocks")}>
          <ToggleControl
            label="Gallery Mode"
            checked={galleryMode}
            onChange={value => {
              setAttributes({ galleryMode: value });
            }}
          />
          <ToggleControl
            label="Loop"
            checked={!!sliderConfig.loop}
            onChange={value => this.setSliderAttributes("loop", value)}
          />
          <ToggleControl
            label="Auto Height"
            checked={sliderConfig.autoHeight ? sliderConfig.autoHeight : false}
            onChange={value => this.setSliderAttributes("autoHeight", value)}
          />

          <ToggleControl
            label="Autoplay"
            checked={!!sliderConfig.autoplay}
            onChange={value => this.setSliderAttributes("autoplay", value)}
          />

          <ToggleControl
            label="Fade"
            checked={!!sliderConfig.effect}
            onChange={value => this.setSliderAttributes("fade", value)}
          />

          <ToggleControl
            label="Thumb Gallery"
            checked={!!sliderConfig.thumbs}
            onChange={value => this.setSliderAttributes("thumbs", value)}
          />

          {sliderConfig.autoplay != null &&
            typeof sliderConfig.autoplay == "object" && (
              <TextControl
                label="AutoPlay Delay"
                value={sliderConfig.autoplay.delay}
                onChange={value => {
                  setAttributes({
                    sliderConfig: {
                      ...sliderConfig,
                      autoplay: { delay: value }
                    }
                  });
                }}
              />
            )}
          <TextControl
            label="Slide Transition Duration"
            value={sliderConfig.speed ? parseInt(sliderConfig.speed) : 300}
            type="number"
            onChange={value => this.setSliderAttributes("speed", value)}
          />

          <div className="lg-button-group-wrap">
            <ButtonGroup>
              <Button
                isSmall
                title="Mobile(Default)"
                isPrimary={this.state.responsiveMode === "default"}
                onClick={() => this.setResponsiveMode("default")}
              >
                <Icon icon={icons.mobile} />
              </Button>
              <Button
                isSmall
                title="Tablet"
                isPrimary={this.state.responsiveMode === "tablet"}
                onClick={() => this.setResponsiveMode("tablet")}
              >
                <Icon icon={icons.tablet} />
              </Button>
              <Button
                isSmall
                title="Desktop"
                isPrimary={this.state.responsiveMode === "desktop"}
                onClick={() => this.setResponsiveMode("desktop")}
              >
                <Icon icon={icons.desktop} />
              </Button>
            </ButtonGroup>
            <RangeControl
              label="Slides per view"
              value={this.getSlidesPerView()}
              min={1}
              max={10}
              onChange={value =>
                this.setSliderAttributes("slidesPerView", value)
              }
            />
          </div>
        </PanelBody>
        <PanelBody
          title={__("Slider Arrows Settings", "lg-blocks")}
          initialOpen={false}
        >
          <ToggleControl
            label="Show Arrows"
            checked={!!sliderConfig.navigation}
            onChange={value => this.setSliderAttributes("navigation", value)}
          />
          {!!sliderConfig.navigation && (
            <ColorPaletteControl
              label={__("Arrows Color")}
              onChange={value => {
                setAttributes({ arrowsColor: value });
              }}
              value={arrowsColor}
            />
          )}
        </PanelBody>
        <PanelBody
          title={__("Slider Dots Settings", "lg-blocks")}
          initialOpen={false}
        >
          <ToggleControl
            label="Show Dots"
            checked={!!sliderConfig.pagination}
            onChange={value => this.setSliderAttributes("pagination", value)}
          />
          {!!sliderConfig.pagination && (
            <ColorPaletteControl
              label={__("Dots Color")}
              onChange={value => {
                setAttributes({ dotsColor: value });
              }}
              value={dotsColor}
            />
          )}
        </PanelBody>
      </InspectorControls>
    );
  }

  isSmall;
}

export default Inspector;
