import {Swiper  } from "swiper/core";
import { Navigation, Pagination, Autoplay } from "swiper/modules";


Swiper.use([Navigation, Pagination, Autoplay]);

jQuery(document).ready(function(){
  const swiperSliders = document.querySelectorAll(".wp-block-lg-blocks-swiper-slider");
  swiperSliders.forEach(swiperSlider => {
    let swiperConfig = swiperSlider.dataset.swiperConfig;
    swiperConfig = JSON.parse(swiperConfig);
    new Swiper(swiperSlider, swiperConfig);
  });
});
