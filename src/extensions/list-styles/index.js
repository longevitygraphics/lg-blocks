import { __ } from "@wordpress/i18n";
import { registerBlockStyle } from "@wordpress/blocks";

// Default list style for reset.
registerBlockStyle("core/list", {
  name: "default",
  label: __("Default", "lg-blocks"),
  isDefault: true
});

registerBlockStyle("core/list", {
  name: "checkbox-icon",
  label: __("Checkbox Icon", "lg-blocks"),
  isDefault: false
});

registerBlockStyle("core/list", {
  name: "none",
  label: __("None", "lg-blocks"),
  isDefault: false
});
