import { __ } from "@wordpress/i18n";
import {
  RichTextToolbarButton,
  RichTextShortcut
} from "@wordpress/block-editor";
import { Fragment } from "@wordpress/element";
const { toggleFormat } = wp.richText;
const { registerFormatType } = wp.richText;

import './style.scss';

/**
 * adds Open in Video Modal format to RichText block
 */
export const videoModal = {
  name: "lg-blocks/video-modal",
  title: __("Video Modal"),
  tagName: "u",
  className: "video-link",
  attributes: {
    style: "style"
  },
  edit({ isActive, value, onChange }) {
    const onToggle = () => {
      onChange(
        toggleFormat(value, {
          type: "lg-blocks/video-modal",
          attributes: {
            style: 'text-decoration: underline;',
          }
        })
      );
    };
    return (
      <Fragment>
        <RichTextToolbarButton
          icon="editor-underline"
          title={__("Video Modal")}
          onClick={onToggle}
          isActive={isActive}
          shortcutType="primary"
          shortcutCharacter="v"
        />
      </Fragment>
    );
  }

};


/**
 * adds underline format to RichText block
 */
export const underline = {
  name: "lg-blocks/underline",
  title: __("Underline"),
  tagName: "u",
  className: "underline",
  attributes: {
    style: "style"
  },
  edit({ isActive, value, onChange }) {
    const onToggle = () => {
      onChange(
        toggleFormat(value, {
          type: "lg-blocks/underline",
          attributes: {
            style: 'text-decoration: underline; text-transform: uppercase;',
          }
        })
      );
    };
    return (
      <Fragment>
        <RichTextShortcut type="primary" character="u" onUse={onToggle} />
        <RichTextToolbarButton
          icon="editor-underline"
          title={__("Underline")}
          onClick={onToggle}
          isActive={isActive}
          shortcutType="primary"
          shortcutCharacter="u"
        />
      </Fragment>
    );
  }
};



function registerFormats() {
  [underline, videoModal].forEach(({ name, ...settings }) =>
    registerFormatType(name, settings)
  );
}
registerFormats();


