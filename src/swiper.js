// import Swiper bundle with all modules installed
import Swiper from "swiper/swiper-bundle";

// import styles bundle
import "swiper/swiper-bundle.css";

window.Swiper = Swiper;
