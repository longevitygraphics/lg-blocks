import { addFilter } from "@wordpress/hooks"
import { set as _set } from 'lodash'

const modifyCoreBlocks = (settings) => {

  if (settings.name === 'core/navigation-link') {

    settings.parent = [
      ...settings.parent, "lg-blocks/mega-menu", "core/group"
    ]

  }
    
  return settings
}

addFilter('blocks.registerBlockType', 'koa/font-familly-supports', modifyCoreBlocks);