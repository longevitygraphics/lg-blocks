import { addFilter } from "@wordpress/hooks"
import { set as _set } from 'lodash'

function addFontFamillySupport(settings) {
  const names = ['core/heading', 'core/paragraph']

  if(names.includes(settings.name))
    settings = _set(settings, 'supports.typography.__experimentalFontFamily', true)
  
  return settings
}

addFilter('blocks.registerBlockType', 'koa/font-familly-supports', addFontFamillySupport)