<?php

/**
 * Plugin Name:     Longevity Blocks
 * Plugin URI:      https://www.longevitygraphics.com
 * Description:     The Longevity Blocks Plugin
 * Author:          Longevity Graphics
 * Author URI:      https://www.longevitygraphics.com
 * Text Domain:     lg-blocks
 * Domain Path:     /languages
 * Version:         3.5.2
 *
 * @package         lg_blocks
 */

if (!defined('ABSPATH')) {
	exit();
}


require_once(__DIR__ . '/build/blocks/AdvancedMap/AdvancedMap.php');
require_once(__DIR__ . '/build/blocks/Breadcrumb/index.php');
require_once(__DIR__ . '/build/blocks/LatestPosts/latest-posts.php');
require_once(__DIR__ . '/build/blocks/PostFilter/index.php');
require_once(__DIR__ . '/build/blocks/SwiperTestimonials/index.php');
require_once(__DIR__ . '/build/blocks/PageExcerpt/PageExcerpt.php');
require_once(__DIR__ . '/build/hoc/data-attributes/simple_html_dom.php');
require_once(__DIR__ . '/build/hoc/data-attributes/data-attributes.php');
require_once(__DIR__ . '/build/blocks/ProgressBar/ProgressBar.php');
require_once(__DIR__ . '/build/blocks/PDF/PDF.php');

function lg_blocks_categories($categories)
{
	return array_merge(
		$categories,
		array(
			array(
				'slug'  => 'lg-category',
				'title' => __("Longevity Blocks", 'lg-blocks')
			)
		)
	);
}

add_filter('block_categories_all', 'lg_blocks_categories', 10, 2);


function reg_blocks_lg()
{
	register_block_type(__DIR__ . '/build/blocks/Accordion');
	register_block_type(__DIR__ . '/build/blocks/Accordion/AccordionItem');
	register_block_type(__DIR__ . '/build/blocks/AdvancedMap', array(
		'render_callback' => 'lg_blocks_render_advanced_map'
	));
	register_block_type(__DIR__ . '/build/blocks/AdvancedSlider');
	register_block_type(__DIR__ . '/build/blocks/AdvancedSlider/AdvancedSlide');
	register_block_type(__DIR__ . '/build/blocks/AdvancedSlider/VideoSlide');
	register_block_type(__DIR__ . '/build/blocks/AdvancedSlider/TextSlide');
	register_block_type(__DIR__ . '/build/blocks/Base');
	register_block_type(__DIR__ . '/build/blocks/Breadcrumb', array(
		'render_callback' => 'lg_blocks_render_breadcrumb_block'
	));
	register_block_type(__DIR__ . '/build/blocks/Cards');
	register_block_type(__DIR__ . '/build/blocks/Cards/Card');
	register_block_type(__DIR__ . '/build/blocks/Copyright');
	register_block_type(__DIR__ . '/build/blocks/CTA');
	register_block_type(__DIR__ . '/build/blocks/FancyHeading');
	register_block_type(__DIR__ . '/build/blocks/Gallery');
	// register_block_type(__DIR__ . '/build/blocks/ImgText');
	register_block_type(__DIR__ . '/build/blocks/Mailchimp');
	register_block_type(__DIR__ . '/build/blocks/MapText');
	register_block_type(__DIR__ . '/build/blocks/MultiCTA');
	register_block_type(__DIR__ . '/build/blocks/PostVideo');
	register_block_type(__DIR__ . '/build/blocks/PricingTable');
	register_block_type(__DIR__ . '/build/blocks/PricingTable/PricingTableItem');
	register_block_type(__DIR__ . '/build/blocks/ResponsiveImage');
	register_block_type(__DIR__ . '/build/blocks/SwiperTestimonials', array(
		'render_callback' => 'lg_blocks_render_swiper_testimonials'
	));
	register_block_type(__DIR__ . '/build/blocks/Tabs');
	register_block_type(__DIR__ . '/build/blocks/Tabs/Tab');
	register_block_type(__DIR__ . '/build/blocks/TeamMembers');
	register_block_type(__DIR__ . '/build/blocks/TeamMembers/TeamMember');
	register_block_type(__DIR__ . '/build/blocks/VideoBanner');
	register_block_type(__DIR__ . '/build/blocks/PageExcerpt', array(
		'render_callback' => 'lg_blocks_render_page_excerpt'
	));
	register_block_type(__DIR__ . '/build/blocks/Modal');
	register_block_type(__DIR__ . '/build/blocks/SwiperSlider');
	register_block_type(__DIR__ . '/build/blocks/SwiperSlider/SwiperSingleSlide');
	register_block_type(__DIR__ . '/build/blocks/ProgressBar', array(
		'render_callback' => 'lg_blocks_render_progress_bar'
	));
	register_block_type(__DIR__ . '/build/blocks/MegaMenu');
	register_block_type(__DIR__ . '/build/blocks/MegaMenu/MegaLink');
	register_block_type(__DIR__ . '/build/blocks/FontAwesome');
	register_block_type(__DIR__ . '/build/blocks/PDF', array(
		'render_callback' => 'lg_blocks_render_pdf'
	));
	register_block_type(__DIR__ . '/build/blocks/SimpleMap');
	
}

add_action('init', 'reg_blocks_lg');


function reg_formats_lg()
{

	$asset_file = include plugin_dir_path(__FILE__) . 'build/formats/index.asset.php';

	wp_register_script(
		"lg-blocks-formats",
		plugins_url('/build/formats/index.js', __FILE__),
		$asset_file['dependencies'],
		$asset_file['version'],
		true
	);

	wp_enqueue_script(
		"lg-blocks-formats"
	);
}

add_action('enqueue_block_editor_assets', 'reg_formats_lg');

function reg_filters_lg()
{

	$asset_file = include plugin_dir_path(__FILE__) . 'build/filters/index.asset.php';

	wp_register_script(
		"lg-blocks-filters",
		plugins_url('/build/filters/index.js', __FILE__),
		$asset_file['dependencies'],
		$asset_file['version'],
		true
	);

	wp_enqueue_script(
		"lg-blocks-filters"
	);
}

add_action('enqueue_block_editor_assets', 'reg_filters_lg');

function reg_hoc_lg() {
	$asset_file = include plugin_dir_path(__FILE__) . 'build/hoc/index.asset.php';

	wp_register_script(
		"lg-blocks-hoc",
		plugins_url('/build/hoc/index.js', __FILE__),
		$asset_file['dependencies'],
		$asset_file['version'],
		true
	);

	wp_enqueue_script(
		"lg-blocks-hoc"
	);
}

add_action('enqueue_block_editor_assets', 'reg_hoc_lg');

function reg_styles_lg() {

	wp_enqueue_style(
		"lg-blocks-hoc",
		plugins_url('/build/hoc/style-index.css', __FILE__),
		array(),
		filemtime(plugin_dir_path(__FILE__) . '/build/hoc/style-index.css')
	);
	
	wp_enqueue_style(
		"lg-blocks-global",
		plugins_url('/build/hoc/index.css', __FILE__),
		array(),
		filemtime(plugin_dir_path(__FILE__) . '/build/hoc/index.css')
	);
}

add_action( 'init', 'reg_styles_lg' );
add_action( 'admin_enqueue_scripts', 'reg_styles_lg' );