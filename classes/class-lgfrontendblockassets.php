<?php
/**LG Front End Blocks
 *
 * @package LG Blocks classes
 */

/**
 * Longevity front end block assets class
 */
class LGFrontendBlockAssets {
	/**
	 * Block Name
	 *
	 * @var string
	 */
	public $lg_block;
	/**
	 * JavaScript URL
	 *
	 * @var string
	 */
	public $js_url;
	/**
	 * CSS URL
	 *
	 * @var string
	 */
	public $css_url;
	/**
	 *  LG Blocks constructor.
	 *
	 * @param lg_block $lg_block -> block name.
	 */
	public function __construct( $lg_block ) {
		$this->lg_block = $lg_block;
		$this->js_url   = str_replace( ' ', '', ucwords( str_replace( '-', ' ', $this->lg_block ) ) ) . 'Block.js';
		$this->css_url  = str_replace( ' ', '', ucwords( str_replace( '-', ' ', $this->lg_block ) ) ) . 'Block.css';
		$this->enqueue_block_assets( $this->css_url, $this->js_url );
	}
	/**
	 * Enqueue front end block assets
	 *
	 * @param string $css css url of the block.
	 * @param string $js js url of the block.
	 */
	public function enqueue_block_assets( $css, $js ) {
		wp_register_style(
			$this->lg_block,
			plugins_url( 'lg-blocks/dist/' . $css ),
			array(),
			filemtime( plugin_dir_path( __DIR__ ) ),
		);

		wp_enqueue_style( $this->lg_block );

		wp_register_script(
			$this->lg_block,
			plugins_url( 'lg-blocks/dist/' . $js ),
			array( 'jquery' ),
			filemtime( plugin_dir_path( __DIR__ ) ),
			true
		);
		wp_enqueue_script( $this->lg_block );
	}
}
