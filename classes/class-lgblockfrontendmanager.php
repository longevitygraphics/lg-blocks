<?php
/**
 * Class LGBlock
 *
 * @package LG Blocks classes
 */

/**
 * Class to manage front end resources and assets
 */
class LGBlockFrontEndManager {
	/**
	 * Blocks that are used in the front end
	 *
	 * @var array
	 */
	public static $post_blocks = array();
	/**
	 * Check if add swiper globally is enabled
	 *
	 * @var boolean
	 */
	public static $add_swiper_globally;
	/**
	 * Check if swiper assets should be included
	 *
	 * @var boolean
	 */
	public static $include_swiper_assets;
	/**
	 * Check if swiper testimonial assets should be included
	 *
	 * @var boolean
	 */
	public static $include_swiper_testimonial_assets;
	/**
	 * Check if gallery assets should be included
	 *
	 * @var boolean
	 */
	public static $include_gallery_assets;
	/**
	 * Check if post filter block asset should be included
	 *
	 * @var boolean
	 */
	public static $include_post_filter_assets;
	/**
	 * Check if video banner assets should be included
	 *
	 * @var boolean
	 */
	public static $include_video_banner_assets;


	/**
	 *  Function to intialize front end management functions
	 */
	public static function init() {
		add_action( 'wp', array( __CLASS__, 'get_post_blocks' ) );
		add_action(
			'wp_enqueue_scripts',
			function() {
				foreach ( self::$post_blocks as $post_block ) {
					if ( strpos( $post_block, 'lg-blocks' ) === 0 ) {
						new LGFrontendBlockAssets( preg_replace( '/lg-blocks\//', '', $post_block ) );
					}
				}
			}
		);
		if ( ! is_admin() ) {
			add_action( 'init', array( __CLASS__, 'register_ssr_blocks' )  );
		}
		add_action( 'wp_enqueue_scripts', array( __CLASS__, 'enqueue_additional_assets' ) );
	}

	/**
	 *  Function to register block that uses server side render
	 */
	public static function register_ssr_blocks() {
		$has_breadcrumb = in_array( 'lg-blocks/breadcrumb', self::$post_blocks, true );
		new LGBlockFrontendRegistration(
			'breadcrumb',
			array(
				'render_callback' => 'lg_blocks_render_breadcrumb_block',
				'attributes'      => array(
					'alignment' => array(
						'type'    => 'string',
						'default' => 'text-center',
					),
				),
			)
		);
		new LGBlockFrontendRegistration(
			'swiper-testimonials',
			array(
				'render_callback' => 'lg_blocks_render_swiper_testimonials',
				'attributes'      => array(
					'className'            => array(
						'type' => 'string',
					),
					'layoutType'           => array(
						'type'    => 'string',
						'default' => 'slider',
					),
					'postType'             => array(
						'type'    => 'string',
						'default' => 'lg_testimonial',
					),
					'showTitle'            => array(
						'type'    => 'boolean',
						'default' => true,
					),
					'sliderFetchCount'     => array(
						'type'    => 'number',
						'default' => 5,
					),
					'sliderSlidesPerView'  => array(
						'type'    => 'number',
						'default' => 1,
					),
					'sliderSlidesToScroll' => array(
						'type'    => 'number',
						'default' => 1,
					),
					'sliderDots'           => array(
						'type'    => 'boolean',
						'default' => true,
					),
					'sliderArrows'         => array(
						'type'    => 'boolean',
						'default' => false,
					),
					'sliderFade'           => array(
						'type'    => 'boolean',
						'default' => false,
					),
					'sliderAutoplay'       => array(
						'type'    => 'boolean',
						'default' => true,
					),
					'sliderInfinite'       => array(
						'type'    => 'boolean',
						'default' => true,
					),
					'excerpt'              => array(
						'type'    => 'boolean',
						'default' => false,
					),
					'sliderAutoplayDelay'  => array(
						'type'    => 'number',
						'default' => '3000',
					),
					'showImage'            => array(
						'type'    => 'boolean',
						'default' => false,
					),
				),
			),
		);
		new LGBlockFrontendRegistration(
			'post-filter',
			array(
				'render_callback' => 'lg_blocks_render_post_filter',
				'attributes'      => array(
					'postType'     => array(
						'type'    => 'string',
						'default' => 'post',
					),
					'taxonomy'     => array(
						'type'    => 'string',
						'default' => 'category',
					),
					'tabAlignment' => array(
						'type'    => 'string',
						'default' => 'tab-left',
					),
				),
			)
		);

		new LGBlockFrontendRegistration(
			'advanced-map',
			array(
				'render_callback' => 'lg_blocks_render_advanced_map',
				'attributes'      => array(
					'Key'          => array(
						'type' => 'string',
					),
					'Zoom'         => array(
						'type' => 'number',
					),
					'Center'       => array(
						'type' => 'array',
					),
					'Markers'      => array(
						'type' => 'array',
					),
					'MarkerAssets' => array(
						'type' => 'object',
					),
					'BlockId'      => array(
						'type' => 'string',
					),
				),
			)
		);

	}

	/**
	 *  Function to enqueue assets that are additional assets required.
	 */
	public static function enqueue_additional_assets() {
		// check how gallery blocks work later.
		$has_wp_gallery_block          = in_array( 'core/gallery', self::$post_blocks, true );
		$has_swiper_testimonials_block = in_array( 'lg-blocks/swiper-testimonials', self::$post_blocks, true );
		$has_swiper_slider_block       = in_array( 'lg-blocks/swiper-slider', self::$post_blocks, true );
		$has_swiper_block              = in_array( 'lg-blocks/swiper', self::$post_blocks, true );
		$has_post_filter_block         = in_array( 'lg-blocks/post-filter', self::$post_blocks, true );
		$has_video_banner_block        = in_array( 'lg-blocks/video-banner', self::$post_blocks, true );
		$has_advanced_slider_block     = in_array( 'lg-blocks/advanced-slider', self::$post_blocks, true );

		$add_swiper_globally           = function_exists("get_field") && get_field( 'add_swiper_globally', 'option', true ) ? get_field( 'add_swiper_globally', 'option', true ) : false;
		if ( $add_swiper_globally || $has_wp_gallery_block || $has_swiper_testimonials_block || $has_swiper_slider_block || $has_swiper_block || $has_advanced_slider_block) {
			self::enqueue_swiper();
		}
		if ( $has_swiper_slider_block || $add_swiper_globally ) {
			self::enqueue_swiper_slider();
		}
		if ( $has_swiper_testimonials_block ) {
			self::enqueue_swiper_testimonials();
		}
		if ( $has_wp_gallery_block ) {
			self::enqueue_gallery();
		}
		if ( $has_post_filter_block ) {
			self::enqueue_post_filter();
		}
		if ( $has_video_banner_block ) {
			self::enqueue_video_banner();
		}
		if ( $has_advanced_slider_block ) {
			self::enqueue_advanced_slider();
	}

	}

	public static function enqueue_advanced_slider() {

		wp_enqueue_script(
			'swiper-js',
			plugins_url( 'dist/AdvancedSliderBlock.js', __DIR__ ),
			array(),
			filemtime( plugin_dir_path( __DIR__ ) . 'dist/AdvancedSliderBlock.js' ),
			true
		);
	}


	/**
	 *  Enqueue Advanced Map Assets
	 */
	// public static function enquene_advanced_map() {
	// 	// enqueue advanced maps react-frontend js.
	// 	wp_enqueue_script(
	// 		'lg-frontend-maps',
	// 		plugins_url( 'dist/AdvancedMapAdditional.js', __DIR__ ),
	// 		array( 'react', 'react-dom', 'wp-element', 'wp-i18n', 'wp-polyfill' ),
	// 		'c8bc1c8b920e235a183347ea5e43dd43',
	// 		false
	// 	);
	// }

	/**
	 *  Enqueue swiper assets
	 */
	public static function enqueue_swiper() {
		// enqueue swiper js.
		wp_enqueue_script(
			'swiper-js',
			plugins_url( 'dist/Swiper.js', __DIR__ ),
			array(),
			filemtime( plugin_dir_path( __DIR__ ) . 'dist/Swiper.js' ),
			true
		);

		// enqueue swiper css.
		wp_enqueue_style(
			'swiper-css',
			plugins_url( 'dist/Swiper.css', __DIR__ ),
			array(),
			filemtime( plugin_dir_path( __DIR__ ) . 'dist/Swiper.css' )
		);
	}
	/**
	 *  Enqueue swiper slider assets
	 */
	public static function enqueue_swiper_slider() {
		wp_enqueue_script(
			'swiper-slider-js',
			plugins_url( 'dist/SwiperSlider.js', __DIR__ ),
			array(),
			filemtime( plugin_dir_path( __DIR__ ) . 'dist/SwiperSlider.js' ),
			true
		);
	}
	/**
	 *  Enqueue swiper testimonials assets
	 */
	public static function enqueue_swiper_testimonials() {
		wp_enqueue_script(
			'swiper-testimonials-js',
			plugins_url( 'dist/SwiperTestimonials.js', __DIR__ ),
			array(),
			filemtime( plugin_dir_path( __DIR__ ) . 'dist/SwiperTestimonials.js' ),
			true
		);
	}
	/**
	 *  Enqueue gallery assets
	 */
	public static function enqueue_gallery() {
		wp_enqueue_script(
			'longevity-lightbox-js',
			plugins_url( 'dist/longevityLightbox.js', __DIR__ ),
			array( 'jquery' ),
			filemtime( plugin_dir_path( __DIR__ ) . 'dist/LongevityLightbox.js' ),
			true
		);

		wp_enqueue_style(
			'longevityLightbox-css',
			plugins_url( 'dist/longevityLightbox.css', __DIR__ ),
			array(),
			filemtime( plugin_dir_path( __DIR__ ) . 'dist/LongevityLightbox.css' )
		);
	}
	/**
	 *  Enqueue post filter assets
	 */
	public static function enqueue_post_filter() {
		wp_enqueue_script(
			'post-filter-js',
			plugins_url( 'dist/PostFilter.js', __DIR__ ),
			array(),
			filemtime( plugin_dir_path( __DIR__ ) . 'dist/PostFilter.js' ),
			true
		);
	}
	/**
	 *  Enqueue video banner assets
	 */
	public static function enqueue_video_banner() {
		wp_enqueue_script(
			'jquery-youtube-background-js',
			plugins_url( 'dist/jquery.youtube-background.js', __DIR__ ),
			array( 'jquery' ),
			filemtime( plugin_dir_path( __DIR__ ) . 'dist/jquery.youtube-background.js' ),
			true
		);
		wp_enqueue_script(
			'longevity-video-banner-js',
			plugins_url( 'dist/VideoBanner.js', __DIR__ ),
			array( 'jquery', 'jquery-youtube-background-js' ),
			filemtime( plugin_dir_path( __DIR__ ) . 'dist/VideoBanner.js' ),
			true
		);

		wp_enqueue_style(
			'video-banner-css',
			plugins_url( 'dist/videoBanner.css', __DIR__ ),
			array(),
			filemtime( plugin_dir_path( __DIR__ ) . 'dist/VideoBanner.css' )
		);
	}
	/**
	 *  Function to get blocks in a post
	 */
	public static function get_post_blocks() {
		if ( is_admin() ) {
			return;
		}
		global $post;
		$post_content = get_post( $post->ID )->post_content;
		$blocks       = parse_blocks( $post_content );
		$blocks       = array_map(
			function( $block ) {
				return array( $block['blockName'], $block['innerBlocks'] );
			},
			$blocks
		);
		/**
		 * Flatten the array
		 *
		 * @param blocks        $blocks -> block name.
		 * @param preserve_keys $preserve_keys -> block name.
		 * @param new_array     $new_array -> check if has assets.
		 */
		function array_flatten( $blocks, $preserve_keys, $new_array ) {
			foreach ( $blocks as $key => $child ) {
				if ( is_array( $child ) ) {
					$new_array = array_flatten( $child, $preserve_keys, $new_array );
				} elseif ( $preserve_keys + is_string( $key ) > 1 ) {
					$new_array[ $key ] = $child;
				} else {
					$new_array[] = $child;
				}
			}

			return $new_array;
		}

		$blocks = array_flatten( $blocks, 0, array() );
		$blocks = array_filter(
			$blocks,
			function( $block ) {
				return ( 0 === strpos( $block, 'lg-blocks' ) && 'lg-blocks/accordion-item' !== $block && 'lg-blocks/swiper-single-slide' !== $block && 'lg-blocks/card' !== $block ) || ( strpos( $block, 'core' ) === 0 );
			}
		);

		$blocks = array_unique( $blocks );
		foreach ( $blocks as $block ) {
			array_push( self::$post_blocks, $block );
		}
	}

}

LGBlockFrontEndManager::init();
