<?php
/**Class LGBlock
 *
 * @package LG Blocks classes
 */

/**
 * Class to manage Longevity Blocks in the backend
 */
class LGBlockManager {
	/**
	 * Initialize LG Blocks registration and assets
	 *
	 * @var string
	 */
	public static function init() {
		add_filter( 'block_categories', array( __CLASS__, 'lg_blocks_categories' ), 10, 2 );
		add_filter( 'block_categories_all', array( __CLASS__, 'lg_blocks_categories' ), 10, 2 );
		add_action( 'enqueue_block_editor_assets', array( __CLASS__, 'lg_blocks_enqueue_assets' ) );
		add_action( 'wp_enqueue_scripts', array( __CLASS__, 'enqueue_global_assets' ) );
		add_action( 'wp_enqueue_scripts', array( __CLASS__, 'enqueue_animation' ) );
		add_action( 'init', array( __CLASS__, 'lg_blocks_register' ) );
		add_action( 'init', array( __CLASS__, 'init_blocks' ) );
		add_action( 'init', array( __CLASS__, 'register_block_styles' ) );
	}
	/**
	 * Block Categories
	 *
	 * @param string $categories category name.
	 * @param array  $post the post.
	 */
	public static function lg_blocks_categories( $categories, $post ) {
		return array_merge(
			$categories,
			array(
				array(
					'slug'  => 'lg-category',
					'title' => __( 'LG Blocks', 'lg-blocks' ),
					'icon'  => 'wordpress',
				),
			)
		);
	}
	/**
	 * Register Block Styles
	 */
	public static function register_block_styles() {
		register_block_style(
			'lg-blocks/cards',
			array(
				'name'  => 'solid-header',
				'label' => __( 'Solid Color Header', 'lg-blocks' ),
			)
		);
	}


	/**
	 * Enqueue Global Assets
	 */
	public static function enqueue_global_assets() {
		wp_register_style(
			'blocks-global-style',
			plugins_url( '/lg-blocks/dist/Style.css' ),
			array(),
			filemtime( plugin_dir_path( __DIR__ ) . '/dist/Style.css' ),
		);
		wp_enqueue_style( 'blocks-global-style' );
	}
	/**
	 * Enqueue Animation Assets
	 */
	public static function enqueue_animation() {
		wp_enqueue_script(
			'longevity-animations-js',
			plugins_url( 'dist/Animations.js', __DIR__ ),
			array( 'jquery' ),
			filemtime( plugin_dir_path( __DIR__ ) . 'dist/Animations.js' ),
			true
		);

		wp_enqueue_style(
			'longevity-animations-css',
			plugins_url( 'dist/Animations.css', __DIR__ ),
			array(),
			filemtime( plugin_dir_path( __DIR__ ) . 'dist/Animations.css' )
		);
	}
	/**
	 * Initialize all blocks.
	 */
	public static function init_blocks() {
		if ( is_admin() ) {
			new LGBlock(
				'breadcrumb',
				array(
					'render_callback' => 'lg_blocks_render_breadcrumb_block',
					'attributes'      => array(
						'alignment' => array(
							'type'    => 'string',
							'default' => 'text-center',
						),
					),
				),
				true
			);
			new LGBlock( 'video-modal', array(), true );
			new LGBlock( 'video-banner', array(), true );
			new LGBlock(
				'swiper-testimonials',
				array(
					'render_callback' => 'lg_blocks_render_swiper_testimonials',
					'attributes'      => array(
						'className'            => array(
							'type' => 'string',
						),
						'layoutType'           => array(
							'type'    => 'string',
							'default' => 'slider',
						),
						'postType'             => array(
							'type'    => 'string',
							'default' => 'lg_testimonial',
						),
						'showTitle'            => array(
							'type'    => 'boolean',
							'default' => true,
						),
						'sliderFetchCount'     => array(
							'type'    => 'number',
							'default' => 5,
						),
						'sliderSlidesPerView'  => array(
							'type'    => 'number',
							'default' => 1,
						),
						'sliderSlidesToScroll' => array(
							'type'    => 'number',
							'default' => 1,
						),
						'sliderDots'           => array(
							'type'    => 'boolean',
							'default' => true,
						),
						'sliderArrows'         => array(
							'type'    => 'boolean',
							'default' => false,
						),
						'sliderFade'           => array(
							'type'    => 'boolean',
							'default' => false,
						),
						'sliderAutoplay'       => array(
							'type'    => 'boolean',
							'default' => true,
						),
						'sliderInfinite'       => array(
							'type'    => 'boolean',
							'default' => true,
						),
						'excerpt'              => array(
							'type'    => 'boolean',
							'default' => false,
						),
						'sliderAutoplayDelay'  => array(
							'type'    => 'number',
							'default' => '3000',
						),
						'showImage'            => array(
							'type'    => 'boolean',
							'default' => false,
						),
					),
				),
				true
			);
			new LGBlock( 'swiper-slider', array(), true );
			new LGBlock( 'swiper-single-slide', array(), false );
			new LGBlock( 'pricing-table', array(), true );
			new LGBlock(
				'post-filter',
				array(
					'render_callback' => 'lg_blocks_render_post_filter',
					'attributes'      => array(
						'postType'     => array(
							'type'    => 'string',
							'default' => 'post',
						),
						'taxonomy'     => array(
							'type'    => 'string',
							'default' => 'category',
						),
						'tabAlignment' => array(
							'type'    => 'string',
							'default' => 'tab-left',
						),
					),
				),
				true
			);
			new LGBlock( 'page-header', array(), false );
			new LGBlock( 'map-text', array(), true );
			new LGBlock( 'mailchimp', array(), true );
			new LGBlock( 'img-text', array(), true );
			new LGBlock( 'fancy-heading', array(), true );
			new LGBlock( 'cards', array(), true );
			new LGBlock( 'card', array(), false );
			new LGBlock( 'responsive-image', array(), true );
			new LGBlock( 'accordion', array(), true );
			new LGBlock( 'accordion-item', array(), false );
			new LGBlock(
				'advanced-map',
				array(
					'render_callback' => 'lg_blocks_render_advanced_map',
					'attributes'      => array(
						'Key'          => array(
							'type' => 'string',
						),
						'Zoom'         => array(
							'type' => 'number',
						),
						'Center'       => array(
							'type' => 'array',
						),
						'Markers'      => array(
							'type' => 'array',
						),
						'MarkerAssets' => array(
							'type' => 'object',
						),
						'BlockId'      => array(
							'type' => 'string',
						),
					),
				),
				true
			);
			new LGBlock( 'copyright', array(), false );
			new LGBlock( 'advanced-slider', array(), true );
			new LGBlock( 'post-video', array(), true );
		}
	}
	/**
	 * Enqueue Editor Assets.
	 */
	public static function lg_blocks_enqueue_assets() {
		wp_enqueue_script(
			'lg-blocks-editor-js',
			plugins_url( 'dist/EditorScript.js', __DIR__ ),
			array( 'wp-data' ),
			filemtime( plugin_dir_path( __DIR__ ) . '/dist/EditorScript.js' ),
			false
		);
	}
	/**
	 * LG Block register function
	 */
	public static function lg_blocks_register() {
		wp_register_script(
			'lg-blocks-editor-script',
			plugins_url( 'dist/Editor.js', __DIR__ ),
			array(
				'wp-blocks',
				'wp-i18n',
				'wp-element',
				'wp-block-editor',
				'wp-components',
				'lodash',
				'wp-blob',
				'wp-data',
				'wp-html-entities',
				'wp-compose',
				'wp-hooks',
				'wp-server-side-render',
			),
			filemtime( plugin_dir_path( __DIR__ ) . 'dist/Editor.js' ),
			false
		);

		wp_register_script(
			'lg-blocks-script',
			plugins_url( 'dist/Script.js', __DIR__ ),
			array( 'jquery' ),
			filemtime( plugin_dir_path( __DIR__ ) . 'dist/Script.js' ),
			false
		);

		wp_register_style(
			'lg-blocks-editor-style',
			plugins_url( 'dist/Editor.css', __DIR__ ),
			array( 'wp-edit-blocks' ),
			filemtime( plugin_dir_path( __DIR__ ) . 'dist/Editor.css' )
		);

		wp_register_style(
			'lg-blocks-style',
			plugins_url( 'dist/Style.css', __DIR__ ),
			array(),
			filemtime( plugin_dir_path( __DIR__ ) . 'dist/Style.css' )
		);
	}
}

LGBlockManager::init();
