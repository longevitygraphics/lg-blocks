<?php
/**
 * Class LGBlock
 *
 * @package LG Blocks classes
 */

/**
 * Longevity Block class
 */
class LGBlock {
	/**
	 * Block Name
	 *
	 * @var string
	 */
	public $lg_block;
	/**
	 * JS file link
	 *
	 * @var string
	 */
	public $js_url;
	/**
	 * CSS file link
	 *
	 * @var string
	 */
	public $css_url;
	/**
	 * LG Blocks constructor.
	 *
	 * @param string  $lg_block block name of the block.
	 * @param array   $lg_block_options block options, mainly for server side render.
	 * @param boolean $has_assets see if the block has front end assets to embed.
	 */
	public function __construct( $lg_block, $lg_block_options, $has_assets ) {
		$this->lg_block         = $lg_block;
		$this->lg_block_options = $lg_block_options;
		$this->js_url           = str_replace( ' ', '', ucwords( str_replace( '-', ' ', $this->lg_block ) ) ) . 'Block.js';
		$this->css_url          = str_replace( ' ', '', ucwords( str_replace( '-', ' ', $this->lg_block ) ) ) . 'Block.css';
		if ( $has_assets ) {
			$this->enqueue_assets();
		}
		$this->lg_register_block_type();
	}
	/**
	 *  Function to register block type
	 */
	public function lg_register_block_type() {
		register_block_type(
			'lg-blocks/' . $this->lg_block,
			array_merge(
				array(
					'editor_script' => 'lg-blocks-editor-script',
					'editor_style'  => 'lg-blocks-editor-style',
					'script'        => 'lg-blocks-script',
					'style'         => 'lg-blocks-style',
				),
				$this->lg_block_options
			)
		);
	}
	/**
	 *  Enqueue function assets
	 */
	public function enqueue_assets() {
			wp_register_style(
				$this->lg_block,
				plugins_url( 'lg-blocks/dist/' . $this->css_url ),
				array(),
				filemtime( plugin_dir_path( __DIR__ ) ),
			);

			wp_enqueue_style( $this->lg_block );

			wp_register_script(
				$this->lg_block,
				plugins_url( 'lg-blocks/dist/' . $this->js_url ),
				array( 'jquery' ),
				filemtime( plugin_dir_path( __DIR__ ) ),
				true
			);
			wp_enqueue_script( $this->lg_block );
	}
}
