<?php
/**LG Front End Block registration, this would only be required for server side rendered content
 *
 * @package LG Blocks classes
 */

/**
 * Class to register blocks for server sider render
 */
class LGBlockFrontendRegistration {
	/**
	 * Block Name
	 *
	 * @var string
	 */
	public $block;
	/**
	 * Block options
	 *
	 * @var array
	 */
	public $options;
	/**
	 * LG Blocks constructor.
	 *
	 * @param string $lg_block block name of the block.
	 * @param array  $options block options, contains server side render content.
	 */
	public function __construct( $lg_block, $options ) {
		$this->lg_block = $lg_block;
		$this->options  = $options;
		$this->lg_register_frontend_block_type();
	}
	/**
	 *  Function to register block type so that server sider rendered content could be fetched
	 */
	public function lg_register_frontend_block_type() {
		register_block_type(
			'lg-blocks/' . $this->lg_block,
			array_merge(
				array(
					'editor_script' => 'lg-blocks-editor-script',
					'editor_style'  => array( 'lg-blocks-editor-style' ),
					'script'        => 'lg-blocks-script',
					'style'         => 'lg-blocks-style',
				),
				$this->options
			)
		);
	}
}
